<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parent_controller extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		// if ( ! $this->session->userdata('is_logged_in')){
			// redirect('auth');
		// }
		$this->load->library('encryption');
		$this->load->model('Parent_model');
		$this->load->model('Offering_model');
		$this->load->model('Auth_model');
		$this->load->model('fe/Frontend_model');
		$this->load->model('Course_model');
		$this->load->model('Report_model');
		$this->load->model('Dashboard_model');
		$this->load->library('form_validation');
		 $this->load->library('Ajax_pagination');
        $this->perPage = 3;
		
		$data["get_categories"] = $this->Parent_model->get_categories();
			$data["content"] = "header/header_view";
		$this->load->view('template/template', $data, true);
    }
	/**
    * encript the password
    * @return mixed
    */
	
    function __encrip_password($password) {
		$password = $this->encryption->encrypt($password);
        return $password;
    }
	/**
    * encript the password
    * @return mixed
    */
	
    function __decrip_password($password) {
		$password = $this->encryption->decrypt($password);
        return $password;
    }
	/**
    * encript the password
    * @return mixed
    */
	
    function get_date_time() {
		$this->load->helper('date');
 
		$date_time = date('m-d-Y H:i:s'); 
        return $date_time;
    }
	/**
    * encript the password
    * @return mixed
    */
	
    function get_date() {
		$this->load->helper('date');
 
		$date_time = date('m-d-Y'); 
        return $date_time;
    }
	/**
    * encript the password
    * @return mixed
    */
	
    function get_time() {
		$this->load->helper('date');
 
		$date_time = date('H:i:s'); 
        return $date_time;
    }
	/**
    * Change passwore in the database
    * @return void
    */
	function course_upload_attachment($upload_path , $field_name, $course_id)
	{
            
            $config['upload_path']   = $upload_path;
            $config['allowed_types'] = "gif|jpg|jpeg|png|tiff|tif|pdf|jpeg|jif|bmp";

            $config['max_size'] = "0";
            $final_file_name = time();
			$config['file_name'] = $final_file_name;
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 90;
            $config['height'] = 90;
            $this->load->library('upload', $config);
			$this->upload->initialize($config);

            if(!empty($course_id)){
                $get_course_name = $this->Course_model->get_course_name($course_id);
                $old_path = "./assets/courses/".$get_course_name;

                $new_path =  $upload_path;
                //$full_path = rename($old_path, './'.$new_path);
                $pathToUpload = $upload_path;
            } else {
                $pathToUpload = $upload_path;
            }
	
	 
            if ( ! file_exists($pathToUpload) )
            {
                $create = mkdir($pathToUpload, 0777, TRUE);
                if ( ! $create)
                return;
            } else {
                 $create = $upload_path;

            }
           
            if (!$this->upload->do_upload($field_name)) {
				
                return $this->upload->display_errors();
				// return $this->upload->data();
            } else {
                $finfo = $this->upload->data();
				
				return $finfo['file_name'];
				
               
            }
    }
	/**
    * Change passwore in the database
    * @return void
    */
	function upload_attachment($upload_path , $field_name)
	{
            $pathToUpload = $upload_path;
				$createThumbsFolder = '';
				if ( ! file_exists($pathToUpload) )
				{
//					$create = mkdir($pathToUpload, 0777, TRUE);
					$createThumbsFolder = mkdir($pathToUpload, 0777);
					if ( ! $createThumbsFolder)
					return;
				} else {
					 $createThumbsFolder = $upload_path;
				
				}
				
            $config['upload_path']   = $createThumbsFolder;
            $config['allowed_types'] = "gif|jpg|jpeg|png|tiff|tif|pdf|jpeg|jif|bmp";

            $config['max_size'] = "0";
            //$config['max_width'] = "0";
            //$config['max_height'] = "0";
			// $config['file_name'] = $var;
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 90;
            $config['height'] = 90;
			$new_name = time().$_FILES[$field_name]['name'];
			$config['file_name'] = $new_name;

            $this->load->library('upload', $config);
			$this->upload->initialize($config);
            if (!$this->upload->do_upload($field_name)) {
				
                return $this->upload->display_errors();
				// return $this->upload->data();
            } else {
                $finfo = $this->upload->data($field_name);
				// return true;
                // $this->_createThumbnail($finfo['file_name']);
				// echo $finfo['file_name'];
				return $new_name;
				// return "testing ".$finfo;
               
            }
    }
	/**
    * Change passwore in the database
    * @return void
    */
	function upload_zip_attachment($upload_path , $field_name , $course_name, $unique_name)
	{
			 
				$pathToUpload = $upload_path.'/lessons/'.$unique_name;
				$create = '';
				if ( ! file_exists($pathToUpload) )
				{
					$create = mkdir($pathToUpload, 0777, TRUE);
					// $createThumbsFolder = mkdir($pathToUpload, 0777);
					if ( ! $create)
					return;
				} 
				 else {
					 $create = $pathToUpload;
			
				} 
            $this->load->library('unzip');
			$config['upload_path'] = $pathToUpload;
			 // $config['allowed_types'] = "gif|jpg|jpeg|png|tiff|tif|pdf|jpeg|jif|bmp|zip";
			$config['allowed_types'] = 'zip';
			$config['max_size'] = '0';
//			$config['max_size'] = '';
			$this->load->library('upload', $config);
		  if ( ! $this->upload->do_upload($field_name)){
			$error = array('error' => $this->upload->display_errors());
			// $this->load->view('upload_form_view', $error);
		  }else{
			  
			$data = array('upload_file' => $this->upload->data());

    // Optional: Only take out these files, anything else is ignored
    $this->unzip->allow(array('css', 'zip', 'png', 'gif', 'jpeg', 'jpg', 'tpl', 'html', 'swf', 'js', 'txt', 'mp3', 'mp4', 'xsd', 'xml', 'json'));

    $this->unzip->extract($data['upload_file']['full_path'], $pathToUpload);
				return $_FILES['upload_file']['name'];	

			}
		  
    }
	function upload_pdf_attachment($upload_path , $field_name , $course_name, $unique_name)
	{
			 
				$pathToUpload = $upload_path.'/lessons/'.$unique_name;
				$create = '';
				if ( ! file_exists($pathToUpload) )
				{
					$create = mkdir($pathToUpload, 0777, TRUE);
					// $createThumbsFolder = mkdir($pathToUpload, 0777);
					if ( ! $create)
					return;
				} 
				 else {
					 $create = $pathToUpload;
			
				} 
            // $this->load->library('unzip');
			$config['upload_path'] = $pathToUpload;
			$config['allowed_types'] = "gif|pdf|ppt|pptx|docx";
			$config['max_size'] = '';
			$this->load->library('upload', $config);
		  if ( ! $this->upload->do_upload($field_name)){
			$error = array('error' => $this->upload->display_errors());
			// $this->load->view('upload_form_view', $error);
		  }else{
			  
			$data = array('upload_file' => $this->upload->data());
			return $data['upload_file']['file_name'];   

			}
		  
    }
	
	 function ajax_pagination_data($per_page , $offset , $total_rec , $fun_name)
    {
        
        //pagination configuration
        $config['target']      = '#container';
        $config['base_url']    = base_url().$fun_name;
        $config['total_rows']  = $total_rec;
        $config['per_page']    = $per_page;
        
        $this->ajax_pagination->initialize($config);
        
    }
	
	
	function get_categories22() {
		
			$data["get_categories"] = "profile/profile_view";
			$data["content"] = "header/header_view";
		$this->load->view('template/template', $data);
	}
	function get_profile_image($user_id) {
		return $this->Parent_model->get_profile_image($user_id);
	}
	
	function get_membership_packages() {
		return $this->Parent_model->get_membership_packages();
	}
	function get_membership_by_id($membership_id) {
		return $this->Parent_model->get_membership_by_id($membership_id);
	}
	
	function paypal_credentialnal($membership_price , $company_id) {
		$vars = array(
						'cmd' => '_xclick',
						'business' => $this->config->item('business_email'),
						// 'rm' => '1',
						'lc' => 'US',
						'item_name' => 'signup',
						'item_number' => $company_id,
						'company_id' => $company_id,
						// 'amount' => number_format($membership_price, 2),
						'amount' => number_format($membership_price, 2),
						'notify_url' => base_url().$this->config->item('notify_url'),
						'return' => base_url().$this->config->item('return_url'),
						'currency_code' => $this->config->item('currency_code'),
						'button_subtype' => 'goods',
						'no_note' => 0,
						'tax_rate' => 0,
				);
		
				$paypal_url = $this->config->item('paypal_url').http_build_query($vars);
			return $paypal_url;
	}
	/* 
	# function to pay 
	# buy indiviual courses
	# retunr url
	*/
	function paypal_buy($membership_price , $company_id, $student_id) {
		$vars = array(
						'cmd' => '_xclick',
						'business' => $this->config->item('business_email'),
						// 'rm' => '1',
						'lc' => 'US',
						'item_name' => $student_id,
						'item_number' => $company_id,
						'user_id' => $student_id,
						// 'amount' => number_format($membership_price, 2),
						'amount' => number_format($membership_price, 2),
						'notify_url' => base_url().$this->config->item('notify_url'),
						'return' => base_url().$this->config->item('return_url'),
						'currency_code' => $this->config->item('currency_code'),
						'button_subtype' => 'goods',
						'no_note' => 0,
						'tax_rate' => 0,
				);
		
				$paypal_url = $this->config->item('paypal_url').http_build_query($vars);
			return $paypal_url;
	}

	/*
		* function to buy with paypal
		* a proper method
	*/


	function payment_paypal($membership_price, $course_id)
	{
		$course_information = $this->Course_model->get_course_by_id($course_id);
		$course_name = str_replace("_", " ", $course_information[0]['displayName']);
		$course_code = '0'.$course_id;
		$app_client_id = $this->config->item('paypal_client_id');
		$app_sceret_key = $this->config->item('paypal_client_sceret_key');
		
       	$apiContext = new \PayPal\Rest\ApiContext(
   		 	new \PayPal\Auth\OAuthTokenCredential(
        	$app_client_id,     // ClientID
        	$app_sceret_key       // ClientSecret
    		)
		);

		$apiContext->setConfig([
				'mode' => 'live',
				'http.ConnectionTimeOut' => 30
			]);

		$payer = new PayPal\Api\Payer();
		$payer->setPaymentMethod("paypal");
				
		$item1 = new PayPal\Api\Item();
		$item1->setName($course_name)
		    ->setCurrency('USD')
		    ->setQuantity(1)
		    ->setSku($course_code) // Similar to `item_number` in Classic API
		    ->setPrice($membership_price);

		$itemList = new PayPal\Api\ItemList();
		$itemList->setItems(array($item1));

		$details = new PayPal\Api\Details();
		$details->setShipping(0)
		    ->setTax(0)
		    ->setSubtotal($membership_price);

		    $amount = new PayPal\Api\Amount();
		$amount->setCurrency("USD")
		    ->setTotal($membership_price)
		    ->setDetails($details);

		    $transaction = new PayPal\Api\Transaction();
		$transaction->setAmount($amount)
		    ->setItemList($itemList)
		    ->setDescription("Payment description")
		    ->setInvoiceNumber(uniqid());

		    // $baseUrl = getBaseUrl();
		$redirectUrls = new PayPal\Api\RedirectUrls();
		$redirectUrls->setReturnUrl(base_url()."/payments?approve=true&price=".$membership_price."&course_id=".$course_id)
		    ->setCancelUrl(base_url()."/payments?approve=false&price=".$membership_price."&course_id=".$course_id);

		    $payment = new PayPal\Api\Payment();
		$payment->setIntent("sale")
		    ->setPayer($payer)
		    ->setRedirectUrls($redirectUrls)
		    ->setTransactions(array($transaction));

		    $request = clone $payment;

		    try {
		    $payment->create($apiContext);
		} catch (Exception $ex) {
			ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex);
		    exit(1);
		}

		$approvalUrl = $payment->getApprovalLink();


		foreach ($payment->getLinks() as $link) {
			if($link->getRel() == 'approval_url')
			{
				$redirectUrl = $link->getHref();
			}
		}

		return $redirectUrl;
	}


	/*
		* End of the Paypal Account
	*/
	
	/**
    * export excel.
    * @return void
    */
	function csv_report($report, $file_name, $file_type = '')
	{
		
		$this->load->dbutil();
		$this->load->helper('file');
		$this->load->helper('download');
		/* get the object   */
	
		$delimiter = ",";
		$newline = "\r\n";
		$new_report = $this->dbutil->csv_from_result($report, $delimiter, $newline);
		if($file_type = "load_file")
		{
			write_file( 'assets/asha_report/load_file/'.$file_name.'.csv', $new_report);
		}else
		{
			write_file( 'application/third_party/'.$file_name.'.csv', $new_report);
		}
		force_download($file_name.'.csv', $new_report);
		// $this->load->view('report_success.php');
	}
    
    function refresh($course_id, $offering_id = ""){
        
        $course_summary = $this->Frontend_model->get_course_summary($course_id);
        $active_course_offer = $this->Report_model->get_active_course_offer($course_id);
        $part_forms = 0;
        $generating_report;
        if(!empty($offering_id) || !empty($active_course_offer))
        {
        	if(!empty($offering_id))
        	{
        		$generating_report = $this->Report_model->generate_reports_from_raw($course_id, $offering_id);
        	}else if(!empty($active_course_offer))
        	{
        		$offering_id = $active_course_offer['0']['offeringID'];
        		$generating_report = $this->Report_model->generate_reports_from_raw($course_id, $active_course_offer['0']['offeringID']);
        	}
        	
        	if(empty($generating_report))
        	{
        		$part_forms = 0;
        	}else
        	{
        		$part_forms = count($generating_report);
        	}

        	$data = array(
        		"partForms" => $part_forms,
        		"modifiedDate" => date('Y-m-d H:i:s'),
        	);
        	$this->Frontend_model->update_asha_summary($course_id, $offering_id, $data);
        }
        if(!empty($course_summary))
        {
        	foreach($course_summary as $summary)
	        {
	            $assgin_courses_id = $summary['assigned_courses'];
	            $total_course_assign = $summary['Total Assignee'];

	            $review_course_id = $summary['review_courses'];
	            $total_reviews_course = $summary['Total_Reviews'];
	            $average_rating_course = $summary['Average_Rating'];
	            
	            if($assgin_courses_id != "")
	            {
	                $this->Frontend_model->update_total_assign($assgin_courses_id,$total_course_assign);
	            }
	            else if ($review_course_id != "")
	            {
	                $this->Frontend_model->update_course_rating($review_course_id,$total_reviews_course,$average_rating_course);
	            }
	        }
        }
    }
    
    public function earnings($course_id)
    {
        $information     = $this->Course_model->earnings($course_id);
        $ceCheck = $information[0]['ceCheck'];
        $price = $information[0]['price'];
        $admin_commission2 = 0;
        if($ceCheck == '1')
        {
            $admin_commission = (($price * 70) / 100);
            $admin_commission2 = (($price * 70) / 100);
        }else
        {
            $admin_commission =  (($price * 20) / 100);
            $admin_commission2 = (($price * 20) / 100);
        }
        
        if($admin_commission > 0)
        {
            $eductor_earnings = ($price - $admin_commission);
            
            $arraylenghts = count($information);
            if($arraylenghts > 1)
            {
                $final_earnings = ($eductor_earnings / $arraylenghts);
                $admin_commission = ($admin_commission2 / $arraylenghts);
            }else
            {
                $final_earnings = $eductor_earnings;
            }
        }else
        {
            $final_earnings = null;
        }
        
        foreach($information as $info)
        {
            $author_id = $info['userID'];
            $author_earnings = $this->Course_model->author_earnings($author_id, $course_id, $final_earnings, $admin_commission, $price);
        }
        
    }
        
    /**
    * Send Email Function
    * @return void
    */
	function send_email($id, $email_type , $paypal_url = '', $userid = null, $course_slug = null,$course_id = null)
	{
											
					$this->load->library('email');
					
					$config['protocol'] 		= 'smtp';
					$config['smtp_host']        = 'ssl://smtp.mailgun.org';
					$config['smtp_port']        = 465;
					$config['smtp_user']        = 'postmaster@mail.yguni.com';
					$config['smtp_pass']        = '75aa2fc22be147890b517841ac00e565';
					$config['smtp_crypto']      = '';
					// $config['mailpath'] = '/usr/sbin/sendmail';
					$config['charset'] = 'iso-8859-1';
					$config['wordwrap'] = TRUE;   
					$config['mailtype'] = 'html';

					$this->email->initialize($config);
					$this->email->from('info@yguni.com', 'YappGuru University (yguni.com)');
					
                    if($email_type == 'register')
                    {
                        $userinfo = $this->Parent_model->get_user($id);
                        $data['password'] =  $this->__decrip_password($userinfo[0]['userPassword']);
                        $data['user_info'] = $this->Parent_model->get_user($id);
                        $data['paypal_url'] = $paypal_url;
                        
                        $messages = $this->load->view('email_notification/registration_email', $data, true);

                        $this->email->to($userinfo[0]['userEmail']);
                        $this->email->subject('Confirmation for Student');
                    }else if($email_type == "pending_users")
                    {
                    	$userinfo = $this->Parent_model->get_user($id);
                        $data['user_info'] = $this->Parent_model->get_user($id);
                        $data['course_slug'] = $course_slug;
                        $messages = $this->load->view('email_notification/for_asha_email', $data, true);

                        $this->email->to($userinfo[0]['userEmail']);
                        $this->email->subject('LAST CHANCE for SLP Summit CE reporting'); 
                    }else if($email_type == 'userStatus')
                    {
                        $userinfo = $this->Parent_model->get_user($id);
                        $data['password'] =  $this->__decrip_password($userinfo[0]['userPassword']);
                        $data['user_info'] = $this->Parent_model->get_user($id);

                        $messages = $this->load->view('email_notification/user_status_change_email', $data, true);
                        $this->email->to($userinfo[0]['userEmail']);
                        $this->email->subject('Your user status changed');
                    }else if($email_type == 'paymentStatus')
                    {
                        $userinfo = $this->Parent_model->get_user($id);
                        $data['password'] =  $this->__decrip_password($userinfo[0]['userPassword']);
                        $data['user_info'] = $this->Parent_model->get_user($id);

                        $messages = $this->load->view('email_notification/payment_status_change_email', $data, true);
                        $this->email->to($userinfo[0]['userEmail']);
                        $this->email->subject('Your payment status changed');
                    }else if($email_type == 'payment')
                    {
                        $userinfo = $this->Parent_model->get_user($id);
                        $data['password'] =  $this->__decrip_password($userinfo['0']['userPassword']);
                        $data['user_info'] = $this->Parent_model->get_user($id);
                        
                        $messages = $this->load->view('email_notification/payment_success_email', $data, true);
                        $this->email->to($userinfo['0']['userEmail']);
                        $this->email->subject('Payment Successful'); 
                    }else if($email_type == 'course')
                    {
                        $userinfo = $this->Parent_model->get_user($id);
                        $data['password'] =  $this->__decrip_password($userinfo[0]['userPassword']);
                        $data['user_info'] = $this->Parent_model->get_user($id);

                        $messages = $this->load->view('email_notification/course_assign_email', $data, true);
                        $this->email->to($userinfo[0]['userEmail']);
                        $this->email->subject('Course is assigned to you'); 
                    }else if($email_type == 'course_pending')
                    {
                        $userinfo = $this->Parent_model->get_user($id);
                        $data['password'] =  $this->__decrip_password($userinfo[0]['userPassword']);
                        $data['user_info'] = $this->Parent_model->get_user($id);
                        $data['course_detail'] = $this->Course_model->get_course_by_id($course_id);

                        $messages = $this->load->view('email_notification/course_approval_email', $data, true);
                        $this->email->to("mailing.chan@gmail.com");
                        $this->email->subject('You have new pending Course'); 
                    }else if($email_type == 'signup')
                    {
                        $userinfo = $this->Parent_model->get_user($id);
                        $data['password'] =  $this->__decrip_password($userinfo[0]['userPassword']);
                        $data['user_info'] = $this->Parent_model->get_user($id);
                        $data['paypal_url'] = $paypal_url;
                        
                        $messages = $this->load->view('email_notification/registration_email', $data, true);
                        $this->email->to($userinfo[0]['userEmail']);
                        $this->email->subject('Pay to proceed online signup'); 
                    }else if($email_type == 'verification')
                    {
                        $userinfo = $this->Parent_model->get_user($id);
                        $data['user_info'] = $this->Parent_model->get_user($id);
                        $data['course_slug'] = $course_slug;
                        $messages = $this->load->view('email_notification/verification_email', $data, true);

                        $this->email->to($userinfo[0]['userEmail']);
                        $this->email->subject('Verification Email'); 
    
                    }else if($email_type == 'remainder_quiz')
                    {
                        $userinfo = $this->Parent_model->get_user($id);
                        $data['user_info'] = $this->Parent_model->get_user($id);
                        $messages = $this->load->view('email_notification/remainder_quiz_email', $data, true);
                        
                        $this->email->to($userinfo[0]['userEmail']);
                        $this->email->subject('Verification Email'); 
                    }else if($email_type == 'forget')
                    {
                        $userinfo = $this->Parent_model->get_user($id);
                        $data['user_info'] = $this->Parent_model->get_user($id);
                        $messages = $this->load->view('email_notification/forget_email', $data, true);
                        
                        $this->email->to($userinfo[0]['userEmail']);
                        $this->email->subject('Verification Email'); 
                    }else if($email_type = "cron_job")
                    {
                        $messages = "cron job send";
                        $this->email->to("wajahat@ikarobar.com");
                        $this->email->subject('Cron Job Email'); 
                    }
                    
                                        
					$this->email->message($messages);				
					return $this->email->send();
					
		
	}

	public function made_me_instructor($user_id,$user_know)
    {
        $user_info = $this->Parent_model->get_user_by_ids($user_id);

        if($user_know = "admin")
        {

        $user_id2           = $this->session->userdata('userID');
        $user_name          = $this->session->userdata('userName');
        $user_email         = $this->session->userdata('userEmail');
        $first_name         = $this->session->userdata('firstName');
        $last_name          = $this->session->userdata('lastName');
        $user_phone_no      = $this->session->userdata('userPhoneNo');
        $user_type          = $this->session->userdata('userType');
        $company_id         = $this->session->userdata('companyID');
        $user_profile       = $this->session->userdata('fileName');
        $logged_in_check    = $this->session->userdata('is_logged_in');

            

        $change_permission = array(
                        'userID2'            => $user_id2,
                        'userName2'          => $user_name,
                        'userEmail2'         => $user_email,
                        'firstName2'         => $first_name,
                        'lastName2'          => $last_name,
                        'userPhoneNo2'       => $user_phone_no,
                        'userType2'          => $user_type,
                        'companyID2'         => $company_id,
                        'fileName2'  		 => $user_profile,
                        'is_logged_in2'      => $logged_in_check
            );

        $changing_permissions = $this->session->set_userdata($change_permission);

        
            $newdata = array(
                        'userID'            => $user_info[0]['userID'],
                        'userName'          => $user_info[0]['userName'],
                        'userEmail'         => $user_info[0]['userEmail'],
                        'firstName'         => $user_info[0]['firstName'],
                        'lastName'         => $user_info[0]['lastName'],
                        'userPhoneNo'       => $user_info[0]['userPhoneNo'],
                        'userType'          => $user_info[0]['userType'],
                        'additionalRole'    => $user_type,
                        'additionalID'      => $user_id2,
                        'companyID'         => $user_info[0]['companyID'],
                        'fileName'  => $user_info[0]['fileName'],
                        'is_logged_in'      => TRUE
                    );
            $is_set_permission = $this->session->set_userdata($newdata);

            $something_to_return = true;

            return $something_to_return;
        
        
        }
    }

    public function made_me_admin($user_id,$user_know)
    {
       $user_info = $this->Parent_model->get_user_by_ids($user_id);

       $this->session->unset_userdata('additionalRole');
       $this->session->unset_userdata('additionalID');
       
        if($user_know = "admin")
        {
     
            $newdata = array(
                        'userID'            => $user_info[0]['userID'],
                        'userName'          => $user_info[0]['userName'],
                        'userEmail'         => $user_info[0]['userEmail'],
                        'firstName'         => $user_info[0]['firstName'],
                        'lastName'         => $user_info[0]['lastName'],
                        'userPhoneNo'       => $user_info[0]['userPhoneNo'],
                        'userType'          => $user_info[0]['userType'],
                        'companyID'         => $user_info[0]['companyID'],
                        'userProfileImage'  => $user_info[0]['fileName'],
                        'is_logged_in'      => TRUE
                    );
            $is_set_permission = $this->session->set_userdata($newdata);

            $something_to_return = true;

            return $something_to_return;
        
        } 
    }

	/*Function to send HTTP POST Requests*/
	/*Used by every function below to make HTTP POST call*/
	function sendRequest($calledFunction, $data){
		/*Creates the endpoint URL*/
		$request_url =  $this->config->item('api_url').$calledFunction;

		/*Adds the Key, Secret, & Datatype to the passed array*/
		$data['api_key'] =  $this->config->item('api_key');
		$data['api_secret'] =  $this->config->item('api_secret');
		$data['data_type'] = 'JSON';

		$postFields = http_build_query($data);
		/*Check to see queried fields*/
		/*Used for troubleshooting/debugging*/
		// echo $postFields;

		/*Preparing Query...*/
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_URL, $request_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
		$response = curl_exec($ch);
		
		curl_close($ch);

		/*Will print back the response from the call*/
		/*Used for troubleshooting/debugging		*/
		// echo $request_url;
		// var_dump($data);
		// var_dump($response);

		if(!$response){
			return false;
		}
		/*Return the data in JSON format*/
		//return json_decode($response, true);
        $someArray = json_decode($response, true);
        return $someArray;        // Dump all data of the Array
        
	}

	/*
	* Zoom api for the webinar registration
	* return void
    */
    
    public function zoom_api($user_id , $course_id)
    {
    	$user_infor = $this->Auth_model->get_users_info($user_id);
    	foreach ($user_infor as $key) {
    		$user_id 	= $key['userID'];
    		$user_email = $key['userEmail'];
    		$first_name = $key['firstName'];
    		$last_name  = $key['lastName'];
    	}
    	$course_infor = $this->Course_model->get_lesson_by_id($course_id, "webinar");

    	if(!empty($course_infor))
    	{
    		foreach($course_infor as $lesson)
    		{
	    		$data['id'] = $lesson['webinarId'];
	    		$data['email'] =$user_email;
	    		$data['first_name'] =$first_name;
	    		$data['last_name'] =$last_name;

	    		$response = $this->sendRequest("webinar/register", $data);

	    		$response['userID'] = $user_id;
	    		$response['courseID'] = $course_id;
	    		$response['lessonID'] = $lesson['lessonID'];
	    		$response['createdDate'] = date('Y-m-d H:i:s');
	    		$response['modifiedDate'] = date('Y-m-d H:i:s');
	    		$response['deleted'] = '0';

	    		$this->Auth_model->zoom_array($response);
    		}
    	}
    }

    public function convert_date($date)
    {
    	$time = strtotime($date);
    	return $time;
    }

    public function set_course_offering()
	{
		$set_offers = $this->Offering_model->get_valid_offerings();
		foreach ($set_offers as $key) {
			$course_id = $key['coursesID'];
			$offering_id = $key['offeringID'];
			$this->Offering_model->set_course_offer($course_id, $offering_id);
		}
	}

	function generateCsv($data, $file_type, $file_name = '', $delimiter = ',', $enclosure = '') {
		$year 	= date("Y");
		$month 	= date("m");
		$day 	= date("d");
		$root 	= getcwd();
		$handle2 = "";
		if($file_type == "load_file")
		{
			$handle = fopen($root.'/assets/asha_report/load_file/'.$file_name.'.csv', 'w+');
		}else if($file_type == "reference_file")
		{
			$handle = fopen($root.'/assets/asha_report/reference_file/'.$file_name.'ref.csv', 'w+');
		}else if($file_type == "individual_file")
		{
			$handle = fopen($root.'/assets/asha_report/report'.time().'indiviual.csv', 'w+');
			$handle2 = 'report'.time().'indiviual.csv';
		}else if($file_type == "missed_users")
		{
			$handle = fopen($root.'/assets/file.csv', 'w+');
		}
       foreach ($data as $line) {
           
           fputcsv($handle, explode(',',$line), $delimiter);
       }
       //exit;
       rewind($handle);
       $contents = "";
       while (!feof($handle)) {
               $contents .= fread($handle, 8192);
       }
       fclose($handle);
       		// echo "<br>";
		// echo time();

       return $handle;
       // exit;
	}

	function collect_raw_data($report_id, $user_id, $course_id, $quiz_id)
	{
		$check_has_asha = $this->Report_model->get_asha_information($user_id);
		$last_name = $check_has_asha[0]['lastName'];
		$asha_account_number = $check_has_asha[0]['ashaAccountNumber'];

		if(!empty($check_has_asha))
		{
			$active_course_offer = $this->Report_model->get_active_course_offer($course_id);
			$connected_courses = $this->Report_model->get_connections($course_id);
			$other_connected_course = $connected_courses[0]['connectWithID'];
			$checking_report = $this->Report_model->get_user_report_by_course_id($user_id, $other_connected_course);
			$remove_duplication = $this->remove_duplication_one_user($user_id, $course_id);
			if(!empty($checking_report))
			{
				if($checking_report[0]['obtainedPercentage'] > 80)
				{
					$this->Report_model->enter_raw_data($user_id, $other_connected_course, $quiz_id, $report_id, $last_name, $active_course_offer[0]['offeringID'], $asha_account_number);
				}else
				{
					$this->Report_model->enter_raw_data($user_id, $course_id, $quiz_id, $report_id, $last_name, $active_course_offer[0]['offeringID'], $asha_account_number);
				}
			}else
			{
				$this->Report_model->enter_raw_data($user_id, $course_id, $quiz_id, $report_id, $last_name, $active_course_offer[0]['offeringID'], $asha_account_number);
			}
		}

	}

	//Extension of the remove duplication function for only one user
	function remove_duplication_one_user($user_id, $course_id)
	{
		$report 			= $this->Report_model->get_user_report_by_course_id($user_id, $course_id);
		$get_connection 	= $this->Report_model->get_connections($course_id);
		if(!empty($get_connection[0]['connectWithID']) && $course_id != $get_connection[0]['connectWithID'])
		{
			$check_duplication 	= $this->Report_model->get_duplicate_report($user_id, $get_connection[0]['connectWithID'], $report[0]['obtainedPercentage'], $course_id);
			return true;
		}else if(!empty($get_connection[0]['coursesID']))
		{
			$check_duplication 	= $this->Report_model->get_duplicate_report($user_id, $get_connection[0]['coursesID'], $report[0]['obtainedPercentage'], $course_id);
			return true;
		}
		echo "Done";
	}
}
