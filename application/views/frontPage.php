<main id="main" class="site-main">
    <header class="text-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
                    <h1 class="header">
                        JOTUN
                        <br>Identity
                    </h1>
                    <h3 class="sub-header">Colour Collection 2019</h3>
                    <p>Create your personal space with colours designed by Jotun.</p>
                </div>
            </div>
        </div>
    </header>

    <section class="section section--themes" id="themes">
        <div class="container card-container">
            <div class="row">
                <div class="card col-xs-12 col-sm-6 col-lg-4 ">
                    <div class="row">
                        <figure class="card__item card__item--figure col-xs-12" id="t1-image--card">
                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t1_0" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>

                                <a href="http://jotun-gcc.com.dev02.allegro.no/calm" tabindex="-1">
                                    <picture>
                                        <source data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_calm_a2_0531.webp" type="image/webp"
                                            srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_calm_a2_0531.webp">
                                        <img class="card__image img-responsive lazyload" src="<?php echo base_url(); ?>/assets/dist/Jotun_calm_a2_0531.jpg"
                                            data-src="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_calm_a2_0531.jpg" data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_calm_a2_0531.jpg"
                                            alt="WALL: JOTUN 1877 WARM GREY" srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_calm_a2_0531.jpg">
                                    </picture>
                                </a>
                            </figure>
                        </figure>
                        <div class="card__item card__body col-xs-12">
                            <h2 class="card__header">
                                <a class="" href="<?php echo $this->config->item("calm_link"); ?>">CALM</a>
                            </h2>
                            <p>Soft neutral tones and warm, subtle contrasts. <br class="visible-lg"><a href="<?php echo $this->config->item("calm_link"); ?>">Discover
                                    now</a></p>
                        </div>
                    </div>
                </div>
                <div class="card col-xs-12 col-sm-6 col-lg-4 ">
                    <div class="row">
                        <figure class="card__item card__item--figure col-xs-12" id="t2-image--card">
                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t2_0" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>

                                <a href="http://jotun-gcc.com.dev02.allegro.no/refined" tabindex="-1">
                                    <picture>
                                        <source data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_moderne_acc1_0590.webp"
                                            type="image/webp" srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_moderne_acc1_0590.webp">
                                        <img class="card__image img-responsive lazyload" src="<?php echo base_url(); ?>/assets/dist/Jotun_moderne_acc1_0590.jpg"
                                            data-src="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_moderne_acc1_0590.jpg"
                                            data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_moderne_acc1_0590.jpg" alt="CEILING: JOTUN PERFECTION 1453 COTTON BALL PANEL WALL: JOTUN SUPREME FINISH MATT 10580 SOFT SKIN WALL (IN FRONT): JOTUN PURE COLOR 20046 SAVANNA SUNSET SKIRTING: JOTUN SUPREME FINISH MATT 20046 SAVANNA SUNSET"
                                            srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_moderne_acc1_0590.jpg">
                                    </picture>
                                </a>
                            </figure>
                        </figure>
                        <div class="card__item card__body col-xs-12">
                            <h2 class="card__header">
                                <a class="" href="<?php echo $this->config->item("refined_link"); ?>">REFINED</a>
                            </h2>
                            <p>A vibrant palette of greens and yellows. <br class="visible-lg"><a href="<?php echo $this->config->item("refined_link"); ?>">Discover
                                    now</a></p>
                        </div>
                    </div>
                </div>
                <div class="card col-xs-12 col-sm-6 col-lg-4">
                    <div class="row">
                        <figure class="card__item card__item--figure col-xs-12" id="t3-image--card">
                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t3_0" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>

                                <a href="http://jotun-gcc.com.dev02.allegro.no/raw" tabindex="-1">
                                    <picture>
                                        <source data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_Raw_r4-ng4_0854.webp"
                                            type="image/webp" srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_Raw_r4-ng4_0854.webp">
                                        <img class="card__image img-responsive lazyload" src="<?php echo base_url(); ?>/assets/dist/Jotun_Raw_r4-ng4_0854.jpg"
                                            data-src="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_Raw_r4-ng4_0854.jpg"
                                            data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_Raw_r4-ng4_0854.jpg" alt="WALL: JOTUN PURE COLOR 6350 SOFT TEAL"
                                            srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_Raw_r4-ng4_0854.jpg">
                                    </picture>
                                </a>
                            </figure>
                        </figure>
                        <div class="card__item card__body col-xs-12">
                            <h2 class="card__header">
                                <a class="" href="<?php echo $this->config->item("raw_link"); ?>">RAW</a>
                            </h2>
                            <p>Warm colours of soil and sand, peaches and earthy reds. <br class="visible-lg"><a href="<?php echo $this->config->item("raw_link"); ?>">Discover
                                    now</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section--cta text-center js-section-video">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h2>How to use the colour card</h2>
                <p class="lead">This colour card allows you to create harmonic colour combinations. See how to use
                    the new colour card.</p>

                <figure class="column__item column__item--video">
                    <div class="video-container keep-16-9">
                        <span class="video-overlay js-video-overlay" data-bg="<?php echo base_url(); ?>/assets/media/images/forside/gcc-frontpage-video.jpg"
                            data-bg-mobile="<?php echo base_url(); ?>/assets/media/images/forside/gcc-frontpage-video.jpg" style="background-image: url(&quot;<?php echo base_url(); ?>/assets/media/images/forside/gcc-frontpage-video.jpg&quot;);"></span>
                        <iframe class="video js-video" title="How to use the colour card" src="https://www.youtube.com/embed/yi2M0DRXbaw?enablejsapi=1&amp;showinfo=0&amp;rel=0"
                            width="720" height="1280" frameborder="0" allowfullscreen="" id="widget2"></iframe>
                    </div>
                </figure>
            </div>
        </div>
    </section>

    <section class="section section--cta text-center">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h2>Save your favourites</h2>
                <p class="lead">Create a wish list of your favourite colours, inspirational photos and paints from
                    Jotun by pressing <i class="material-icons material-icons--first"></i></p>
                <a class="btn btn--line" href="<?php echo $this->config->item("wishlist_link"); ?>">
                    <span class="btn__text">My favorites
                        <span class="btn--heart">
                            <i class="material-icons material-icons--second"></i>
                            <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span>
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </section>

    <section class="section text-center">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h2>Express yourself with colours</h2>
                <p class="lead">Let the colours and combinations reflect and enhance who you are – your own
                    personal colour identity. Create your interior palette. Pick a colour, explore complementary
                    colours and create your unique interior palette.</p>
                <a href="http://jotun-gcc.com.dev02.allegro.no/palettes.section.image.link">
                    <picture>
                        <source data-srcset="<?php echo base_url(); ?>/assets/media/images/banners/palettes-mockup-calm-no-2.webp" type="image/webp"
                            srcset="<?php echo base_url(); ?>/assets/media/images/banners/palettes-mockup-calm-no-2.webp">
                        <img class="img-responsive lazyload" src="<?php echo base_url(); ?>/<?php echo base_url(); ?>/assets/dist/palettes-mockup-calm-no-2.png" data-src="<?php echo base_url(); ?>/assets/media/images/banners/palettes-mockup-calm-no-2.png"
                            data-srcset="<?php echo base_url(); ?>/assets/media/images/banners/palettes-mockup-calm-no-2.png" alt="Express yourself with colours"
                            srcset="<?php echo base_url(); ?>/assets/media/images/banners/palettes-mockup-calm-no-2.png">
                    </picture>
                </a>
            </div>
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <a class="btn btn--line" href="<?php echo $this->config->item("palette_link"); ?>">
                    <span class="btn__text">Create your personal interior palette</span>
                </a>
            </div>
        </div>
    </section>

    <div class="section section--intro" id="intro">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-xl-offset-2">
                    <div class="row">
                        <figure class="card__item card__item--figure col-sm-6">
                            <figure class="card__image">
                                <picture>
                                    <source data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/_TK_8696.webp" type="image/webp"
                                        srcset="<?php echo base_url(); ?>/assets/media/images/forside/_TK_8696.webp">
                                    <img class="card__image img-responsive lazyload" src="<?php echo base_url(); ?>/assets/dist/_TK_8696.jpg"
                                        data-src="<?php echo base_url(); ?>/assets/media/images/forside/_TK_8696.jpg" data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/_TK_8696.jpg"
                                        alt="Lisbeth Larsen Global Colour Manager Jotun" srcset="<?php echo base_url(); ?>/assets/media/images/forside/_TK_8696.jpg">
                                </picture>
                            </figure>
                            <figcaption class="card__caption sr-only">Lisbeth Larsen Global Colour Manager Jotun</figcaption>
                        </figure>
                        <div class="col-sm-6">
                            <h3>REMEMBER WHO YOU ARE</h3>
                            <p>Life moves fast. A million things happen to each of us every day, changing the way
                                we think and feel in small, sometimes imperceptible ways. Out in the world, our
                                identities are constantly shifting and evolving – sometimes, it can be hard to keep
                                track of who we are and what we like.<br><br> Home is different. Home sets the mood
                                we begin the day with and the tone of the welcome we return to. It should ground us
                                and comfort us – a constant, ever-reliable reminder of what makes us happy. Home is
                                the living story of who we are, told in the objects and colours we fill it with.
                                Create your personal space with colours designed by Jotun.<br><br> This colour card
                                is designed to give you a starting point to create a space for your story. It
                                contains ideas and inspiration for choosing the colours and combinations that
                                reflect and enhance who you are – your own personal colour identity.</p>
                            <div class="endorsement-footer">
                                <span class="signature"></span>
                                <p>Lisbeth Larsen Global Colour Manager Jotun<br> Global Colour Manager Jotun</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main><!-- .site-main -->