<header class="theme">
        <div class="theme__content">
            <div class="theme__column theme__column--image">
                <figure class="theme__image lazyload" data-bg="assets/media/images/t3/Jotun_Raw_r4-ng4_0854.jpg" role="img"
                    aria-label="WALL: JOTUN 20120 ORGANIC RED SKIRTING: JOTUN 20120 ORGANIC RED WINDOW: JOTUN 12078 COMFORT GREY CEILING: RECYCLED MATERIALS"
                    id="t3-image--1" style="background-image: url(&quot;assets/media/images/t3/Jotun_Raw_r4-ng4_0854.jpg&quot;);">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="20120 12078" data-image="assets/media/images/t3/Jotun_Raw_r4-ng4_0854.jpg"
                        data-product="2 14">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figcaption class="card__caption card__caption--color js-card-caption">
                        <p>WALL: JOTUN 20120 <strong>ORGANIC RED</strong><br> SKIRTING: JOTUN 20120 <strong>ORGANIC RED</strong><br>
                            WINDOW: JOTUN 12078 <strong>COMFORT GREY</strong><br> CEILING: RECYCLED MATERIALS</p>
                    </figcaption>
                </figure>

                <div class="theme__bar">
                    <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                        aria-label="Save your favourites" data-favorite="t3_1" style="visibility: visible; animation-name: d;">
                        <i class="material-icons material-icons--first"></i>
                        <i class="material-icons material-icons--second"></i>
                        <span class="sr-only">Save your favourites</span>
                    </button>

                    <div class="theme__caption">
                        <p>WALL: JOTUN 20120 <strong>ORGANIC RED</strong><br> SKIRTING: JOTUN 20120 <strong>ORGANIC RED</strong><br>
                            WINDOW: JOTUN 12078 <strong>COMFORT GREY</strong><br> CEILING: RECYCLED MATERIALS</p>
                    </div>
                </div>
            </div>

            <div class="theme__column theme__column--content">
                <div class="theme__header">
                    <h1 class="theme__title">RAW</h1>
                </div>

                <div class="theme__video js-section-video" id="t3-video--1">
                    <div class="column__item column__item--video">
                        <div class="video-container keep-16-9">
                            <div class="column__caption js-video-caption">
                                <h3>Explore the colours</h3>
                            </div>
                            <span class="video-overlay js-video-overlay" data-bg="assets/media/images/t3/t3-video-2.jpg"
                                data-bg-mobile="assets/media/images/t3/t3-video-2.jpg" style="background-image: url(&quot;assets/media/images/t3/t3-video-2.jpg&quot;);"></span>
                            <iframe class="video js-video" src="https://www.youtube.com/embed/eK6ghltvooI?enablejsapi=1&rel=0&showinfo=0" title="Explore the colours"
                                width="720" height="1280" frameborder="0" allowfullscreen="" id="widget2"></iframe>
                        </div>
                    </div>

                    <div class="theme__text">
                        <p>Deep earthy reds, sensuous peaches, greens and autumnal browns carry a promise of simpler
                            living. A contemporary, boldly feminine palette balances rawness and refinement, the
                            hand-crafted and clean-lined modernity.</p>
                    </div>
                </div>
            </div>
        </div>
    </header>
<main id="main" class="site-main">

        <section class="section section--cta text-center">
            <div class="container">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h2>Save your favourites</h2>
                    <p class="lead">Create a wish list of your favourite colours, inspirational photos and paints from
                        Jotun by pressing <i class="material-icons material-icons--first"></i></p>
                    <a class="btn btn--line" href="<?php echo $this->config->item("whislist_link"); ?>">
                        <span class="btn__text">My favorites
                            <span class="btn--heart">
                                <i class="material-icons material-icons--second"></i>
                                <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </section>

        <div class="section section--image">
            <div class="container">
                <div class="row">
                    <figure class="card__item--figure col-xs-12 image-more" id="t3-image--2">
                        <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                            data-toggle="modal" data-target="#plus" data-colors="12078 2024" data-image="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_ng3-r3_0923.jpg"
                            data-product="2 12">
                            <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                        </button>

                        <figure class="card__image">
                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                aria-label="Save your favourites" data-favorite="t3_2" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Save your favourites</span>
                            </button>

                            <picture>
                                <source data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_ng3-r3_0923.webp" type="image/webp"
                                    srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_ng3-r3_0923.webp">
                                <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_Raw_ng3-r3_0923.jpg" data-src="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_ng3-r3_0923.jpg"
                                    data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_ng3-r3_0923.jpg" alt="WALL: JOTUN 12078 COMFORT GREY WALL: JOTUN 2024 SENSES BED: JOTUN 2024 SENSES WINDOW: JOTUN 12078 COMFORT GREY"
                                    srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_ng3-r3_0923.jpg">
                            </picture>
                        </figure>

                        <figcaption class="card__caption card__caption--col card__caption--color card__caption--right js-card-caption">
                            <p>WALL: JOTUN 12078 <strong>COMFORT GREY</strong><br> WALL: JOTUN 2024 <strong>SENSES</strong><br>
                                BED: JOTUN 2024 <strong>SENSES</strong><br> WINDOW: JOTUN 12078 <strong>COMFORT GREY</strong></p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>

        <div class="section section--text">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                        <p><strong>JOTUN COLOUR CONFIDENCE</strong> Our innovative colour technology offers unmatched
                            colour accuracy. With Jotun Colour Confidence you can be sure that you will get exactly the
                            colour you want.</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="section section--image">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-lg-4">
                        <figure class="card__item--figure image-more" id="t3-image--3">
                            <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                                data-toggle="modal" data-target="#plus" data-colors="20120" data-image="<?php echo base_url(); ?>assets/media/images/t3/Jotun_r4_8015.jpg"
                                data-product="2">
                                <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                            </button>

                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t3_3" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>

                                <picture>
                                    <source data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_r4_8015.webp" type="image/webp"
                                        srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_r4_8015.webp">
                                    <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_r4_8015.jpg" data-src="<?php echo base_url(); ?>assets/media/images/t3/Jotun_r4_8015.jpg"
                                        data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_r4_8015.jpg" alt="WALL: JOTUN 20120 ORGANIC RED"
                                        srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_r4_8015.jpg">
                                </picture>
                            </figure>

                            <figcaption class="card__caption card__caption--color card__caption--col js-card-caption card__caption--nocol">
                                <p>WALL: JOTUN 20120 <strong>ORGANIC RED</strong></p>
                            </figcaption>
                        </figure>

                        <h3 class="product__header">WARM COLOURS OF SOIL</h3>
                        <p>In the world’s urban landscapes, we are less connected to the earth beneath our feet; it’s
                            only natural that we should seek to reground ourselves, creating modern rustic spaces –
                            easy on the eye and stirring to the soul.</p>

                        <div class="color-group">
                            <figure class="color-card  color-card--hex js-color-card u-bg--20118 wow slideRightFade"
                                data-color-id="20118" style="visibility: visible; animation-name: i;">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 20118 <strong>Amber Red</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="20118" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>
                            <figure class="color-card  color-card--hex js-color-card u-bg--12075 wow slideRightFade"
                                data-color-id="12075" style="visibility: visible; animation-name: i;">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12075 <strong>Soothing Beige</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12075" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>
                            <small><a href="<?php echo $this->config->item("raw_link"); ?>#allcolors" class="js-btn-scroll">See
                                    all the colours together at the bottom of the page</a></small>
                        </div>
                    </div>

                    <figure class="card__item--figure col-xs-12 col-lg-8 image-more" id="t3-image--4">
                        <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                            data-toggle="modal" data-target="#plus" data-colors="20118 12075 1622" data-image="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc3-ng3_0893.jpg"
                            data-product="2 13">
                            <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                        </button>

                        <figure class="card__image">
                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                aria-label="Save your favourites" data-favorite="t3_4" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Save your favourites</span>
                            </button>

                            <picture>
                                <source data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc3-ng3_0893.webp" type="image/webp"
                                    srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc3-ng3_0893.webp">
                                <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_Raw_acc3-ng3_0893.jpg"
                                    data-src="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc3-ng3_0893.jpg" data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc3-ng3_0893.jpg"
                                    alt="WALL: JOTUN 20118 AMBER RED WALL: JOTUN 12075 SOOTHING BEIGE SKIRTING: JOTUN 1622 REFLECTION"
                                    srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc3-ng3_0893.jpg">
                            </picture>
                        </figure>

                        <figcaption class="card__caption card__caption--col js-card-caption card__caption--color">
                            <p>WALL: JOTUN 20118 <strong>AMBER RED</strong><br> WALL: JOTUN 12075 <strong>SOOTHING
                                    BEIGE</strong><br> SKIRTING: JOTUN 1622 <strong>REFLECTION</strong></p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>

        <div class="section section--grow">
            <div class="container">
                <div class="row">
                    <div class="color-group hidden-xs hidden-sm">
                        <figure class="color-card  color-card--hex js-color-card u-bg--7613 wow slideRightFade"
                            data-color-id="7613" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 7613 <strong>Northern Mystic</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="7613" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                        <figure class="color-card  color-card--hex js-color-card u-bg--12078 wow slideRightFade"
                            data-color-id="12078" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="12078" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                        <figure class="color-card  color-card--hex js-color-card u-bg--2024 wow slideRightFade"
                            data-color-id="2024" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 2024 <strong>Senses</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="2024" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                        <figure class="color-card  color-card--hex js-color-card u-bg--20120 wow slideRightFade"
                            data-color-id="20120" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 20120 <strong>Organic Red</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="20120" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>

                        <small><a href="<?php echo $this->config->item("raw_link"); ?>#allcolors" class="js-btn-scroll">See
                                all the colours together at the bottom of the page</a></small>
                    </div>

                    <figure class="card__item--figure image-more" id="t3-image--5">
                        <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                            data-toggle="modal" data-target="#plus" data-colors="7613" data-image="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc4_0988.jpg"
                            data-product="2">
                            <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                        </button>

                        <figure class="card__image">
                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                aria-label="Save your favourites" data-favorite="t3_5" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Save your favourites</span>
                            </button>

                            <picture>
                                <source data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc4_0988.webp" type="image/webp"
                                    srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc4_0988.webp">
                                <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_Raw_acc4_0988.jpg" data-src="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc4_0988.jpg"
                                    data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc4_0988.jpg" alt="WALL: JOTUN 7613 NORTHERN MYSTIC"
                                    srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc4_0988.jpg">
                            </picture>
                        </figure>

                        <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                            <p>WALL: JOTUN 7613 <strong>NORTHERN MYSTIC</strong></p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>

        <div class="section section--image">
            <div class="container">
                <div class="row">
                    <figure class="card__item--figure col-xs-12 image-more" id="t3-image--6">
                        <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                            data-toggle="modal" data-target="#plus" data-colors="7628" data-image="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_g3_1033.jpg"
                            data-product="12">
                            <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                        </button>

                        <figure class="card__image">
                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                aria-label="Save your favourites" data-favorite="t3_6" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Save your favourites</span>
                            </button>

                            <picture>
                                <source data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_g3_1033.webp" type="image/webp"
                                    srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_g3_1033.webp">
                                <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_Raw_g3_1033.jpg" data-src="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_g3_1033.jpg"
                                    data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_g3_1033.jpg" alt="PANEL WALL: JOTUN 7628 TREASURE"
                                    srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_g3_1033.jpg">
                            </picture>
                        </figure>

                        <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                            <p>PANEL WALL: JOTUN 7628 <strong>TREASURE</strong></p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>

        <div class="section section--text">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                        <p>Majestic Perfect Beauty & Care brings enduring colour and a flawless sheen to your walls. With minimal emissions and a low-odour composition, they're as easy to apply as they are to look at.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="section section--product" id="t3-product--1">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                        <div class="product__body">
                            <h3 class="product__title">MAJESTIC PERFECT BEAUTY AND CARE</h3>
                            <figure class="product__image">
                                <picture>
                                    <source data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint1.png" type="image/png" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint1.png">
                                    <img class="img-responsive lazyload" src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint1.png" data-src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint1.png"
                                        data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint1.png" alt="FENOMASTIC Wonderwall" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint1.png">
                                </picture>

                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Add the product to your favourites"
                                    aria-label="Add the product to your favourites" data-favorite="product_3" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the product to your favourites</span>
                                </button>
                            </figure>

                            <p class="product__tag">Long-lasting looks for a healthy home</p>

                            <div class="product__btn">
                                <a href="http://jotun-gcc.com.dev02.allegro.no/products" class="btn btn--line">
                                    <span class="btn__text">Learn about the product</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section section--colors visible-xs visible-sm">
            <div class="container">
                <div class="row">
                    <div class="color-group col-xs-12 col-lg-6">
                        <figure class="color-card  color-card--hex js-color-card u-bg--7613 wow slideRightFade animated"
                            data-color-id="7613" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 7613 <strong>Northern Mystic</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite animated" type="button"
                                title="Add the colour chip to your favourites" data-favorite="7613" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                        <figure class="color-card  color-card--hex js-color-card u-bg--12078 wow slideRightFade animated"
                            data-color-id="12078" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite animated" type="button"
                                title="Add the colour chip to your favourites" data-favorite="12078" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                        <figure class="color-card  color-card--hex js-color-card u-bg--2024 wow slideRightFade animated"
                            data-color-id="2024" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 2024 <strong>Senses</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite animated" type="button"
                                title="Add the colour chip to your favourites" data-favorite="2024" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>

                        <small><a href="<?php echo $this->config->item("raw_link"); ?>#allcolors" class="js-btn-scroll">See
                                all the colours together at the bottom of the page</a></small>
                    </div>
                </div>
            </div>
        </div>

        <div class="section">
            <div class="container">
                <div class="row row--table-lg">
                    <div class="col-xs-12 col-lg-6 col--cell-lg">
                        <figure class="card__item--figure image-more" id="t3-image--7">
                            <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                                data-toggle="modal" data-target="#plus" data-colors="12074" data-image="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc2_0992.jpg"
                                data-product="2">
                                <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                            </button>

                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t3_7" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>

                                <picture>
                                    <source data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc2_0992.webp" type="image/webp"
                                        srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc2_0992.webp">
                                    <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_Raw_acc2_0992.jpg"
                                        data-src="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc2_0992.jpg" data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc2_0992.jpg"
                                        alt="WALL: JOTUN 12074 PEACHY" srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_acc2_0992.jpg">
                                </picture>
                            </figure>

                            <figcaption class="card__caption card__caption--col card__caption--color js-card-caption card__caption--nocol">
                                <p>WALL: JOTUN 12074 <strong>PEACHY</strong></p>
                            </figcaption>
                        </figure>

                        <figure class="card__item--figure image-more u-nm" id="t3-image--8">
                            <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                                data-toggle="modal" data-target="#plus" data-colors="12078" data-image="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_ng3_1580.jpg"
                                data-product="2">
                                <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                            </button>

                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t3_8" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>

                                <picture>
                                    <source data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_ng3_1580.webp" type="image/webp"
                                        srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_ng3_1580.webp">
                                    <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_thelab_ng3_1580.jpg"
                                        data-src="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_ng3_1580.jpg" data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_ng3_1580.jpg"
                                        alt="WALL: JOTUN 12078 COMFORT GREY" srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_ng3_1580.jpg">
                                </picture>
                            </figure>

                            <figcaption class="card__caption card__caption--color js-card-caption">
                                <p>WALL: JOTUN 12078 <strong>COMFORT GREY</strong></p>
                            </figcaption>
                        </figure>
                    </div>

                    <div class="col-lg-6 col--cell-lg align-bottom">
                        <div class="row">
                            <div class="col-xs-12" id="t3-text">
                                <h3>RUSTIC ELEGANCE</h3>
                                <p>The raw home is crafted with artisanal honesty, celebrating
                                    the beauty of natural materials – the unprocessed and the
                                    imperfect. Earthbound neutrals meet and enhance more vibrant
                                    nuances, creating strong lines, fresh looks, and spaces that
                                    nourish creative possibility.</p>
                            </div>

                            <figure class="card__item--figure col-xs-12 image-more" id="t3-image--9">

                                <figure class="card__image">

                                    <picture>
                                        <source data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_1126.webp" type="image/webp"
                                            srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_1126.webp">
                                        <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_thelab_1126.jpg"
                                            data-src="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_1126.jpg" data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_1126.jpg"
                                            alt="t3.image.9.text.1" srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_1126.jpg">
                                    </picture>
                                </figure>

                            </figure>

                            <figure class="card__item--figure col-xs-12 image-more" id="t3-image--10">
                                <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                                    data-toggle="modal" data-target="#plus" data-colors="20120" data-image="<?php echo base_url(); ?>assets/media/images/t3/Jotun_r4_8267.jpg"
                                    data-product="4">
                                    <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                                </button>

                                <figure class="card__image">
                                    <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                        aria-label="Save your favourites" data-favorite="t3_10" style="visibility: visible; animation-name: d;">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Save your favourites</span>
                                    </button>
                                    <picture>
                                        <source data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_r4_8267.webp" type="image/webp"
                                            srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_r4_8267.webp">
                                        <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_r4_8267.jpg" data-src="<?php echo base_url(); ?>assets/media/images/t3/Jotun_r4_8267.jpg"
                                            data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_r4_8267.jpg" alt="WALL: JOTUN 20120 ORGANIC RED"
                                            srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_r4_8267.jpg">
                                    </picture>
                                </figure>

                                <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                                    <p>WALL: JOTUN 20120 <strong>ORGANIC RED</strong></p>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section section--text">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                        <p>Create earthy, nature-inspired design styles with a shimmer of luxury. Majestic Design Prestige introduces a sleek metallic sheen to your walls, lifting simple spaces with a distinctly regal touch</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="section section--product" id="t3-product--2">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                        <div class="product__body">
                            <h3 class="product__title">MAJESTIC DESIGN PRESTIGE</h3>
                            <figure class="product__image">
                                <picture>
                                    <source data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint6.png" type="image/png" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint6.png">
                                    <img class="img-responsive lazyload" src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint6.png" data-src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint6.png"
                                        data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint6.png" alt="Fenomastic My Home Rich Matt"
                                        srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint6.png">
                                </picture>

                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Add the product to your favourites"
                                    aria-label="Add the product to your favourites" data-favorite="product_4" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the product to your favourites</span>
                                </button>
                            </figure>

                            <p class="product__tag">A rustic look with a luxurious finish</p>

                            <div class="product__btn">
                                <a href="http://jotun-gcc.com.dev02.allegro.no/products" class="btn btn--line">
                                    <span class="btn__text">Learn about the product</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section section--image">
            <div class="container">
                <div class="row">
                    <figure class="card__item--figure image-more col-xs-12" id="t3-image--11">
                        <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                            data-toggle="modal" data-target="#plus" data-colors="12085 12078 1622" data-image="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_a4-ng3_0878.jpg"
                            data-product="2 12">
                            <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                        </button>

                        <figure class="card__image">
                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                aria-label="Save your favourites" data-favorite="t3_11" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Save your favourites</span>
                            </button>

                            <picture>
                                <source data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_a4-ng3_0878.webp" type="image/webp"
                                    srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_a4-ng3_0878.webp">
                                <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_Raw_a4-ng3_0878.jpg" data-src="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_a4-ng3_0878.jpg"
                                    data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_a4-ng3_0878.jpg" alt="WALL: JOTUN 12085 RURAL WINDOW AND DOOR: JOTUN 12078 COMFORT GREY SKIRTING: JOTUN 1622 REFLEKSJON"
                                    srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_a4-ng3_0878.jpg">
                            </picture>
                        </figure>

                        <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                            <p>WALL: JOTUN 12085 <strong>RURAL</strong><br> WINDOW AND DOOR: JOTUN 12078 <strong>COMFORT
                                    GREY</strong><br> SKIRTING: JOTUN 1622 <strong>REFLEKSJON</strong></p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>

        <div class="section section--image">
            <div class="container">
                <div class="row">
                    <figure class="card__item--figure image-more col-xs-12" id="t3-image--12">
                        <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                            data-toggle="modal" data-target="#plus" data-colors="2024 20120" data-image="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_r3-r4_0908.jpg"
                            data-product="2 12">
                            <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                        </button>

                        <figure class="card__image">
                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                aria-label="Save your favourites" data-favorite="t3_12" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Save your favourites</span>
                            </button>

                            <picture>
                                <source data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_r3-r4_0908.webp" type="image/webp"
                                    srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_r3-r4_0908.webp">
                                <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_Raw_r3-r4_0908.jpg" data-src="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_r3-r4_0908.jpg"
                                    data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_r3-r4_0908.jpg" alt="WALL: JOTUN 2024 SENSES DECORATION: JOTUN 20120 ORGANIC RED"
                                    srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_Raw_r3-r4_0908.jpg">
                            </picture>
                        </figure>

                        <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                            <p>WALL: JOTUN 2024 <strong>SENSES</strong><br> DECORATION: JOTUN 20120 <strong>ORGANIC RED</strong></p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>

        <div class="section section--text">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                        <p><strong>HARMONIES</strong> Subtle transitions and shifts of nuance within a single colour
                            family create the most delicate surroundings.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="section section--image">
            <div class="container">
                <div class="row">
                    <figure class="card__item--figure image-more col-xs-12" id="t3-image--13">
                        <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                            data-toggle="modal" data-target="#plus" data-colors="2024-01" data-image="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_meia3_1632.jpg"
                            data-product="1 12">
                            <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                        </button>

                        <figure class="card__image">
                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                aria-label="Save your favourites" data-favorite="t3_13" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Save your favourites</span>
                            </button>

                            <picture>
                                <source data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_meia3_1632.webp" type="image/webp"
                                    srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_meia3_1632.webp">
                                <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_thelab_meia3_1632.jpg"
                                    data-src="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_meia3_1632.jpg" data-srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_meia3_1632.jpg"
                                    alt="WALL: JOTUN 2024 SENSES PEDESTALS: JOTUN 2024 SENSES" srcset="<?php echo base_url(); ?>assets/media/images/t3/Jotun_thelab_meia3_1632.jpg">
                            </picture>
                        </figure>

                        <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                            <p>WALL: JOTUN 2024 <strong>SENSES</strong><br> PEDESTALS: JOTUN 2024 <strong>SENSES</strong></p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
        <div class="section section--text">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                        <p>For rich matt colours engineered to withstand the rigours of real life, Majestic True Beauty Matt provides a pristine finish and smoothness that enriches walls and endures year after year.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="section section--product" id="t3-product--3">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                        <div class="product__body">
                            <h3 class="product__title">MAJESTIC TRUE BEAUTY MATT</h3>
                            <figure class="product__image">
                                <picture>
                                    <source data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png" type="image/png" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png">
                                    <img class="img-responsive lazyload" src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png" data-src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png"
                                        data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png" alt="Lady Design Romano" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png">
                                </picture>

                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Add the product to your favourites"
                                    aria-label="Add the product to your favourites" data-favorite="product_1" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the product to your favourites</span>
                                </button>
                            </figure>

                            <p class="product__tag">True, deep colour designed to last</p>

                            <div class="product__btn">
                                <a href="http://jotun-gcc.com.dev02.allegro.no/products" class="btn btn--line">
                                    <span class="btn__text">Learn about the product</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="section section--cta text-center">
            <div class="container">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h2>Colour your own space</h2>
                    <p class="lead">Choosing the right colours for your home might seem a challenging and
                        time-consuming task, but Jotun’s new Colour Design app makes the process a breeze. Down load
                        now from <a href="https://itunes.apple.com/no/app/jotun-colourdesign/id1312722535?mt=8">App
                            Store</a> eller <a href="https://play.google.com/store/apps/details?id=com.jotun.colourdesign">Google
                            Play.</a></p>
                </div>
            </div>
        </section>

        <div class="section section--video js-section-video" id="mood">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-8 col-md-offset-2">
                        <h2>Get the Raw look</h2>
                        <p>Create your personal space with colours designed by Jotun. Get ideas and inspiration on how
                            to use and combine colours from the RAW colour theme.</p>

                        <div class="column__item column__item--video">
                            <div class="video-container keep-16-9">
                                <span class="video-overlay js-video-overlay" data-bg="<?php echo base_url(); ?>assets/media/images/t3/video-2-raw.jpg"
                                    data-bg-mobile="<?php echo base_url(); ?>assets/media/images/t3/video-2-raw.jpg" style="background-image: url(&quot;<?php echo base_url(); ?>assets/media/images/t3/video-2-raw.jpg&quot;);"></span>
                                <iframe class="video js-video" title="Create your personal space with colours designed by Jotun. Get ideas and inspiration on how to use and combine colours from the RAW colour theme."
                                    src="<?php echo base_url(); ?>assets/dist/t3.mood.html" width="720" height="1280" frameborder="0" allowfullscreen=""
                                    id="widget4"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section section--colors" id="allcolors">
            <div class="container">
                <div class="row">
                    <div class="color-group col-xs-12 col-md-6">
                        <figure class="color-card  color-card--hex js-color-card u-bg--12078 wow slideRightFade"
                            data-color-id="12078" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="12078" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                    </div>
                    <div class="color-group col-xs-12 col-md-6">
                        <figure class="color-card  color-card--hex js-color-card u-bg--0394 wow slideRightFade"
                            data-color-id="0394" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 0394 <strong>SOFT GREY</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="0394" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                    </div>
                    <div class="color-group col-xs-12 col-md-6">
                        <figure class="color-card  color-card--hex js-color-card u-bg--12084 wow slideRightFade"
                            data-color-id="12084" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 12084 <strong>Dusky Peach</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="12084" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                    </div>
                    <div class="color-group col-xs-12 col-md-6">
                        <figure class="color-card  color-card--hex js-color-card u-bg--12085 wow slideRightFade"
                            data-color-id="12085" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 12085 <strong>Rural</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="12085" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                    </div>
                    <div class="color-group col-xs-12 col-md-6">
                        <figure class="color-card  color-card--hex js-color-card u-bg--7628 wow slideRightFade"
                            data-color-id="7628" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 7628 <strong>Treasure</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="7628" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                    </div>
                    <div class="color-group col-xs-12 col-md-6">
                        <figure class="color-card  color-card--hex js-color-card u-bg--7629 wow slideRightFade"
                            data-color-id="7629" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 7629 <strong>Antique Green</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="7629" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                    </div>
                    <div class="color-group col-xs-12 col-md-6">
                        <figure class="color-card  color-card--hex js-color-card u-bg--12086 wow slideRightFade"
                            data-color-id="12086" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 12086 <strong>Rustic Pink</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="12086" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                    </div>
                    <div class="color-group col-xs-12 col-md-6">
                        <figure class="color-card  color-card--hex js-color-card u-bg--2024 wow slideRightFade"
                            data-color-id="2024" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 2024 <strong>Senses</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="2024" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                    </div>
                    <div class="color-group col-xs-12 col-md-6">
                        <figure class="color-card  color-card--hex js-color-card u-bg--20120 wow slideRightFade"
                            data-color-id="20120" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 20120 <strong>Organic Red</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="20120" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                    </div>
                    <div class="color-group col-xs-12 col-md-6">
                        <figure class="color-card  color-card--hex js-color-card u-bg--7613 wow slideRightFade"
                            data-color-id="7613" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 7613 <strong>Northern Mystic</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="7613" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                    </div>
                    <div class="color-group col-xs-12 col-md-6">
                        <figure class="color-card  color-card--hex js-color-card u-bg--12074 wow slideRightFade"
                            data-color-id="12074" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 12074 <strong>Peachy</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="12074" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                    </div>
                    <div class="color-group col-xs-12 col-md-6">
                        <figure class="color-card  color-card--hex js-color-card u-bg--20118 wow slideRightFade"
                            data-color-id="20118" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 20118 <strong>Amber Red</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="20118" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                    </div>
                </div>
                <div class="row text-center">
                    <p>While we have made every effort to make the colours on screen as close as possible to physical
                        painted samples, please be aware that colours will vary depending on your screen settings and
                        resolution.</p>
                </div>
            </div>
        </div>

        <section class="section section--cta text-center">
            <div class="container">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h2>How to use the colours</h2>
                    <p class="lead">More about how to use the colours of the Raw palette.</p>
                    <a class="btn btn--line" href="<?php echo $this->config->item("raw_link"); ?>#colors-t3" data-toggle="modal"
                        data-target="#colors-t3">
                        <span class="btn__text">Raw colours</span>
                    </a>
                    <a class="btn btn--line" href="<?php echo $this->config->item("colour_link"); ?>">
                        <span class="btn__text">See all 28 colours</span>
                    </a>
                </div>
            </div>
        </section>

        <section class="section text-center">
            <div class="container">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h2>Express yourself with colours</h2>
                    <p class="lead">Let the colours and combinations reflect and enhance who you are – your own
                        personal colour identity. Create your interior palette. Pick a colour, explore complementary
                        colours and create your unique interior palette.</p>
                    <a href="http://jotun-gcc.com.dev02.allegro.no/palettes.section.image.link">
                        <picture>
                            <source data-srcset="<?php echo base_url(); ?>assets/media/images/banners/palettes-mockup-raw-no-2.webp" type="image/webp"
                                srcset="<?php echo base_url(); ?>assets/media/images/banners/palettes-mockup-raw-no-2.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/palettes-mockup-raw-no-2.png" data-src="<?php echo base_url(); ?>assets/media/images/banners/palettes-mockup-raw-no-2.png"
                                data-srcset="<?php echo base_url(); ?>assets/media/images/banners/palettes-mockup-raw-no-2.png" alt="Express yourself with colours"
                                srcset="<?php echo base_url(); ?>assets/media/images/banners/palettes-mockup-raw-no-2.png">
                        </picture>
                    </a>
                </div>
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <a class="btn btn--line" href="<?php echo $this->config->item("palette_link"); ?>">
                        <span class="btn__text">Create your personal interior palette</span>
                    </a>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row">
                    <h2 class="text-center">More colour palettes</h2>
                    <div class="card col-xs-12 col-sm-6 col-lg-4 col-lg-offset-2 ">
                        <div class="row">
                            <figure class="card__item card__item--figure col-xs-12" id="t1-image--card">
                                <figure class="card__image">
                                    <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                        aria-label="Save your favourites" data-favorite="t1_0" style="visibility: visible; animation-name: d;">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Save your favourites</span>
                                    </button>

                                    <a href="<?php echo $this->config->item("calm_link"); ?>" tabindex="-1">
                                        <picture>
                                            <source data-srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_calm_a2_0531.webp"
                                                type="image/webp" srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_calm_a2_0531.webp">
                                            <img class="card__image img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_calm_a2_0531.jpg"
                                                data-src="<?php echo base_url(); ?>assets/media/images/forside/Jotun_calm_a2_0531.jpg"
                                                data-srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_calm_a2_0531.jpg" alt="WALL: JOTUN 1877 WARM GREY"
                                                srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_calm_a2_0531.jpg">
                                        </picture>
                                    </a>
                                </figure>
                            </figure>
                            <div class="card__item card__body col-xs-12">
                                <h2 class="card__header">
                                    <a class="" href="<?php echo $this->config->item("calm_link"); ?>">CALM</a>
                                </h2>
                                <p>Soft neutral tones and warm, subtle contrasts. <br class="visible-lg"><a href="<?php echo $this->config->item("calm_link"); ?>">Discover
                                        now</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="card col-xs-12 col-sm-6 col-lg-4 ">
                        <div class="row">
                            <figure class="card__item card__item--figure col-xs-12" id="t2-image--card">
                                <figure class="card__image">
                                    <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                        aria-label="Save your favourites" data-favorite="t2_0" style="visibility: visible; animation-name: d;">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Save your favourites</span>
                                    </button>

                                    <a href="<?php echo $this->config->item("refined_link"); ?>" tabindex="-1">
                                        <picture>
                                            <source data-srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_moderne_acc1_0590.webp"
                                                type="image/webp" srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_moderne_acc1_0590.webp">
                                            <img class="card__image img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_moderne_acc1_0590.jpg"
                                                data-src="<?php echo base_url(); ?>assets/media/images/forside/Jotun_moderne_acc1_0590.jpg"
                                                data-srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_moderne_acc1_0590.jpg"
                                                alt="CEILING: JOTUN PERFECTION 1453 COTTON BALL PANEL WALL: JOTUN SUPREME FINISH MATT 10580 SOFT SKIN WALL (IN FRONT): JOTUN PURE COLOR 20046 SAVANNA SUNSET SKIRTING: JOTUN SUPREME FINISH MATT 20046 SAVANNA SUNSET"
                                                srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_moderne_acc1_0590.jpg">
                                        </picture>
                                    </a>
                                </figure>
                            </figure>
                            <div class="card__item card__body col-xs-12">
                                <h2 class="card__header">
                                    <a class="" href="<?php echo $this->config->item("refined_link"); ?>">REFINED</a>
                                </h2>
                                <p>A vibrant palette of greens and yellows. <br class="visible-lg"><a href="<?php echo $this->config->item("refined_link"); ?>">Discover
                                        now</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Modal -->
        <div class="modal modal--plus fade" id="plus" tabindex="-1" role="dialog" aria-labelledby="modal-label">
            <div class="modal-dialog" role="document">
                <div class="modal-slider col-md-6">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <figure class="background-image modal-header lazyload js-modal-image">
                        <div class="modal-caption js-modal-caption"></div>
                    </figure>
                </div>
                <div class="modal-info">
                    <i class="material-icons zoom js-zoom" tabindex="0"></i>
                    <div class="js-modal-info"></div>
                </div>
                <div class="modal-content col-md-6">
                    <div class="container">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 color-group js-modal-colors"></div>
                                <div class="col-xs-12 js-modal-products"></div>
                            </div>
                        </div>
                        <div class="modal-footer text-center">
                            <div class="buttons js-modal-buttons">
                                <div class="btn-row">
                                    <a class="btn btn--line" href="<?php echo $this->config->item("whislist_link"); ?>">
                                        <span class="btn__text">My favourites <span class="btn--heart"><i class="material-icons material-icons--second"></i>
                                                <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span></span></span>
                                    </a>
                                    <a class="btn btn--line" href="http://jotun-gcc.com.dev02.allegro.no/products">
                                        <span class="btn__text">Choose the right product</span>
                                    </a>
                                </div>
                                <div class="btn-row">
                                <div class="btn btn--line btn--facebook js-btn-facebook fb-share-button" data-href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/raw" data-layout="button" data-size="large" data-mobile-iframe="true">
                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com%2Fraw&amp;src=sdkpreparse" ><i class="icon icon--facebook"></i> Share to Facebook</a>
                                </div>
                                    <button class="btn btn--line btn--pinterest pinterest-pin-it"><i class="icon icon--pinterest"></i>Pin
                                        to Pinterest</button>
                                </div>
                                <div class="btn-row">
                                    <button type="button" class="btn btn--line" data-dismiss="modal">
                                        <span class="btn__text">Back</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal modal--colors fade" id="colors-t3" tabindex="-1" role="dialog" aria-labelledby="modal-label">
            <div class="modal-dialog" role="document">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="modal-content">
                    <div class="modal-body">

                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                                    <h2 class="text-center">How to use the Raw colours</h2>

                                    <figure class="color-card color-card--hex js-color-card u-bg--12078" data-color-id="12078">
                                        <figcaption class="color-card__caption">
                                            <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                                        </figcaption>

                                        <div class="color-card__helper">
                                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                                <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                    in fullscreen</span>
                                            </button>
                                        </div>

                                        <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                            data-favorite="12078">
                                            <i class="material-icons material-icons--first"></i>
                                            <i class="material-icons material-icons--second"></i>
                                            <span class="sr-only">Add the colour chip to your favourites</span>
                                        </button>
                                    </figure>

                                    <p class="color-card__description">A warm grey nuance. 12078 Comfort Grey is a
                                        brighter version of the popular 0394 Soft Grey. This colour looks quite cool
                                        against contrasts such as the red tones 20120 Organic Red or 20118 Amber Red,
                                        check it also out against the darker green tone 7629 Antique Green. Blue tones
                                        such as 4618 Evening Light, 4638 Elegant Blue, 4477 Deco Blue and 4744
                                        Sophisticated Blue works harmoniously with this grey nuance, as do the warm
                                        greys 12077 Sheer Grey, 1352 Form, 10429 Discrete, 10853 Velvet Grey and 10249
                                        Vandyke Brown. <br><strong>FIND THE RIGHT WHITE:</strong> 12078 Comfort Grey
                                        works perfect with pure white colours 7236 Jazz, 9918 Classic White or 1624
                                        Skylight, but may also be combined with 1001 Egg White and 1453 Cotton Ball.</p>
                                    <figure class="color-card color-card--hex js-color-card u-bg--0394" data-color-id="0394">
                                        <figcaption class="color-card__caption">
                                            <p>JOTUN 0394 <strong>SOFT GREY</strong></p>
                                        </figcaption>

                                        <div class="color-card__helper">
                                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                                <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                    in fullscreen</span>
                                            </button>
                                        </div>

                                        <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                            data-favorite="0394">
                                            <i class="material-icons material-icons--first"></i>
                                            <i class="material-icons material-icons--second"></i>
                                            <span class="sr-only">Add the colour chip to your favourites</span>
                                        </button>
                                    </figure>

                                    <p class="color-card__description">A warm grey nuance. This is a suitable grey tone
                                        if you are looking for warm grey nuances in your house. This has been a Jotun
                                        favourite among greys for many years! Works well with other grey nuances such
                                        as 12077 Sheer Grey, 1024 Timeless, 1352 Form, 10679 Washed Linen or 1376 Mist.
                                        Try it out with green tones such as 8494 Organic Green, 8469 Green Leaf and
                                        8252 Green Harmony, - it looks great! Against pink nuances such as 20046
                                        Savanna Sunset, 20047 Blushing Peach, 2024 Senses or 2771 Rustic Terracotta it
                                        will also appear quite cool. <br><strong>FIND THE RIGHT WHITE:</strong> 0394
                                        Soft Grey functions well with 9918 Classic White, 1624 Skylight, 1001 Egg White
                                        and 1453 Cotton Ball.</p>
                                    <figure class="color-card color-card--hex js-color-card u-bg--12084" data-color-id="12084">
                                        <figcaption class="color-card__caption">
                                            <p>JOTUN 12084 <strong>Dusky Peach</strong></p>
                                        </figcaption>

                                        <div class="color-card__helper">
                                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                                <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                    in fullscreen</span>
                                            </button>
                                        </div>

                                        <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                            data-favorite="12084">
                                            <i class="material-icons material-icons--first"></i>
                                            <i class="material-icons material-icons--second"></i>
                                            <span class="sr-only">Add the colour chip to your favourites</span>
                                        </button>
                                    </figure>

                                    <p class="color-card__description">A subdued peach nuance. The colour is golden,
                                        more like an apricot/peach nuance and not a traditional pink. In many ways this
                                        is a yellowy version of the colour 2024 Senses, known to many. 12084 Dusky
                                        Peach appears new and exciting against the green nuance 7628 Treasure, and to
                                        anyone who enjoys a fresh accent, 20118 Amber Red will be a fun element! The
                                        colour will appear soft and warm when combined with the other peach nuances
                                        12083 Devine og 12085 Rural. It works well with brighter colours such as 1024
                                        Timeless and 10679 Washed Linen. <br><strong>FIND THE RIGHT WHITE:</strong>
                                        12084 Dusky Peach functions well with 9918 Classic White, 1624 Skylight, 1001
                                        Egg White and 1453 Cotton Ball.</p>
                                    <figure class="color-card color-card--hex js-color-card u-bg--12085" data-color-id="12085">
                                        <figcaption class="color-card__caption">
                                            <p>JOTUN 12085 <strong>Rural</strong></p>
                                        </figcaption>

                                        <div class="color-card__helper">
                                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                                <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                    in fullscreen</span>
                                            </button>
                                        </div>

                                        <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                            data-favorite="12085">
                                            <i class="material-icons material-icons--first"></i>
                                            <i class="material-icons material-icons--second"></i>
                                            <span class="sr-only">Add the colour chip to your favourites</span>
                                        </button>
                                    </figure>

                                    <p class="color-card__description">A reddish brown peach nuance. The name implies
                                        countryside colours, a mixture of reddish brown earth and golden, warm
                                        sunshine. The colour is like a hybrid, somewhere in the middle between red,
                                        pink and orange. For anyone wanting to pursue these nuances, it may be combined
                                        with like-minded colours such as 12084 Dusky Peach, or with the stylish and
                                        pure version 12074 Peachy. Green nuances such as 7613 Northern Mystic and 8469
                                        Green Leaf are exciting combinations for the daring. A more neutral base, such
                                        as 12075 Soothing Beige, 10678 Space and 12076 Modern Beige is, however, also a
                                        lovely combination. <br><strong>FIND THE RIGHT WHITE:</strong> 2085 Rural
                                        functions well with 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453
                                        Cotton Ball.</p>
                                    <figure class="color-card color-card--hex js-color-card u-bg--7628" data-color-id="7628">
                                        <figcaption class="color-card__caption">
                                            <p>JOTUN 7628 <strong>Treasure</strong></p>
                                        </figcaption>

                                        <div class="color-card__helper">
                                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                                <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                    in fullscreen</span>
                                            </button>
                                        </div>

                                        <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                            data-favorite="7628">
                                            <i class="material-icons material-icons--first"></i>
                                            <i class="material-icons material-icons--second"></i>
                                            <span class="sr-only">Add the colour chip to your favourites</span>
                                        </button>
                                    </figure>

                                    <p class="color-card__description">A subdued green nuance. The colour appears as
                                        more golden and thus more greenish than 7163 Minty Breeze, which is known to
                                        many. It combines beautifully with cool white tones such as 7236 Jazz or the
                                        greyish white 8394 White Poetry. Blue is a colour that may work well as a
                                        coloured contrast, and the greenish blues 5030 St. Pauls Blue and 5180 Oslo are
                                        preferable. The colour will appear smashing against bright peach nuances such
                                        as 12083 Devine, but it also works well with the cooler pink shades. A trendy
                                        contrast may be achieved with a quite dark green nuance such as 7613 Northern
                                        Mystic.<br><strong>FIND THE RIGHT WHITE:</strong> 7628 Treasure combines
                                        perfectly with the pure white nuances 7236 Jazz, 9918 Classic White or 1624
                                        Skylight, but may also work well with 1001 Egg White and 1453 Cotton Ball.</p>
                                    <figure class="color-card color-card--hex js-color-card u-bg--7629" data-color-id="7629">
                                        <figcaption class="color-card__caption">
                                            <p>JOTUN 7629 <strong>Antique Green</strong></p>
                                        </figcaption>

                                        <div class="color-card__helper">
                                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                                <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                    in fullscreen</span>
                                            </button>
                                        </div>

                                        <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                            data-favorite="7629">
                                            <i class="material-icons material-icons--first"></i>
                                            <i class="material-icons material-icons--second"></i>
                                            <span class="sr-only">Add the colour chip to your favourites</span>
                                        </button>
                                    </figure>

                                    <p class="color-card__description">A subdued green nuance. It appears cooler and
                                        more bluish than the golden greens 8252 Green Harmony or 8469 Green Leaf, known
                                        to many. 7629 Antique Green is lovely against the brighter versions 7628
                                        Treasure and 7627 Refresh. Subdued greens such as this also combine
                                        harmoniously with this years´ peach and yellow tones - a more eccentric way of
                                        combining different colours. If you like blue, 5180 Oslo fits nicely. <br><strong>FIND
                                            THE RIGHT WHITE:</strong> 7629 Antique Green appears at its best against
                                        pure white tones 7236 Jazz, 9918 Classic White or 1624 Skylight, but may
                                        function well against 1001 Egg White and 1453 Cotton Ball.</p>
                                    <figure class="color-card color-card--hex js-color-card u-bg--12086" data-color-id="12086">
                                        <figcaption class="color-card__caption">
                                            <p>JOTUN 12086 <strong>Rustic Pink</strong></p>
                                        </figcaption>

                                        <div class="color-card__helper">
                                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                                <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                    in fullscreen</span>
                                            </button>
                                        </div>

                                        <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                            data-favorite="12086">
                                            <i class="material-icons material-icons--first"></i>
                                            <i class="material-icons material-icons--second"></i>
                                            <span class="sr-only">Add the colour chip to your favourites</span>
                                        </button>
                                    </figure>

                                    <p class="color-card__description">A subdued pink tone. The colour is somewhat
                                        golden. We may say that 10286 Rustic Pink is a brighter version of the
                                        well-known 2024 Senses - they are beautiful together. Also try it with 20120
                                        Organic Red or 20119 Transparent Pink, <br><strong>FIND THE RIGHT WHITE:</strong>
                                        12086 Rustic Pink functions well with 9918 Classic White, 1624 Skylight, 1001
                                        Egg White. 1453 Cotton Ball may turn in a golden direction when combined with
                                        10286 Rustic Pink.</p>
                                    <figure class="color-card color-card--hex js-color-card u-bg--2024" data-color-id="2024">
                                        <figcaption class="color-card__caption">
                                            <p>JOTUN 2024 <strong>Senses</strong></p>
                                        </figcaption>

                                        <div class="color-card__helper">
                                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                                <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                    in fullscreen</span>
                                            </button>
                                        </div>

                                        <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                            data-favorite="2024">
                                            <i class="material-icons material-icons--first"></i>
                                            <i class="material-icons material-icons--second"></i>
                                            <span class="sr-only">Add the colour chip to your favourites</span>
                                        </button>
                                    </figure>

                                    <p class="color-card__description">A pink powder nuance. This is a golden pink
                                        nuance that will appear as both delicate and subdued. It is lovely when
                                        combined with brighter variations such as 10580 Soft Skin, 20119 Transparent
                                        Pink and 12086 Devine. This colour appears as quite cool when combined with
                                        burnt terracotta red nuances such as 2771 Rustic Terracotta, 2859 Whispering
                                        Red or 2995 Dusty Red. It is also gorgeous as a golden pink contrast to darker
                                        golden grey nuances such as 0394 Soft Grey and 1352 Form. <br><strong>FIND THE
                                            RIGHT WHITE:</strong> It functions well with white nuances such as 9918
                                        Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                                    <figure class="color-card color-card--hex js-color-card u-bg--20120" data-color-id="20120">
                                        <figcaption class="color-card__caption">
                                            <p>JOTUN 20120 <strong>Organic Red</strong></p>
                                        </figcaption>

                                        <div class="color-card__helper">
                                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                                <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                    in fullscreen</span>
                                            </button>
                                        </div>

                                        <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                            data-favorite="20120">
                                            <i class="material-icons material-icons--first"></i>
                                            <i class="material-icons material-icons--second"></i>
                                            <span class="sr-only">Add the colour chip to your favourites</span>
                                        </button>
                                    </figure>

                                    <p class="color-card__description">A subdued reddish pink nuance. The colour
                                        appears warm and burnt. You will find elements of red, pink and brown in this
                                        warm nuance. Combined with brighter tones, such as 2024 Senses or 12086 Rustic
                                        Pink, the colours will create a warm colour flow. Green is also an exciting
                                        combination, check out 7629 Antique Green, or more golden green shades such as
                                        8252 Green Harmony and 8469 Green Leaf. Among the basic colours, nuances such
                                        as the beige 10678 Space, 12075 Soothing Beige, 12076 Modern Beige, 1140 Sand,
                                        and 1623 Marrakesh will work well. <br><strong>FIND THE RIGHT WHITE:</strong>
                                        20120 Organic Red functions well with 9918 Classic White, 1624 Skylight, 1001
                                        Egg White and 1453 Cotton Ball.</p>
                                    <figure class="color-card color-card--hex js-color-card u-bg--7613" data-color-id="7613">
                                        <figcaption class="color-card__caption">
                                            <p>JOTUN 7613 <strong>Northern Mystic</strong></p>
                                        </figcaption>

                                        <div class="color-card__helper">
                                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                                <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                    in fullscreen</span>
                                            </button>
                                        </div>

                                        <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                            data-favorite="7613">
                                            <i class="material-icons material-icons--first"></i>
                                            <i class="material-icons material-icons--second"></i>
                                            <span class="sr-only">Add the colour chip to your favourites</span>
                                        </button>
                                    </figure>

                                    <p class="color-card__description">A dark, golden green nuance. The colour is deep
                                        green, perfectly balanced in the sense that it is neither blue and cool, nor
                                        golden and yellow. It works well as a beautiful contrast to a series of other
                                        beige, grey, green, golden pink and peach nuances. It may work as a dark, cool
                                        contrast to practically all the colours in this years´ colour card from LADY.
                                        Some favourites are 7628 Treasure, 7629 Antique Green, 12078 Sheer Grey and
                                        0394 Soft Grey. <br><strong>FIND THE RIGHT WHITE:</strong> 7613 Northern Mystic
                                        functions well with 7236 Jazz, 9918 Classic White, 1624 Skylight, 1001 Egg
                                        White and 1453 Cotton Ball.</p>
                                    <figure class="color-card color-card--hex js-color-card u-bg--12074" data-color-id="12074">
                                        <figcaption class="color-card__caption">
                                            <p>JOTUN 12074 <strong>Peachy</strong></p>
                                        </figcaption>

                                        <div class="color-card__helper">
                                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                                <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                    in fullscreen</span>
                                            </button>
                                        </div>

                                        <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                            data-favorite="12074">
                                            <i class="material-icons material-icons--first"></i>
                                            <i class="material-icons material-icons--second"></i>
                                            <span class="sr-only">Add the colour chip to your favourites</span>
                                        </button>
                                    </figure>

                                    <p class="color-card__description">A subdued peach nuance. 12074 is a greyish peach
                                        tone, but it will appear fresh enough on your wall. It is brighter than 12085
                                        Rural and slightly darker than 12084 Dusky Peach. 12074 Peachy creates a cool
                                        accent when combined with 7628 Treasure and 7629 Antique Green for the more
                                        daring, or merely as a fresh golden nuance among the beige tones such as 10678
                                        Space, 12075 Soothing Beige, 12076 Modern Beige - or 1622 Reflection, 1024
                                        Timeless, 10679 Washed Linen, 12077 Sheer Grey, 12078 Comfort Grey and 0394
                                        Soft Grey. The colour also combines well with 12085 Rural. <br><strong>FIND THE
                                            RIGHT WHITE:</strong> 12074 Peachy works well with 9918 Classic White, 1624
                                        Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                                    <figure class="color-card color-card--hex js-color-card u-bg--20118" data-color-id="20118">
                                        <figcaption class="color-card__caption">
                                            <p>JOTUN 20118 <strong>Amber Red</strong></p>
                                        </figcaption>

                                        <div class="color-card__helper">
                                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                                <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                    in fullscreen</span>
                                            </button>
                                        </div>

                                        <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                            data-favorite="20118">
                                            <i class="material-icons material-icons--first"></i>
                                            <i class="material-icons material-icons--second"></i>
                                            <span class="sr-only">Add the colour chip to your favourites</span>
                                        </button>
                                    </figure>

                                    <p class="color-card__description">A golden red nuance. The colour appears burnt
                                        and golden. It will appear more lively and slightly more red than 2771 Rustic
                                        Terracotta, but will appear more reddish brown when compared with 2859
                                        Whispering Red. It works well with grey and beige such as 10678 Space, 12075
                                        Soothing Beige, 12076 Modern Beige, 1622 Reflection, 1024 Timeless, 10679
                                        Washed Linen, 10429 Discrete, 12077 Sheer Grey, 12078 Comfort Grey, 0394 Soft
                                        Grey and 1973 Objective. <br><strong>FIND THE RIGHT WHITE:</strong> 20118 Amber
                                        Red works well with 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453
                                        Cotton Ball.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="modal-footer text-center">
                            <button type="button" class="btn btn--line" data-dismiss="modal">
                                <span class="btn__text">Back</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main><!-- .site-main -->

    <script>
        var params = {
        "badge_01" : {
            method      : "feed",
            link        : $site_url,
            picture     : $root_url+"/<?php echo base_url(); ?>assets/media/facebook.t3.image",
            name        : "facebook.t3.name",
            caption     : "facebook.t3.caption",
            description : "facebook.t3.text"
        },
        "badge_02" : {
            method      : "feed",
            link        : $site_url,
            name        : "facebook.t3.name.2",
            caption     : "facebook.t3.caption.2"
        }
    };
</script>
<footer id="footer" class="footer section">
        <div class="container">
            <div class="btn-row">
                <a class="btn btn--line" href="<?php echo $this->config->item("whislist_link"); ?>">
                    <span class="btn__text">My favourites <span class="btn--heart"><i class="material-icons material-icons--second"></i>
                            <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span></span></span>
                </a>
            </div>
            <div class="btn-row">
                <a class="btn btn--line" href="<?php echo $this->config->item("palette_link"); ?>">
                    <span class="btn__text">Create your colour palette</span>
                </a>
                <a class="btn btn--line" href="http://jotun-gcc.com.dev02.allegro.no/products">
                    <span class="btn__text">Choose the right product</span>
                </a>
            </div>
            <div class="btn-row">
            <div class="btn btn--line btn--facebook js-btn-facebook fb-share-button" data-href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/raw" data-layout="button" data-size="large" data-mobile-iframe="true">
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com%2Fraw&amp;src=sdkpreparse" ><i class="icon icon--facebook"></i> Share to Facebook</a>
                </div>
                <button class="btn btn--line btn--pinterest pinterest-pin-it"><i class="icon icon--pinterest"></i>Pin
                    to Pinterest</button>
            </div>
            <a href="https://jotun.com/" class="footer__logo">
                <img data-src="assets/media/logos/jotun.svg" class=" lazyload" alt="JOTUN" src="./assets/dist/jotun.svg">
                <span class="sr-only">JOTUN</span>
            </a>
            <ul class="footer-links list-unstyled">

                <li>
                    <a class="ladybloggen" href="https://www.jotun.com/no/en/corporate/Termsandconditionscorporate.aspx"
                        target="_blank">
                        Privacy, terms &amp; condition and cookie policy
                    </a>
                </li>
            </ul>
        </div>
    </footer>

     <div class="fullscreencolor color-card--fullscreen js-color-card--fullscreen" role="dialog" aria-modal="true"
        aria-labelledby="fullscreenCaption" aria-describedby="colorinfo" tabindex="-1">
        <div class="fullscreencolor__color">

            <button type="button" class="close js-color-card-close" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>

            <button type="button" class="fullscreencolor__expand js-color-fullscreen-toggle" aria-label="View the colour in fullscreen"
                aria-expanded="true" aria-controls="fullscreenContent">
                <i class="material-icons">fullscreen</i>
            </button>


            <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" aria-label="Add the colour chip to your favourites">
                <i class="material-icons material-icons--first"></i>
                <i class="material-icons material-icons--second"></i>
                <span class="sr-only">Add the colour chip to your favourites</span>
            </button>

            <div class="fullscreencolor__caption fullscreencolor__caption--color js-color-card--caption" id="fullscreenCaption"></div>
        </div>

        <div class="fullscreencolor__content" id="fullscreenContent">
            <div class="fullscreencolor__caption js-color-card--caption"></div>

            <ul class="nav nav-tabs" role="tablist" id="colorTabs">
                <li role="presentation" class="active">
                    <a href="http://jotun-gcc.com.dev02.allegro.no/raw#colorinfo" aria-controls="colorinfo" role="tab"
                        data-toggle="tab">About the colour</a>
                </li>
                <li role="presentation">
                    <a href="http://jotun-gcc.com.dev02.allegro.no/raw#colormatching" aria-controls="colormatching"
                        role="tab" data-toggle="tab">Matching colours</a>
                </li>
            </ul>

            <div class="fullscreencolor__inner">
                <div class="container">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="colorinfo">
                            <div class="fullscreencolor__info">
                                <p class="js-disclaimer-color"></p>

                                <p class="minerals-mix-text">modal.minerals.mix.text</p>
                                <p class="ceiling-mix-text">modal.ceiling.mix.text</p>

                                <p>
                                    <strong>JOTUN COLOUR CONFIDENCE</strong><br>
                                    Our innovative colour technology offers unmatched colour accuracy. With Jotun
                                    Colour Confidence you can be sure that you will get exactly the colour you want.
                                </p>

                                <p>
                                    <strong>COLOUR REPRODUCTION</strong><br>
                                    Please be aware that colours will vary depending on your screen settings and
                                    resolution. Visit the nearest Jotun store to see sample colours.
                                </p>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="colormatching">
                            <div class="fullscreencolor__matching"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cookie js-cookie" role="alert">
        <div class="container">
            <div class="cookie__row">
                <div class="cookie__left">
                    <p>
                        By using the Jotun sites, you are consenting to our use of cookies in accordance with this
                        Cookie Policy. If you do not agree to our use of cookies in this way, you should set your
                        browser settings accordingly or not use the Jotun Sites. If you disable the cookies that we
                        use, this may impact your user experience while on the Jotun Sites.
                    </p>
                </div>
                <div class="cookie__right">
                    <button type="button" class="cookie__close js-cookie-close">
                        Do not show again
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="./assets/dist/messages.js"></script>
    <script type="text/javascript" src="./assets/dist/jquery.min.js"></script>
    <script type="text/javascript" src="./assets/dist/scripts.min.js"></script>



    <script>
        $(window).load(function () {
            $('.js-btn-facebook').click(function () {
                FB.ui(params[$(this).attr('rel')]);
                return false;
            });
        });
    </script>

</body>

</html>