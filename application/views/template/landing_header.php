<!DOCTYPE html>
<!-- saved from url=(0038)http://jotun-gcc.com.dev02.allegro.no/ -->
<html class="no-js " lang="en">
<!--<![endif]-->

<head data-lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:url" content="">
    <meta property="og:type" content="website">
    <meta property="og:title" content="facebook.general.name">
    <meta property="og:description" content="facebook.general.text">
    <meta property="og:image" content="assets/media/facebook.general.image">

    <meta name="msapplication-TileColor" content="#fbfbfb">
    <meta name="theme-color" content="#fbfbfb">

    <title>Jotun Colour Design app – Now available for iOS/Android</title>
    <meta name="description" content="facebook.general.text">

    <link rel="manifest" href="http://jotun-gcc.com.dev02.allegro.no/manifest.json">
    <link id="main-css" href="<?php echo base_url(); ?>assets/dist/styles.min.css" rel="stylesheet" type="text/css" media="all">

    <link rel="icon" type="image/png" href="http://jotun-gcc.com.dev02.allegro.no/favicon-16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="http://jotun-gcc.com.dev02.allegro.no/favicon-32.png" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" href="http://jotun-gcc.com.dev02.allegro.no/favicon-152.png">
    <meta name="msapplication-TileImage" content="favicon-478.png">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/data.json"></script>
    <script>
        var $site_url = '';
        // Picture element HTML5 shiv
      document.createElement( "picture" );
    </script>
    <script src="<?php echo base_url(); ?>assets/dist/picturefill.min.js" async=""></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <!-- phone animation -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/component2.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/component.css" />
    
    <!-- isometric gallery -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/isometric-demo.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/isometric.css" />
    <script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>
    

</head>

<body class="home page lang-en is-ready">

    <a href="http://jotun-gcc.com.dev02.allegro.no/#main" title="Skip to the main content" class="skip-navigation">To
        the main content</a>


    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu"
                    aria-expanded="false">
                    <span class="navbar__label">Menu</span>
                    <span class="navbar__icon">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span>
                </button>
                <a href="http://jotun-gcc.com.dev02.allegro.no/" class="navbar__logo">
                    <img src="<?php echo base_url(); ?>assets/dist/jotun.svg" alt="JOTUN">
                </a>
            </div>

                <div class="collapse navbar-collapse" id="main-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo $this->config->item("calm_link"); ?>">Calm</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->config->item("refined_link"); ?>">Refined</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->config->item("raw_link"); ?>">Raw</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->config->item("palette_link"); ?>">Create your colour palette</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->config->item("colour_link"); ?>">See all colours</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="favorite-nav js-favorite-nav is-visible">
        <a href="http://jotun-gcc.com.dev02.allegro.no/wishlist" title="Go to my favourites" class="btn btn--transparent js-favorite-btn"
            aria-live="polite" aria-atomic="false">
            <i class="material-icons material-icons--second"></i>
            <p class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</p>
        </a>
    </div>
    
    


    