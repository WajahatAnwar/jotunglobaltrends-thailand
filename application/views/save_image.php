<!doctype html>
<html>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>My personal interior palette</title>
   <link rel="icon" href="http://jotun-gcc.com.dev02.allegro.no/assets/media/images/cropped-favicon-1-478x478.png" sizes="32x32">
   <link rel="icon" href="http://jotun-gcc.com.dev02.allegro.no/assets/media/images/cropped-favicon-1-478x478.png" sizes="192x192">
   <link rel="apple-touch-icon-precomposed" href="http://jotun-gcc.com.dev02.allegro.no/assets/media/images/cropped-favicon-1-478x478.png">
   <meta name="msapplication-TileImage" content="http://jotun-gcc.com.dev02.allegro.no/assets/media/images/cropped-favicon-1-478x478.png">
   <style>
      @font-face {
      font-family:"Trajan Pro";
      src:url(http://jotun-gcc.com.dev02.allegro.no/assets/fonts/trajanproregular.ttf) format('truetype'),
      url(http://jotun-gcc.com.dev02.allegro.no/assets/fonts/trajanproregular.otf) format('opentype'),
      url(http://jotun-gcc.com.dev02.allegro.no/assets/fonts/trajanproregular.eot?#iefix) format('embedded-opentype'),
      url(http://jotun-gcc.com.dev02.allegro.no/assets/fonts/trajanproregular.woff) format('woff');
      font-weight: normal;
      }
      @font-face {
      font-family:"DIN";
      src:url(http://jotun-gcc.com.dev02.allegro.no/assets/fonts/dinproregular.ttf) format('truetype'),
      url(http://jotun-gcc.com.dev02.allegro.no/assets/fonts/dinproregular.otf) format('opentype'),
      url(http://jotun-gcc.com.dev02.allegro.no/assets/fonts/dinproregular.eot?#iefix) format('embedded-opentype'),
      url(http://jotun-gcc.com.dev02.allegro.no/assets/fonts/dinproregular.woff) format('woff');
      font-weight: normal;
      }
      @font-face {
      font-family:"DIN";
      src:url(http://jotun-gcc.com.dev02.allegro.no/assets/fonts/dinprobold.ttf) format('truetype'),
      url(http://jotun-gcc.com.dev02.allegro.no/assets/fonts/dinprobold.otf) format('opentype'),
      url(http://jotun-gcc.com.dev02.allegro.no/assets/fonts/dinprobold.eot?#iefix) format('embedded-opentype'),
      url(http://jotun-gcc.com.dev02.allegro.no/assets/fonts/dinprobold.woff) format('woff');
      font-weight: bold;
      }
      .u-bg--5044-01 { background: #808d92 url(http://jotun-gcc.com.dev02.allegro.no/assets/media/colours/5044-01.jpg); }
      .u-bg--20047-01 { background: #c3a590 url(http://jotun-gcc.com.dev02.allegro.no/assets/media/colours/20047-01.jpg); }
   </style>
   <link href="http://jotun-gcc.com.dev02.allegro.no/assets/dist/palette-noflex.min.css" rel="stylesheet" type="text/css" media="all">
   <div class="palette__nav">
      <table class="palette__spacer">
         <tbody>
            <tr>
               <td></td>
            </tr>
         </tbody>
      </table>
      <table class="palette__spacer">
         <tbody>
            <tr>
               <td></td>
            </tr>
         </tbody>
      </table>
      <div class="palette__logo">
         <img src="http://jotun-gcc.com.dev02.allegro.no/assets/media/logos/jotun.svg" alt="JOTUN">
      </div>
      <table class="palette__spacer">
         <tbody>
            <tr>
               <td></td>
            </tr>
         </tbody>
      </table>
   </div>
   <div class="palette__header">
      <h1 class="palette__title">My personal interior palette</h1>
      <p class="palette__sender">All colours in your interior palette is from our Colour Trends 2019 brochure.</p>
      <a href="http://jotun-gcc.com.dev02.allegro.no/palette" class="palette__link"><?php echo base_url(); ?></a>
   </div>
   <div class="palette__main">
      <div class="palette__section palette__section--base">
         <h2 class="palette__subheader">Your selected base colour</h2>
         <div class="palette__group">
            <div class="palette__colour">
               <div class="palette__wrapper">
                  <div class="color-card color-card--image u-bg--12074" data-color-id="12074" style="background: <?php echo $original_name; ?>"></div>
                  <div class="color-card__caption">
                     <?php echo $original_colour; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php if(!empty($comp_color_array)) { ?>
      <div class="palette__section palette__section--group_1">
         <h3 class="palette__subheader">Complementary colours</h3>
         
            <?php 
            for($i = 0; $i < count($comp_color_array); $i++){ ?>
            <div class="palette__group">
              <div class="palette__colour" style="width: 100% !important;">
                <div class="palette__wrapper">
                    <div class="color-card color-card--image u-bg--<?php echo $comp_color_array[$i]; ?>" data-color-id="<?php echo $comp_color_array[$i]; ?>"></div>
                    <div class="color-card__caption">
                      <?php echo $comp_name_array[$i]; ?>
                    </div>
                </div>
              </div>
          </div>
            <?php } ?>
      </div>
      <?php 
      }
      if(!empty($acce_color_array)) { ?>
      <div class="palette__section palette__section--group_2">
         <h3 class="palette__subheader">Accent Colours</h3>
            <?php 
            for($l = 0; $l < count($acce_color_array); $l++){ ?>
              <div class="palette__group">
              <div class="palette__colour" style="width: 100% !important;">
                <div class="palette__wrapper">
                    <div class="color-card color-card--image u-bg--<?php echo $acce_color_array[$l]; ?>" data-color-id="<?php echo $acce_color_array[$l]; ?>"></div>
                    <div class="color-card__caption">
                      <?php echo $acce_name_array[$l]; ?>
                    </div>
                </div>
              </div>
         </div>
            <?php } ?>
      </div>
          <?php } 
          if(!empty($whit_color_array)) { ?>
      <div class="palette__section palette__section--group_3">
         <h3 class="palette__subheader">Complementary white</h3>
            <?php 
               for($m = 0; $m < count($whit_color_array); $m++){ ?>
                <div class="palette__group">
                <div class="palette__colour" style="width: 100% !important;">
                  <div class="palette__wrapper">
                      <div class="color-card color-card--image u-bg--<?php echo $whit_color_array[$m]; ?>" data-color-id="<?php echo $whit_color_array[$m]; ?>"></div>
                      <div class="color-card__caption">
                        <?php echo $whit_name_array[$m]; ?>
                      </div>
                  </div>
                </div>
         </div>
              <?php } ?>
      </div>
          <?php }?>
   </div>
   <div class="palette__footer">
      <p class="palette__disclaimer">Disclaimer colours on screen:</p>
   </div>
</html>