<main id="main" class="site-main">
       
        <section class="main row" style="background: #eeee; padding-top:2%;">
        
<!--        style="background:#f8f8f8;padding-top: 2%;padding-left: 20%;margin: 0 auto;width: 100%;"-->
        
        <div class="content_section col-lg-6 col-md-6 col-sm-12 text-right" style=" padding-right: 2%;    padding-top: 10%;">
            <h2 class="app-title">
                                <span class="small">COLOUR</span>
                                <span class="big">DESIGN</span>							
                            </h2>
                            <p>
                              
                               Finding your perfect colour is as simple as a tap of the screen. <br>
Download Jotun Colour Design App to Experiment <br>With Colours Like Never Before.
                               
                            </p>
                            
                             <div class="download-buttons">
                        <a href="https://itunes.apple.com/us/app/jotun-colourdesign/id1312722535?ls=1&mt=8" target="_blank">
                            <img style="width:140px;" src="<?php echo base_url(); ?>assets/images/appstore.png" alt="">
                        </a>
                        <a href="https://play.google.com/store/apps/details?id=com.jotun.colourdesign&referrer=utm_source%3Dcolour_trends_microsite_mea%26utm_medium%3Dreferral%26utm_campaign%3Dct_mea_microsite_referral_traffic" target="_blank">
                            <img style="width:140px;" src="<?php echo base_url(); ?>assets/images/googlestore.png" alt="">
                        </a>
                    </div>
                    <br>
        </div>
     
            <div id="ac-wrapper" class="ac-wrapper col-lg-6 col-md-6 col-sm-12 ">
               
                           
               
<!--
                <h2>COLOUR  <span>DESIGN</span>

                    <p style="font-size:16px;" class="ac-title">Finding your perfect Colour is as simple as a tap of the screen. <br>Download Jotun Colour Design App for Experiment With Colours Like Never Before.</p>
-->
                     

                    


                <div class="ac-device">
                    <a href="#"><img style="       width: 240px;
    margin: 0 auto;" src="<?php echo base_url(); ?>assets/images/1-colourdesign/colour-design-1.png"/></a>
                    <h3></h3>
                </div>
                <div class="ac-grid">
                    <a href="#"><img src="<?php echo base_url(); ?>assets/images/1-colourdesign/colour-design-1.png"/><span></span></a>
                    <a href="#"><img src="<?php echo base_url(); ?>assets/images/1-colourdesign/colour-design-1.png"/><span></span></a>
                    <a href="#"><img src="<?php echo base_url(); ?>assets/images/1-colourdesign/colour-design-1.png"/><span></span></a>
<!--                    <a href="#"><img src="<?php echo base_url(); ?>assets/images/1-colourdesign/colour-design-1.png"/><span></span></a>-->
                </div>

                <div class="scroll-below" style="text-align:center;cursor:">

                        <img class="scroller" style="cursor:pointer;width: 70px;margin-top: -20px;" src="<?php echo base_url(); ?>assets/images/gif-bounce-arrow.gif" alt="">

                </div>	
            </div>	
				
        </section>
        
        
        
			<section class="animation isomatric section--intro text-center">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center slideanim">
                            <h2 class="app-title">
                                <span class="small">Get</span>
                                <span class="big">Inspired</span>							
                            </h2>
                            <p>
                                Discover the latest Global Colour Trends of 2018 and be inspired by a vast gallery<br> of colour collections for interior and exterior paint.
                            </p>
                    </div>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                        <img src="<?php echo base_url(); ?>assets/images/2-getinspired/getinspired.png" alt="">
                    </div>
                    
				<div class="isolayer isolayer--deco1 isolayer--shadow" style="display:none;">
					<ul class="grid stop_overflow">
					
<!--					    (li.grid__item>a.grid_link[href="javascript:void(0)"]>(div.layer*3)>img[class="grid__img layer" src="<?php echo base_url(); ?>assets/images/screen1_$.png" alt="New Colour Design App"])-->
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/1.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/2.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/3.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/4.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/5.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/1.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/2.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/3.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/4.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/5.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/1.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/2.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/3.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/4.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/5.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/2.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/3.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/4.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/5.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/1.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/2.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/3.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/4.png" alt="01" />
							</a>
						</li>
						<li class="grid__item">
							<a class="grid__link" href="javascript:void(0)"><div class="layer"></div><div class="layer"></div><div class="layer"></div><img class="grid__img layer" src="<?php echo base_url(); ?>assets/images/2-getinspired/5.png" alt="01" />
							</a>
						</li>
						
					</ul>
				</div>
			</section><!--/get inspired-->
			
		
		

        <section class="animation_1 section section--cta text-center js-section-video" id="" style="padding-bottom:0px;">
            <div class="container card-container">
                <div class="row">
                   
                    <div class="card col-xs-12 col-sm-6 col-lg-6 " >
                        <div class="ms-wrapper ms-effect-1">
                        <button class="activate" style="display:none;">toggle view</button>
                        <div class="ms-perspective">
                            <div class="ms-device">
                                <div class="ms-object">
                                    <div class="ms-front"></div>
                                    <div class="ms-back"></div>
                                    <div class="ms-left ms-side"></div>
                                    <div class="ms-right ms-side"></div>
                                    <div class="ms-top ms-side"></div>
                                    <div class="ms-bottom ms-side"></div>
                                </div><!-- /ms-object -->
                                <div class="ms-screens">
                                    <a class="ms-screen-1" style="background: url(<?php echo base_url(); ?>assets/images/3-getexperimental/1.jpg) no-repeat center center;background-size: contain;"></a>
                                    <a class="ms-screen-2" style="background: url(<?php echo base_url(); ?>assets/images/3-getexperimental/2.jpg) no-repeat center center;background-size: contain;"></a>
                                    <a class="ms-screen-3" style="background: url(<?php echo base_url(); ?>assets/images/3-getexperimental/3.jpg) no-repeat center center;background-size: contain;"></a>
                                    <a class="ms-screen-4" style="background: url(<?php echo base_url(); ?>assets/images/3-getexperimental/4.jpg) no-repeat center center;background-size: contain;"></a>
                                    <a class="ms-screen-5" style="background: url(<?php echo base_url(); ?>assets/images/3-getexperimental/5.jpg) no-repeat center center;background-size: contain;"></a>
                                </div>
                            </div><!-- /ms-device -->
                        </div><!-- /ms-perspective -->
                        
			        </div><!-- /ms-wrapper -->
                    </div>
                    <div class=" col-xs-12 col-sm-6 col-lg-6" style="margin-top:10%;">
                       <div class="row">
                    <div class="col-xs-12 text-left">
                       
                       
                            <h2 class="app-title">
                                <span class="small">Get</span>
                                <span class="big">EXPERIMENTAL </span>							
                            </h2>
                            <p>
                                Watch your walls come alive with vibrant colours at the tap of your finger. Be bold and experiment with various combinations, upload your own photo and share the final creations with friends and family.
                            </p>
					    
                    </div>
                </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="hand_phone_section section text-center " style="background:url(<?php echo base_url(); ?>assets/images/4-browseproducts/browseproductbackground.png); background-size: cover;    padding-bottom: 0px;">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  text-right slideanim" style="    margin-top: 10%;">
                            <h2 class="app-title">
                                <span class="small">Browse</span>
                                <span class="big">Products</span>							
                            </h2>
                            <p>
                                Browse and discover the most suited Jotun products <br> for your tastes and needs.
                            </p>
                        </div>
                        <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-12  text-left slideanim">
                            <img class="col-lg-8 hand_phone"  style="margin-top: 90px;" src="<?php echo base_url(); ?>assets/images/4-browseproducts/browseproduct.png" alt="">
                        </div>
                        
                </div>
            </div>
        </section>
        
        
			
        <section class=" animation_2 section text-center" >
            <div class="container">
                <div class="row ">
                  
                        <div class="col-lg-offset-3 col-md-offset-3 col-lg-7 col-md-9 col-sm-10 col-xs-12 col-centered text-center slideanim">
						<h2 class="app-title">
							<span class="small">Find a Multicolor Centre </span>
							<span class="big"><span>Near</span> <span>You</span></span>							


						</h2>
						<p>
							Identify the nearest Jotun Dealer near you. Obtain
professional advice, quality products and be ready to craft
your own space.
						</p>
						
					</div>
                   
                </div>
                
                <div class="col-xs-12 col-md-8 col-sm-12" style="margin-top:30px; margin-left:20%;">
                         <div class="ms-wrapper ms-effect-2">
                            <button class="browse_products" style="display:none;">toggle view</button>
                            <div class="ms-perspective">
                                <div class="ms-device ms-device-two">
                                    <div class="ms-object">
                                        <div class="ms-front"></div>
                                        <div class="ms-back"></div>
                                        <div class="ms-left ms-side"></div>
                                        <div class="ms-right ms-side"></div>
                                        <div class="ms-top ms-side"></div>
                                        <div class="ms-bottom ms-side"></div>
                                    </div><!-- /ms-object -->
                                    <div class="ms-screens">
                                        <a class="ms-screen-1" style="background: url(<?php echo base_url(); ?>assets/images/5-nearestdealer/1.png) no-repeat center center;background-size: contain;"></a>
                                        <a class="ms-screen-2" style="background: url(<?php echo base_url(); ?>assets/images/5-nearestdealer/2.png) no-repeat center center;background-size: contain;"></a>
<!--                                        <a class="ms-screen-3" style="background: url(<?php echo base_url(); ?>assets/images/5-nearestdealer/3.jpg) no-repeat center center;background-size: contain;"></a>-->

                                    </div>
                                </div><!-- /ms-device -->
                            </div><!-- /ms-perspective -->
                        </div><!-- /ms-wrapper -->
                        
                    </div>
                      
            </div>
        </section>

        <section class="section section--cta text-center" style="background:url(<?php echo base_url(); ?>assets/images/download-section.jpg)">
            <div class="container">
               
               
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h2> 
 
  <span>DOWNLOAD THE COLOUR DESIGN APP NOW</span>

                    <p style="font-size:16px;" class="ac-title">Finding your perfect colour is as simple as a tap of the screen.</p>
                     

                     <div class="download-buttons">
                        <a href="https://itunes.apple.com/us/app/jotun-colourdesign/id1312722535" target="_blank">
                            <img style="width:140px;" src="<?php echo base_url(); ?>assets/images/appstore.png" alt="">
                        </a>
                        <a href="https://play.google.com/store/apps/details?id=com.jotun.colourdesign" target="_blank">
                            <img style="width:140px;" src="<?php echo base_url(); ?>assets/images/googlestore.png" alt="">
                        </a>
                    </div>

                </h2>
                </div>
            </div>
        </section>