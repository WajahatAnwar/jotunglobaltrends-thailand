<?php

class Members_model extends CI_Model{
function __construct() {
parent::__construct();
}
function add_member($data){
	// Inserting in Table(students) of Database(college)
	
	$this->db->select('membershipName ');
		$where = array (
						'membershipName' => $data['membershipName'],
					);
		$this->db->where($where);
		$query = $this->db->get('membership');

				 if($query->num_rows() > 0) {
					$row = $query->result_array();
				  return false;
				}else{
					$result = $this->db->insert('membership', $data);
					if($result)
					{
						return $result;
					}
				}
	}
	function get_members($params = array())
	{
		$this->db->select('*');
		$where = array (
						'Deleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('membership');
		// $this->db->from('users');
		$this->db->order_by('membershipID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function get_members_by_id($membership_id)
	{
		$this->db->select('*');
		$where = array (
						'membershipID' => $membership_id,
					);
		$this->db->where($where);
		$this->db->from('membership');
		$this->db->order_by('membershipID','desc');

			$this->db->limit(1);
		

				$query = $this->db->get();
				if(($query->num_rows() > 0)){
						 $row = $query->result_array();   
		/* 	$newdata = array(
				  'userID'  => $row[0]['userID'],
				  'userEmail'   => $row[0]['userEmail'],
				  'userCompanyName'   => $row[0]['userCompanyName'],
				); */
				
				return ($row);
				} else {
					return false;
				}
			
		// return ($query->num_rows() > 0)?$query->result_array()$where = array (
						// 'userID' => $userID,
						// 'userEmail' => $userEmail,
						// 'userCompanyName' => $userCompanyName,
					// );:FALSE;
	}
	function update_member($data , $membership_id) {
		
	$where = array (
						'membershipID' => $membership_id,
					);
		$this->db->where($where);
		$result = $this->db->update('membership', $data); 
		if($result) {
			
		return $result;
		} else {
			
		return false;
		}
	}

	
	function delete_membership($membership_id)
	{
		$this->db->select('Deleted'); 
		$where = array (
						'membershipID' => $membership_id
					);
		$this->db->where($where);
		$query = $this->db->get('membership');
	
		 if($query->num_rows() > 0){
			 $row = $query->result_array();
			  if($row[0]['Deleted'] == 0) {
				  $category_deleted = 1;
			  }
			  else {
				  $category_deleted = 0;
			  }
			$this->db->where(array("membershipID" => $membership_id));
			$this->db->set('Deleted',$category_deleted);
            $update = $this->db->update('membership');
			return $update;
		 }
	}
	function get_all_company()
	{

		$this->db->select('*');
		$this->db->where(array("userDeleted" => '0',"userType" => 'company'));
		$this->db->from('users');

		$query = $this->db->get();
		// return $company_id;
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function get_membership()
	{

		$this->db->select('*');
		$this->db->where(array("deleted" => '0'));
		$this->db->from('membership');

		$query = $this->db->get();
		// return $company_id;
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	
	function renewed_membership($company_id , $membership_id ,$company_membership_expired_date)
	{
		
	$where = array (
						'userID' => $company_id,
					);
		$this->db->where($where);
		$this->db->set('membershipID', $membership_id); 
		$this->db->set('membershipExpiredDate', $company_membership_expired_date); 
		$result = $this->db->update('users'); 
		if($result) {
			
		return $result;
		} else {
			
		return false;
		}
	}
	
	function check_membership($company_id)
	{
		$this->db->select('users.membershipID,membership.membershipName');
		$this->db->join('membership', 'users.membershipID = membership.membershipID','left');
		$this->db->where(array("userID" => $company_id ));
		$this->db->from('users');

		$query = $this->db->get();
		// return $company_id;
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
		if($query) {
			
		return $query;
		} else {
			
		return false;
		}
	}
	function get_member_by_id($company_id)
	{
	$this->db->select('*');
		$this->db->join('users_meta', 'users.userID = users_meta.userID','left');
		$where = array (
						'users.userID' => $company_id,
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('users.userID','desc');

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
}