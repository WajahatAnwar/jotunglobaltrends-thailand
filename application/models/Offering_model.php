<?php

class Offering_model extends CI_Model {
		public function __construct() {
		parent::__construct();
		$this->load->library('encrypt');

	}

	function course_listing()
	{
		$query = $this->db->query("SELECT
		`courses`.`displayName`,
		`courses`.`coursesID`,
		`slugs`.`uri`
		FROM
		`courses`
		JOIN `slugs`
		ON `courses`.`coursesID` = `slugs`.`id` 
		WHERE
		`courses`.`ceCheck` = '1' OR `courses`.`reportAsha` = '1' AND `courses`.`coursesDeleted` = '0'");
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function course_listing_by_id($course_id)
	{
		$query = $this->db->query("SELECT
		`courses`.`displayName`,
		`courses`.`coursesID`,
		`slugs`.`uri`
		FROM
		`courses`
		JOIN `slugs`
		ON `courses`.`coursesID` = `slugs`.`id` 
		WHERE
		`courses`.`liveCheck` = '0' AND `courses`.`coursesStatus` = 'publish' AND `courses`.`coursesDeleted` = '0' AND `courses`.`coursesID` = '".$course_id."'");
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function check_offering($course_id)
	{
		$this->db->select('*');
		$where = array(
			"coursesID" => $course_id,
			"status" => 0
		);
		$this->db->where($where);
		$this->db->from('course_offering');

		$data = $this->db->get();

		if($data->num_rows() > 0)
		{
			$this->db->where($where);
			$this->db->set('status', '1');
			$this->db->update('course_offering');

		}
	}

	function insert_offering($data)
	{
		$this->db->select('*');
		$where = array(
			"coursesID" => $data['coursesID'],
			"offeringNumber" => $data['offeringNumber'],
			);
		$this->db->where($where);
		$this->db->from('course_offering');

		$query = $this->db->get();
		if($query->num_rows() == 0)
		{
			$query2 = $this->db->insert('course_offering', $data);
			// echo $query2;
		}
	}

	function get_valid_offerings()
	{
		$this->db->select('*');
		$where = array(
			"status" =>'0',
			"deleted" => '0'
			);
		$this->db->where($where);
		$this->db->from('course_offering');

		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function set_course_offer($course_id, $offering_id)
	{
		$this->db->select('*');
		$where = array(
			"coursesID" => $course_id,
			"offeringID" => $offering_id,
			"coursesDeleted" => '0'
		);
		$this->db->where($where);
		$this->db->from('courses');

		$query = $this->db->get();
		if($query->num_rows() == 0)
		{
			$where2 = array(
				"coursesID" => $course_id,
				"coursesDeleted" => '0'
			);
			$this->db->where($where2);
			$this->db->set('offeringID', $offering_id);
            $query1 = $this->db->update('courses');
		}
	}

	function get_offering($params = array())
	{
		$this->db->select('*');
		$where = array (
				'deleted' => '0'
		);
		$this->db->where($where);
		$this->db->from('course_offering');
		$this->db->order_by('offeringID','ASC');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function get_offering_search($course_id)
	{

			$clientID = $this->session->userdata('userID');
            $query = $this->db->query("SELECT *
                    FROM `course_offering`
                    WHERE coursesID = '" .$course_id. "%' AND deleted = 0
                    LIMIT 0,10");
            $skillarryhold = '';
            if($query->num_rows() > 0)
            {   
			    $rows = $query->result();
                foreach ($rows as $row)
                {
                    $skillarryhold .= ' <ul id="name-list">
                    <li class="phone_click" onClick="selectname('.$row->coursesID.');"><span class=""><i class="fa fa-phone" style="/*background: #e5e5e5;*/ padding: 12px 27px 12px 12px;margin-top: 0;" ></i></span><span class="">'.$row->coursesID.'</span></li></ul>';
                }
                return $skillarryhold;
            }
    }
    function search_offering($course_id)
	{
		$this->db->select('*');
			$where = array (
						'coursesID' => $course_id,
						'deleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('course_offering');
		$this->db->order_by('offeringID','ASC');

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function change_offering_status($offering_id, $status_value)
	{
		$where = array(
			"offeringID" => $offering_id
		);
		$this->db->where($where);
		if($status_value == '1')
		{
			$this->db->set('status', '1');
		}else if($status_value == '2')
		{
			$this->db->set('status', '2');
		}else
		{
			$this->db->set('status', '0');
		}
		$query = $this->db->update('course_offering');
		return $query;
	}
	function get_course_offering_by_id($offering_id)
	{
		$this->db->select('course_offering.offeringID,course_offering.offeringNumber,course_offering.completionDate,course_offering.endingDate,course_offering.status,courses.coursesID,courses.displayName');
		$this->db->from('course_offering');
		$this->db->join('courses','courses.coursesID=course_offering.coursesID');
		$this->db->where('course_offering.offeringID',$offering_id);
		$query=$this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function update_offering($data,$offer_id)
	{
		$this->db->where('offeringID',$offer_id);
		if($this->db->update('course_offering',$data))
		{
				return '1';
		}
		else{
				return '0';
		}
	}
	function delete_offering($offering_id)
	{
		$this->db->set('deleted',1);
		$this->db->where('offeringID',$offering_id);
		if($this->db->update('course_offering'))
		{
				return '1';
		}
		else{
				return '0';
		}
	}
}