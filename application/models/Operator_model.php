<?php

class Operator_model extends CI_Model {

	public function __construct() {
		parent::__construct();

	}



	function add_operator($data , $user_id, $file_name)
	{
		$this->db->select('userCompanyName , userEmail ');
		$where = array (
						'userEmail' => $data['userEmail'],
					);
		$this->db->where($where);
		$query = $this->db->get('users');

				 if($query->num_rows() > 0) {
					$row = $query->result_array();
				  return false;
				} else {

					$this->db->insert('users', $data);
					$last_user_id = $this->db->insert_id();
					$data2 = array (
						'userID' => $last_user_id,
						'userMetaKey' => 'file_name',
						'userMetaValue' => $file_name,
					);

					$inserting = $this->db->insert('users_meta', $data2);
					return $inserting;

				}

	}

	function get_operator($params = array())
	{
		$this->db->select('*');
			$where = array (
						'userType' => 'operator',
						'userDeleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('userID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}


	function get_operator_by_id($operator_id)
	{
		$this->db->select('*');
		$this->db->join('users_meta', 'users.userID = users_meta.userID','LEFt');
		$where = array (
						'users.userID' => $operator_id,
					);
		$this->db->where($where);
		$this->db->from('users');
		
		$query = $this->db->get();
		if(($query->num_rows() > 0)){
			
			$rows = $query->result_array();
			$newdata = array(
			  'userID'  => $rows[0]['userID'],
			  'userEmail'   => $rows[0]['userEmail'],
			  'userCompanyName'   => $rows[0]['userCompanyName'],
			  'userMetaKey'   => $rows[0]['userMetaKey'],
			  'userMetaValue'   => $rows[0]['userMetaValue'],
			);

			return ($rows);
			
		} else {
			return false;
		}

		// return ($query->num_rows() > 0)?$query->result_array()$where = array (
						// 'userID' => $userID,
						// 'userEmail' => $userEmail,
						// 'userCompanyName' => $userCompanyName,
					// );:FALSE;
	}

	function delete_operator($operator_id)
	{
		$this->db->select('userDeleted');
		$where = array (
						'userID' => $operator_id
					);
		$this->db->where($where);
		$query = $this->db->get('users');

		 if($query->num_rows() > 0){
			 $row = $query->result_array();
			  if($row[0]['userDeleted'] == 0) {
				  $user_deleted = 1;
			  }
			  else {
				  $user_deleted = 0;
			  }
			$this->db->where(array("userID" => $operator_id));
			$this->db->set('userDeleted',$user_deleted);
            $this->db->update('users');

		 }
	}


	function update_operator($data , $company_id , $file_name) {

	$where = array (
						'userID' => $company_id
					);
		$this->db->where($where);
		$result = $this->db->update('users', $data);
		
		$data2   = array(
                    'userID' => $company_id,
                    'userMetaKey' => 'file_name',
                    'userMetaValue' => $file_name
            );
			$where2 = array (
				'userID' => $company_id
			);
			if(!empty($file_name)){
				 $updatess = $this->db->update('users_meta' , $data2 , $where2);
			}
		if($result) {

		return true;
		} else {

		return false;
		}
	}
	
	
	function get_educator($params = array())
	{
		$this->db->select('*');
			$where = array (
						'userType' => 'instructor',
						'userDeleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('firstName','ASC');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	
	function get_student($params = array())
	{
		$this->db->select('*');
			$where = array (
						'userType' => 'student',
						'userDeleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('firstName','ASC');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function delete_educator($educator_id)
	{
		$this->db->select('userDeleted');
		$where = array (
						'userID' => $educator_id
					);
		$this->db->where($where);
		$query = $this->db->get('users');

		 if($query->num_rows() > 0){
			 $row = $query->result_array();
			  if($row[0]['userDeleted'] == 0) {
				  $user_deleted = 1;
			  }
			  else {
				  $user_deleted = 0;
			  }
			$this->db->where(array("userID" => $educator_id));
			$this->db->set('userDeleted',$user_deleted);
            $this->db->update('users');

		 }
	}
    
    /**
    * get the instructor
    * store it in a new array and return it to the controller
    * @return array
    */
	function get_educator_phone($phone_no, $role)
	{

			$clientID = $this->session->userdata('userID');
            $query = $this->db->query("SELECT *
                    FROM `users`
                    WHERE firstName like '" .$phone_no. "%' OR lastName like '" .$phone_no. "%' OR userEmail like '".$phone_no."%' AND userDeleted = 0 AND userType='".$role."' 
                    ORDER BY userID
                    LIMIT 0,10");
            $skillarryhold = '';
            if($query->num_rows() > 0){
                
			    $rows     = $query->result();
                foreach ($rows as $row)
                {
                    $skillarryhold .= ' <ul id="name-list">
                    <li class="phone_click" onClick="selectname('.$row->userID.');"><span class=""><i class="fa fa-phone" style="/*background: #e5e5e5;*/ padding: 12px 27px 12px 12px;margin-top: 0;" ></i></span><span class="">'.$row->firstName.' '.$row->lastName.'</span></li></ul>';
                }
                return $skillarryhold;
                
            }
    }
    
    function search_educator($company_id , $role, $params = array())
	{
		$this->db->select('*');
			$where = array (
						'userID' => $company_id,
						'userType' => $role,
						'userDeleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('userID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
    
    /**
    * get show membership package  data from  the database,
    * store it in a new array and return it to the controller
    * @return array
    */
	function get_operator_phone($phone_no)
	{

			$clientID = $this->session->userdata('userID');
            $query = $this->db->query("SELECT *
                    FROM `users`
                    WHERE userName like '" .$phone_no. "%' AND userDeleted = 0 AND userType='operator' 
                    ORDER BY userID
                    LIMIT 0,10");
            $skillarryhold = '';
            if($query->num_rows() > 0){
                
			    $rows     = $query->result();
                foreach ($rows as $row)
                {
                    $cashbackComission = 0;
                    $charityComission = 0;

                    $skillarryhold .= ' <ul id="name-list">
                    <li class="phone_click" onClick="selectname('.$row->userID.');"><span class=""><i class="fa fa-phone" style="/*background: #e5e5e5;*/ padding: 12px 27px 12px 12px;margin-top: 0;" ></i></span><span class="">'.$row->userName.'</span></li></ul>';
                }
                return $skillarryhold;
                
            }
    }
    
    function search_operator($company_id , $params = array())
	{
		$this->db->select('*');
			$where = array (
						'userID' => $company_id,
						'userType' => 'operator',
						'userDeleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('userID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
    
        /**
    * get client users data from  the database,
    * store it in a new array and return it to the controller
    * @return array
    */
	function educator_report()
	{


				return $query = $this->db->query("SELECT
										users.userName,
										users.userEmail,
										users.userPhoneNo
										 FROM `users`
										 WHERE  userDeleted = '0' AND userType='instructor'
										");



	}

	function student_report()
	{


				return $query = $this->db->query("SELECT
										users.userName,
										users.userEmail,
										users.userPhoneNo
										 FROM `users`
										 WHERE  userDeleted = '0' AND userType='student'
										");



	}
    
    /**
    * get client users data from  the database,
    * store it in a new array and return it to the controller
    * @return array
    */
	function operator_report()
	{


				return $query = $this->db->query("SELECT
										users.userName,
										users.userEmail,
										users.userPhoneNo
										 FROM `users`
										 WHERE  userDeleted = '0' AND userType='operator'
										");



	}

}

