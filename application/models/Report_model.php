<?php

class Report_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		// if($this->session->userdata('userType') != "admin")
		// {
		// 	redirect("home");
		// }

	}
	/*
	* This function generate the
	* ASHA report for the user who are eligible
	* for the ASHA Report
	*/

	/*When Doing Some changing in where please check these function where also---- these should match with each other 
	function1 : get_refrence (in where user_include)
	function2 : get_total_pass_in_course(where exactly match to function3)
	function3 : asha_report
	*/
	function asha_report($course_id)
	{
	/*When Doing Some changing in where please check these function where also---- these should match with each other 
	function1 : get_refrence (in where user_include)
	function2 : get_total_pass_in_course(where exactly match to function3)
	function3 : asha_report
	*/
		$query = $this->db->query('SELECT
			`courses`.`displayName`,
			`courses`.`coursesID`,
			`users`.`userName`,
			`users`.`userID`,
			`users`.`firstName`,
			`users`.`lastName`,
			`quizess_report`.`quizId`,
			`quizess_report`.`totalScores`,
			`asha_fields`.`ashaAccountNumber`,
			`employee_courses`.`employeeCourseID`,
			`users`.`userID`,
			`course_summary`.`totalAssign`,
			`quizess_report`.`obtainedPercentage`,
			`course_offering`.`offeringID`,
			`course_offering`.`offeringNumber`,
			`course_offering`.`completionDate`,
			`course_offering`.`endingDate`
			FROM
			`courses`
			LEFT JOIN `quizess_report`
			ON `courses`.`coursesID` = `quizess_report`.`courseId` 
			LEFT JOIN `users`
			ON `quizess_report`.`userId` = `users`.`userID`
			LEFT JOIN `asha_fields`
			ON `users`.`userID` = `asha_fields`.`userID` 
			LEFT JOIN `course_summary`
			ON `quizess_report`.`courseId` = `course_summary`.`coursesID` 
			LEFT JOIN `employee_courses`
			ON `users`.`userID` = `employee_courses`.`employeeID`
			AND `quizess_report`.`courseId` = `employee_courses`.`coursesID` 
			LEFT JOIN `course_offering`
			ON `quizess_report`.`courseId` = `course_offering`.`coursesID`
			WHERE
			`quizess_report`.`obtainedPercentage` >= 80
			AND `quizess_report`.`ashaReport` = 1
			AND `course_offering`.`status` = 0 
			AND `asha_fields`.`address1` != "" 
			AND `asha_fields`.`city` != ""
			AND `asha_fields`.`state` !=""
			AND `asha_fields`.`zip` !=""
			AND `asha_fields`.`primaryPhone` !=""
			AND `courses`.`coursesID` = '.$course_id.'
			ORDER BY
			`users`.`lastName` ASC');
			return ($query->num_rows() > 0)?$query->result_array():FALSE;
}
	/*When Doing Some changing in where please check these function where also---- these should match with each other 
	function1 : get_refrence (in where user_include)
	function2 : get_total_pass_in_course(where exactly match to function3)
	function3 : asha_report
	*/
	function get_total_pass_in_course($course_id)
	{
			/*When Doing Some changing in where please check these function where also---- these should match with each other 
	function1 : get_refrence (in where user_include)
	function2 : get_total_pass_in_course(where exactly match to function3)
	function3 : asha_report
	*/
		$this->db->select('*');
		$this->db->join('asha_fields', 'quizess_report.userId = asha_fields.userID', 'inner');
		$this->db->join('users', 'quizess_report.userId = users.userID', 'inner');
		$where = "`quizess_report`.`courseId` = '".$course_id."' 
			AND `quizess_report`.`obtainedPercentage` >= 80
			AND `quizess_report`.`ashaReport` = 1
			AND `asha_fields`.`address1` != '' 
			AND `asha_fields`.`city` != ''
			AND `asha_fields`.`state` !=''
			AND `asha_fields`.`zip` !=''
			AND `asha_fields`.`primaryPhone` !='' ";
		$this->db->where($where);
		$this->db->from('quizess_report');
		$query = $this->db->get();
		return $query->num_rows();
	} 
	function check_reported($assign_course_id)
	{
		$where = array(
			"employeeCourseID" => $assign_course_id,
		);
		$this->db->where($where);
		$this->db->set('reported', '1');
		$this->db->update('employee_courses');
	}

	function get_courses()
	{
		$query = $this->db->query('SELECT
		`courses`.`coursesID`,
		`courses`.`displayName`,
		`course_category`.`categoryID`,
		`category`.`slug`,
		`category`.`categoryName`
		FROM
		`courses`
		JOIN `course_category`
		ON `courses`.`coursesID` = `course_category`.`courseID` 
		JOIN `category`
		ON `course_category`.`categoryID` = `category`.`categoryID`
		WHERE `courses`.`reportAsha` = "1" GROUP BY `courses`.`coursesID`');
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function get_courses_duplication_by_id($course_id)
	{
		$query = $this->db->query('SELECT
		`courses`.`coursesID`,
		`courses`.`displayName`,
		`course_category`.`categoryID`,
		`category`.`slug`,
		`category`.`categoryName`
		FROM
		`courses`
		JOIN `course_category`
		ON `courses`.`coursesID` = `course_category`.`courseID` 
		JOIN `category`
		ON `course_category`.`categoryID` = `category`.`categoryID`
		WHERE `courses`.`coursesID` = "'.$course_id.'"');
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	/*
	* This function get the users
	* return those users who fill all the mandotory information
	* for the ASHA Report
	* Code by Wajahat -- 18:55 -- August 24,2017
	*/
	function get_reference()
	{
		$this->db->select('users.userID, users.firstName,users.lastName, asha_fields.*');
		$this->db->join('asha_fields', 'users.userID = asha_fields.userID', 'inner');
		$where = "`users`.`userInclude` = 1";
		$this->db->where($where);
	 	$this->db->order_by('users.lastName', 'ASC');
		$this->db->from('users');
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
	} 
	/*
	* This function get the users
	* return those users who fill all the mandotory information
	* for the ASHA Report
	* Code by Wajahat -- 18:55 -- August 24,2017
	*/
	function get_users($course_id)
	{
		$query = $this->db->query('SELECT
		`users`.`userID`,
		`users`.`userName`
		FROM
		`users`
		LEFT JOIN `quizess_report`
		ON `users`.`userID` = `quizess_report`.`userId`
		WHERE `quizess_report`.`courseId` = "'.$course_id.'"');
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
	}

	/*
	* This function will return the quiz report
	* return courseid and other report related things
	* Code by Wajahat -- 18:55 -- August 24,2017
	*/
	function get_user_report_by_course_id($user_id, $course_id)
	{
		$this->db->select('reportId, userId, quizId, courseId, obtainedPercentage, obtainedScores, correctAnswer, status');
		$where = array(
			"userId" => $user_id,
			"courseId" => $course_id
		);
		$this->db->where($where);
		$this->db->from('quizess_report');
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
	}

	/*
	* This function will return the connected courses
	* return courseid and the connection id
	* Code by Wajahat -- 18:55 -- August 24,2017
	*/
	function get_connections($course_id)
	{
		$this->db->select('coursesID, connectWithID');
		$where = "coursesID = $course_id OR connectWithID = $course_id";
		$this->db->where($where);
		$this->db->from('course_connection');
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
	}

	/*
	* This function will check the report
	* this will check the report of the connected courses
	* from both of them the report having more number than other will be eligible for the asha report.
	* Code by Wajahat -- 18:55 -- August 24,2017
	*/
	function get_duplicate_report($user_id /*277*/, $connect_course_id /*288*/, $score /*100*/, $old_course_id /*227*/)
	{
		$report = $this->get_user_report_by_course_id($user_id, $connect_course_id);
		if(!empty($report))
		{
			if($report[0]['obtainedPercentage'] > $score)
			{
				$where = array(
					"reportId" => $report[0]['reportId']
				);
				$this->db->where($where);
				$this->db->set('ashaReport', '1');
				$update = $this->db->update('quizess_report');
				if($update)
				{
					$where2 = array(
						"userId" => $user_id,
						"courseId" => $old_course_id
					);
					$this->db->where($where2);
					$this->db->set('ashaReport', '0');
					$update = $this->db->update('quizess_report');
				}
			}else
			{
				$where2 = array(
						"userId" => $user_id,
						"courseId" => $old_course_id
				);
				$this->db->where($where2);
				$this->db->set('ashaReport', '1');
				$update = $this->db->update('quizess_report');
			}
		}else
		{
				$where2 = array(
						"userId" => $user_id,
						"courseId" => $old_course_id
				);
				$this->db->where($where2);
				$this->db->set('ashaReport', '1');
				$update = $this->db->update('quizess_report');
		}
	}

	function update_include_of_user($user_id)
	{
		$where = array(
			"userID" => $user_id
		);
		$this->db->where($where);
		$this->db->set('userInclude', '1');
		$this->db->update('users');
	}

	function redo_include()
	{
		$where = array(
			"userInclude" => 1
		);
		$this->db->where($where);
		$this->db->set('userInclude', '0');
		$this->db->update('users');
	}

	function asha_report_check($course_id)
	{
		$where = array(
			"coursesID" => $course_id
		);
		$this->db->where($where);
		$this->db->set('reportAsha', '0');
		$this->db->update('courses');
	}

	/* Model function for Cron Jobs*/

	/* 
		* This Function Returns the all active course offering Numbers.
		* Return Array
	*/
	function get_active_courses()
	{
		$this->db->select('*');
		$where = array(
			"status" => "0"
		);
		$this->db->where($where);
		$this->db->from('course_offering');
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
	}
	/* End of the Function  --- Code by Wajahat -- 14:11 -- September 12,2017 */

	/* 
		* This function change the status of the offering
		* if today date is greater or equal to the completion date of the offering
		* return void
	*/

		function change_active($offering_id)
		{
			$where = array(
				"offeringID" => $offering_id
			);
			$this->db->where($where);
			$this->db->set('status', '2');
			$query = $this->db->update('course_offering');
			return $query;
		}

	/* End of the Function  --- Code by Wajahat -- 14:12 -- September 12,2017 */

	/* 
			* This function get the offering
			* According to the course id and the today date
			* return array
	*/

			function get_today_offering()
			{
				$query = $this->db->query('SELECT 
					offeringID,
					status,
					offeringNumber
					FROM course_offering
					WHERE MONTH(completionDate) = MONTH(CURRENT_DATE()) AND YEAR(completionDate) = YEAR(CURRENT_DATE())
				');
				return ($query->num_rows() > 0) ? $query->result_array() : FALSE;	
			}

	/* End of the Function  --- Code by Wajahat -- 14:12 -- September 12,2017 */


	function update_offerings($offering_id)
	{
		$where = array(
			"offeringID" => $offering_id
		);
		$this->db->where($where);
		$this->db->set("status", "0");
		$this->db->update('course_offering');
	}

	function get_today_assigned_courses()
	{
		$query = $this->db->query('SELECT * FROM 
			`employee_courses` 
			WHERE 
			`courseDate` = CURDATE()
			GROUP BY `employee_courses`.`coursesID`
		');
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;	
	}

	function get_active_course_offer($course_id)
	{
		$this->db->select('course_offering.offeringID, course_offering.endingDate, course_offering.offeringNumber');
		$this->db->join('course_offering','course_offering.coursesID = courses.coursesID');
		$where = array(
			"courses.coursesID" => $course_id,
			"course_offering.status" => 0
		);
		$this->db->where($where);
		$this->db->from('courses');
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;	
	}

	function get_offering_by_id_pending($offering_id)
	{
		$this->db->select('course_offering.offeringID, course_offering.endingDate, course_offering.offeringNumber');
		$this->db->join('course_offering','course_offering.coursesID = courses.coursesID');
		$where = array(
			"course_offering.offeringID" => $offering_id,
			"course_offering.status" => 0
		);
		$this->db->where($where);
		$this->db->from('courses');
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;	
	}

	function update_assign_course_offering($offering_id, $course_id)
	{
		$query = $this->db->query('
			UPDATE employee_courses 
			SET offeringID = '.$offering_id.' WHERE coursesID = '.$course_id.' 
			AND courseDate = curdate() 
			AND year(curdate()) = year(courseDate)
		');
		return $query;
	}

	/* End Function for Cron Jobs*/

	/*Report Model Functions*/

	function course_individual_report($course_id)
	{
		$this->db->select('`courses`.`displayName`,`courses`.`ceCheck`,`users`.`firstName`,`users`.`lastName`,`asha_fields`.`ashaAccountNumber`,`asha_fields`.`ashaEmail`,`orders`.`transactionID`,`orders`.`PayerID`,`orders`.`ordersPrice`,`employee_courses`.`courseDate`');
        $this->db->join('employee_courses', '`courses`.`coursesID` = `employee_courses`.`coursesID`', 'LEFt');
        $this->db->join('users', '`employee_courses`.`employeeID` = `users`.`userID`', 'LEFt');
        $this->db->join('asha_fields', '`users`.`userID` = `asha_fields`.`userID`', 'LEFt');
        $this->db->join('orders', '`employee_courses`.`employeeID` = `orders`.`companyID` AND `employee_courses`.`coursesID` = `orders`.`courseID`', 'LEFt');
        $where = array (
           'courses.coursesID' => $course_id
        );
        $this->db->where($where);
        $this->db->order_by('users.firstName');
        $query = $this->db->get('courses');
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function course_individual_summary($course_id)
	{
		$query = $this->db->query('SELECT
			sum(`educatorEarning`.`earnings`) AS `earning`,
			sum(`educatorEarning`.`adminCommission`) AS `Admin Commission`,
			sum(`educatorEarning`.`totalPrice`) AS `total_earning`,
			`courses`.`displayName`,
			`users`.`firstName`,
			`co_author`.`authorID`,
			`co_author`.`userID`,
			`courses`.`ceCheck`
			FROM
			`courses`
			JOIN `co_author`
			ON `courses`.`coursesID` = `co_author`.`coursesID` 
			JOIN `educatorEarning`
			ON `co_author`.`userID` = `educatorEarning`.`authorID`
			AND `co_author`.`coursesID` = `educatorEarning`.`courseID`
			JOIN `users`
			ON `educatorEarning`.`authorID` = `users`.`userID`
			WHERE
			`courses`.`coursesID` = '.$course_id.'
			GROUP BY
			`co_author`.`userID`'
		);
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	/*End of the Report function*/


	function report_load_reports_in_db($data)
	{
		$rows = $this->db->insert('record_load_file', $data);
        return $rows;
	}

	function record_ref_reports_in_db($data)
	{
		$rows = $this->db->insert('record_ref_file', $data);
        return $rows;
	}

	function checking_reports_user($user_id)
	{
		$where = array(
			"userID" => $user_id
		);
		$this->db->where($where);
		$this->db->set('userInclude', '1');
		$query  = $this->db->update('users');
		return $query;
	}
	/*
		* This Function is used by the parent controller function (Name of Function: collect_raw_data)
		* This function return the ASHA Information of the user.
		* This function return the information of the if he had atleast one thing in ASHA Information 
	*/
	function get_asha_information($user_id)
	{
		$this->db->select('users.lastName, asha_fields.ashaAccountNumber, users.skipCheck, asha_fields.address1, asha_fields.city, asha_fields.state, asha_fields.zip, asha_fields.primaryPhone');
		$this->db->join('asha_fields', 'users.userID = asha_fields.userID', 'LEFt');
		$where =  'users.userID = "'.$user_id.'"AND( asha_fields.ashaAccountNumber is not null OR asha_fields.address1 is not null OR asha_fields.city is not null OR asha_fields.state is not null OR asha_fields.zip is not null OR asha_fields.primaryPhone is not null)';
		$this->db->where($where);
		$this->db->from('users');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	/*
		* This Function is used for the specific purpose to send email. it's not for long term usage
		* This function return the ASHA Information of the user.
		* This function return the information of the if he had atleast one thing in ASHA Information 
	*/
	function get_asha_for_email($offset)
	{
		$query = $this->db->query('SELECT `users`.`userID`
			FROM
			`users`
			JOIN `asha_fields`
			ON `users`.`userID` = `asha_fields`.`userID`
			WHERE   `users`.`ashaCredit` = 1 && (`asha_fields`.`address1` is  null 
			OR `asha_fields`.`city` is null 
			OR `asha_fields`.`state` is null 
			OR `asha_fields`.`zip` is null 
			OR `asha_fields`.`primaryPhone` is null
			OR `asha_fields`.`ashaEmail` is null)
			LIMIT 1000 OFFSET '.$offset.'');
		return $query->result_array();
	}

	function get_asha_information2($user_id)
	{
		$this->db->select('users.lastName, asha_fields.ashaAccountNumber, users.skipCheck, asha_fields.address1, asha_fields.city, asha_fields.state, asha_fields.zip, asha_fields.primaryPhone');
		$this->db->join('asha_fields', 'users.userID = asha_fields.userID', 'LEFt');
		$where =  'users.userID = "'.$user_id.'"';
		$this->db->where($where);
		$this->db->from('users');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function skip_asha($user_id)
	{
		$where = array(
			"userID" => $user_id
		);
		$this->db->where($where);
		$this->db->set("skipCheck", "1");
		$query = $this->db->update("users");
		return $query;
	}

	function enter_raw_data($user_id, $course_id, $quiz_id, $report_id, $last_name, $offering_id, $asha_account_number)
	{
		$this->db->select('rawID, lastName, ashaCode');
		$where = array(
			"userID" => $user_id,
			"courseID" => $course_id,
		);
		$this->db->where($where);
		$this->db->from('asha_raw_data');
		$check = $this->db->get();

		$data = array(
			"userID" => $user_id,
			"courseID" => $course_id,
			"quizID" => $quiz_id,
			"reportID" => $report_id,
			"ashaCode" => "ABDB",
			"lastName" => $last_name,
			"offeringID" => $offering_id,
			"ashaAccountNumber" => $asha_account_number,
			"createdDate" => date('Y-m-d h:i:s'),
			"modifiedDate" => date('Y-m-d h:i:s'),
			"deleted" => '0',
		);
		if(!empty($check->result_array()))
		{ 
			$this->db->where($where);
			$query = $this->db->update('asha_raw_data', $data);
		}else
		{
			$query = $this->db->insert('asha_raw_data', $data);
			$last_id = $this->db->insert_id();
            return $last_id;
		}
	}

	function missed_users_get()
	{
		$this->db->select("userID");
		$this->db->join('employee_courses', 'users.userID = employee_courses.employeeID', 'LEFt');
		$where = array(
			"employee_courses.reported" => 1
		);
		$this->db->where($where);
		$this->db->from("users");
		$this->db->group_by("employee_courses.employeeID");
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function insert_tracking($user_id)
	{
		$data = array (
			"userID" => $user_id
		);
		$query = $this->db->insert('ashaReportTracking', $data);
		$last_user_id = $this->db->insert_id();
		return $last_user_id;
	}

	function data_formation_for_raw_data($course_id, $offering_id)
	{
		$query = $this->db->query('SELECT
			`users`.`userID`,
			`users`.`lastName`,
			`courses`.`coursesID`,
			`quizess_report`.`quizId`,
			`quizess_report`.`reportId`,
            `asha_fields`.`ashaAccountNumber`,
            `asha_fields`.`address1`,
            `asha_fields`.`city`,
            `asha_fields`.`state`,
            `asha_fields`.`zip`,
            `asha_fields`.`primaryPhone`,
            `asha_fields`.`ashaEmail`,
            `employee_courses`.`offÿringID`
			FROM
			`courses`
			LEFT JOIN `quizess_report`
			ON `courses`.`coursesID` = `quizess_report`.`courseId` 
			LEFT JOIN `users`
			ON `quizess_report`.`userId` = `users`.`userID`
			LEFT JOIN `asha_fields`
			ON `users`.`userID` = `asha_fields`.`userID` 
			LEFT JOIN `course_summary`
			ON `quizess_report`.`courseId` = `course_summary`.`coursesID` 
			LEFT JOIN `employee_courses`
			ON `users`.`userID` = `employee_courses`.`employeeID`
			AND `quizess_report`.`courseId` = `employee_courses`.`coursesID` 
			LEFT JOIN `course_offering`
			ON `quizess_report`.`courseId` = `course_offering`.`coursesID`
			WHERE
		   `quizess_report`.`obtainedPercentage` >= 80
			AND `course_offering`.`status` = 0
			AND `employee_courses`.`offeringID` = '.$offering_id.'
			AND `courses`.`coursesID` = '.$course_id.'
			ORDER BY
			`users`.`lastName` ASC');
			return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function update_asha($course_id)
	{
		$where = array(
			"coursesID" => $course_id 
		);
		$this->db->where($where);
		$this->db->set("reportAsha", 1);
        $query = $this->db->update('courses');
        echo $query;
	}
// 227 241 259 261 263 265 269 270 273 274 275 276 277 279 280 281 283 285 286 287 289 290 291 293 294
	/*Genrating ASHA REPORTS MODEL*/

	function generate_reports_from_raw($course_id, $offering_id)
	{
		$this->db->select("asha_raw_data.userID, asha_raw_data.courseID, asha_raw_data.quizID, asha_raw_data.reportID, asha_raw_data.offeringID, asha_raw_data.ashaCode, asha_raw_data.lastName, asha_raw_data.ashaAccountNumber, users.lastName");
		$this->db->join('users', 'asha_raw_data.userID = users.userID', 'LEFT');
		$this->db->join('asha_fields', 'users.userID = asha_fields.userID', 'LEFt');
		$where =  'asha_raw_data.courseID = "'.$course_id.'" AND asha_raw_data.offeringID = "'.$offering_id.'" AND (asha_fields.address1 is not null AND asha_fields.city is not null AND asha_fields.state is not null AND asha_fields.zip is not null AND asha_fields.primaryPhone is not null)';
		$this->db->where($where);
		$this->db->from("asha_raw_data");
		$this->db->order_by('users.lastName');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	/*Inserting File Name of the Report*/
	
	function get_reporting_id($file_name)
	{
		$this->db->select("*");
		$where = array(
			"reportName" => $file_name
		);
		$this->db->where($where);
		$this->db->from("asha_report_files");
		$query = $this->db->get();
		$data_things = $query->result_array();

		if($query->num_rows() == 0)
		{
			$data = array(
				"reportName" => $file_name,
				"createdDate" => date('Y-m-d h:i:s'),
				"modifiedDate" => date('Y-m-d h:i:s'),
				"deleted" => "0"
			);
			$this->db->insert("asha_report_files", $data);
		    $last_id = $this->db->insert_id();
		    return $last_id;
		}else 
		{
			return $data_things['0']['filesID'];
		}
	}

	/*END Inserting File Name of the Report*/

	/*Get Total Assignee of the courses*/
	function get_total_assign($course_id, $offering_id)
	{
		$this->db->select("employeeID, coursesID");
		$where = array(
			"coursesID" => $course_id,
			"offeringID" => $offering_id
		);
		$this->db->where($where);
		$this->db->from('employee_courses');
		$query = $this->db->get();
		return $query->num_rows();
	}
	/*END Get Total Assignee of the courses*/

	/*inserting rows in the main table -- reporting_asha*/
			
	function reporting_of_the_asha($data, $course_id, $user_id, $course_number = "")
	{
		$this->db->select("*");
		$where = array (
			"courseID" => $course_id,
			"userID" => $user_id
		);
		$this->db->where($where);
		$this->db->from('reporting_asha');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			if(!empty($course_number))
			{
				$where = array(
					"courseNumber" => $course_number
				);
				$this->db->where($where);
				$query2 = $this->db->update('reporting_asha', $data);
				return $query2;
			}else
			{
				$this->db->where($where);
				$query2 = $this->db->update('reporting_asha', $data);
				return $query2;
			}
		}else 
		{
			$this->db->insert('reporting_asha', $data);
			$last_id = $this->db->insert_id();
		    return $last_id;
		}
	}

	/*END inserting rows in the main table*/

	/*this function will select report on the basis of file name*/

	function download_asha_report($file_id)
	{
		$var = '""';
		return $this->db->query("SELECT
		`reporting_asha`.`formType` as 'Form_Type',
		`reporting_asha`.`providerCode` as 'Provider_code',
		REPLACE(`reporting_asha`.`courseNumber`, ',*,*,', '".$var."') as 'CourseNumber',
		`reporting_asha`.`partsForm` as 'Part_forms',
		`reporting_asha`.`offeringCompletionDate` as 'Offering_complete_date',
		`reporting_asha`.`numberAttending` as 'number_attending',
		`reporting_asha`.`partialCredit` as 'Partial_credit'
		FROM
		`reporting_asha`
		WHERE fileID = ".$file_id."");

	}

	/*END this function will select report on the basis of file name*/

	/*List Number of Files of ASHA Reporting*/

	function get_files($params = array())
	{
		$this->db->select("filesID, reportName, createdDate");
		$where = array(
			"deleted" => 0
		);
		$this->db->where($where);
		$this->db->from("asha_report_files");
		$this->db->order_by("asha_report_files.createdDate", "DESC");

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	/*END List Number of Files of ASHA Reporting*/

	/*Get File name of the report*/
	function get_file_name($file_id)
	{
		$this->db->select('filesID, reportName, refFileName');
		$where = array(
			"filesID" => $file_id
		);
		$this->db->where($where);
		$this->db->from('asha_report_files');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	/*END Get File name of the report*/

	/*Listing of Reported Users*/

	function listing_of_reporting_users($file_id)
	{
		$query = $this->db->query('SELECT
		`reporting_asha`.`userID`
		FROM
		`reporting_asha`
		where `userID` is not null AND `fileID` = '.$file_id.'
		GROUP BY `userID`');
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	/*END Listing of Reported Users*/

	/*END Genrating ASHA REPORTS MODEL*/

	//Entering Raw data in the table for generating reports
	function insert_raw_data($data, $user_id, $course_id)
	{
		$this->db->select('rawID, lastName, ashaCode');
		$where = array(
			"userID" => $user_id,
			"courseID" => $course_id,
		);
		$this->db->where($where);
		$this->db->from('asha_raw_data');
		$check = $this->db->get();
		if($check->num_rows() > 0)
		{
			$this->db->where($where);
			$this->db->update("asha_raw_data", $data);
		}else
		{
			$this->db->insert("asha_raw_data", $data);
		}
	}

	//Update the reference file name in the datbase on the base of file number
	function update_name_of_ref_file($file_id, $file_name)
	{
		$where = array (
			"filesID" => $file_id
		);
		$this->db->where($where);
		$this->db->set("refFileName", $file_name);
		$this->db->update("asha_report_files");
	}

	//Function return the course to reports of the ASHA monthwise
	function get_monthly_active_records($year, $month, $params = array())
	{
		$this->db->select("`course_offering`.`offeringNumber`,`course_offering`.`offeringID`, `course_offering`.`completionDate`,`course_offering`.`endingDate`,`course_offering`.`status`,`courses`.`displayName`,`courses`.`price`,`courses`.`coursesID`, `course_offering`.`partForms`");
		$this->db->join('courses', 'course_offering.coursesID = courses.coursesID', 'LEFT');
		$where = "MONTH(completionDate) = ".$month." AND YEAR(completionDate) = ".$year;
		$this->db->where($where);
		$this->db->from('course_offering');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function to_track_send_email($data)
	{
		foreach ($data as $key) {
			$data2 = array(
				"userID" => $key['userID']
			);
			$this->db->insert('send_email_track', $data2);
		}
	}

	function select_distinct()
	{
		$query = $this->db->query('SELECT DISTINCT `send_email_track`.`userID` FROM send_email_track');
		return $query->result_array();
	}

	function update_new_table($user_id)
	{
		$data = array(
			"userID" => $user_id
		);
		$this->db->insert("new_table", $data );
	}

	function update_include_check()
	{
		$query = $this->db->query('SELECT
`asha_fields`.`country`,
`asha_fields`.`ashaAccountNumber`,
`asha_fields`.`userID`
FROM
`csv_upload`
JOIN `asha_fields`
ON `csv_upload`.`column2` = `asha_fields`.`ashaAccountNumber`');
		$query->result_array();
	}

	//Temporary
// 	function unactive_and_active($course_id)
// 	{
// 		$query = $this->db->query('UPDATE `employee_courses`
// JOIN `course_offering` ON `employee_courses`.`coursesID` = `course_offering`.`coursesID`
// SET `employee_courses`.`offeringID` = `course_offering`.`offeringID`
// WHERE (`employee_courses`.`courseDate` BETWEEN "2017-06-01" AND "2017-08-31")
// AND `employee_courses`.`coursesID` = '.$course_id.' AND `course_offering`.`status` = "0"');
// 		echo $query;
// 	}

// 	function insert_in_new_one()
// 	{
// 		$query = $this->db->query('SELECT * FROM `quizess_report` WHERE `ashaReport` = 1 LIMIT 30000 OFFSET 60000');
// 		return ($query->num_rows() > 0)?$query->result_array():FALSE;
// 	}

// 	function insert_again($data)
// 	{
// 		$query = $this->db->insert("quize_report_duplicate", $data);
// 		echo $query;
// 	}
}