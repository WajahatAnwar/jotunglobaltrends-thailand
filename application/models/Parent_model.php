<?php

class Parent_model extends CI_Model {
		
	public function __construct() {
		parent::__construct();

	}
	
	
	
	function get_categories()
	{
		$this->db->select('*');
			$where = array (
						'categoryDeleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('category');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	
	
	function get_membership_by_id($membership_id)
	{
		$this->db->select('*');
		$where = array (
						'membershipID' => $membership_id,
					);
		$this->db->where($where);
		$this->db->from('membership');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	
	function get_membership_packages()
	{
		$this->db->select('*');
		$where = array (
						'deleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('membership');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	
	function get_profile_image($user_id)
	{
		$this->db->select('*');
		$where = array (
						'userID' => $user_id,
						'userMetaKey' => 'file_name',
					);
		$this->db->where($where);
		$this->db->from('users_meta');
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			$rows = $query->result_array();
			return $rows[0]['userMetaValue'];
		} else {
			return false;
		}
	}
	


	      


	

	    /**
    * get client users data from  the database, 
    * store it in a new array and return it to the controller 
    * @return array
    */
	function company_report()
	{
		
	
				return $query = $this->db->query("SELECT 
										users.userName,
										users.userEmail,
										users.userPhoneNo
										 FROM `users` 
										 WHERE  userDeleted = '0'
										");
			
		  
	
	} 

	/**
    * get show membership package  data from  the database, 
    * store it in a new array and return it to the controller 
    * @return array
    */

	function get_company_phone($phone_no)
	{

			$clientID = $this->session->userdata('userID');
			$query = $this->db->query("SELECT *
							FROM `users`
							WHERE userPhoneNo like '" .$phone_no. "%' OR userName like '" .$phone_no. "%' 
							ORDER BY userPhoneNo
							LIMIT 0,10");
			$skillarryhold = '';
		 	if($query->num_rows() > 0)
		 	{
			 	$rows  = $query->result();						
													
				foreach ($rows as $row)
				{		

					$cashbackComission = 0;
					$charityComission = 0;
															
					$skillarryhold .= ' <ul id="name-list"><li class="phone_click" onClick="selectname('.$cashbackComission.','.$charityComission.','.$row->userPhoneNo.','.$row->userID.');"><span class=""><i class="fa fa-phone" style="/*background: #e5e5e5;*/ padding: 12px 27px 12px 12px;margin-top: 0;" ></i></span><span class="">'.$row->userName.'  - '.$row->userPhoneNo.'</span></li></ul>';													
				}
													
				return $skillarryhold;	
			}
}



	public function search ($title){
        $this->db->select('userName');
        $this->db->select('userPhoneNo');
        $this->db->like('userName', $title, 'both');
        return $this->db->get('users');
    }

 	function get_user($id)
    {
        $this->db->select('*');
		$this->db->join('orders', 'users.userID = orders.companyID','LEFT');
		$where = array (
						'users.userID' => $id,
					);
		$this->db->where($where);
		$this->db->from('users');
        $query = $this->db->get();
        $rows = $query->result_array();
        return $rows;
    }

    function get_user_by_ids($user_id)
	{
		$this->db->select('*');
		$where = array (
						'userID' => $user_id,
					);
		$this->db->where($where);
		$this->db->from('users');

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	
	



}

