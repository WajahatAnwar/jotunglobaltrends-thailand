<?php

class Auth_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->library('encrypt');

	}

	function check_password($userName) {
			// $this->db->where("userEmail",$userName);
		// $where = '(userEmail="'.$userName.'" or userPhoneNo = "'.$userName.'")';
		// $this->db->where($where);
		$this->db->where("userEmail",$userName);
		// $this->db->where("userDeleted",'0');
		$query = $this->db->get("users");
		if($query->num_rows()>0){
			$row = $query->result_array();
			$password = $this->encryption->decrypt($row[0]['userPassword']);
			return $password;
		} else {
			return false;
		}
	}

	function verify_login($userName,$userPassword) {

		$decrypt_password = $this->check_password(strtolower($userName));
		$where = array(
			"userEmail" => strtolower($userName),
			"userDeleted" => '0'
		);
		$this->db->where($where);
		$query = $this->db->get("users");

		if($query->num_rows() > 0)
		{
			$row = $query->result_array();
			if(($userPassword == $decrypt_password && $userName == $row[0]['userPhoneNo']) || ($userPassword == $decrypt_password && strtolower($userName) == $row[0]['userEmail']))	
			{
				$newdata = array(
						'userID'  => $row[0]['userID'],
						'userName'   => $row[0]['userName'],
						'userEmail'   => $row[0]['userEmail'],
						'userPhoneNo'   => $row[0]['userPhoneNo'],
						'userType'   => $row[0]['userType'],
						'is_logged_in' => TRUE
				);
				return ($row);
			}else 
			{
				return false;
			}
		}else  
		{
			return false;
		}
	 }

    /**
    * Serialize the session data stored in the database,
    * store it in a new array and return it to the controller
    * @return array
    */
	function get_db_session_data()
	{
		$query = $this->db->select('user_data')->get('ci_sessions');
		$user = array(); /* array to store the user data we fetch */
		foreach ($query->result() as $row)
		{
		    $udata = unserialize($row->user_data);
		    /* put data in array using username as key */
		    $user['user_name'] = $udata['user_name'];
		    $user['is_logged_in'] = $udata['is_logged_in'];
		}
		return $user;
	}

    /**
    * get client users data from  the database,
    * store it in a new array and return it to the controller
    * @return array
    */
	function operators_report()
	{


				return $query = $this->db->query("SELECT
										users.userName,
										users.userEmail,
										users.userPhoneNo
										 FROM `users`
										 WHERE `userType` = 'operator' AND `userDeleted` = '0'
										");



	}
	
    /**
    * get client users data from  the database,
    * store it in a new array and return it to the controller
    * @return array
    */
	function get_users_info($userID)
	{
		$this->db->select('*');
		$where = array (
						'userID' => $userID,
						'userDeleted' => '0'
					);
		$this->db->where($where);
		$query = $this->db->get('users');
		
		 $skillarryhold = '';
		 if($query->num_rows() > 0)
		 {
			 return $query->result_array();	 	
		 }else
		 {
			return 'no records';
		 }
	}

	function delete_users($userID)
	{
		$this->db->select('userDeleted');
		$where = array (
						'userID' => $userID
					);
		$this->db->where($where);
		$query = $this->db->get('users');

		 if($query->num_rows() > 0){
			 $row = $query->result_array();
			  if($row[0]['userDeleted'] == 0) {
				  $user_deleted = 1;
			  }
			  else {
				  $user_deleted = 0;
			  }
			$this->db->where(array("userID" => $userID));
			$this->db->set('userDeleted',$user_deleted);
            $this->db->update('users');

		 }



	}
	function update_user($data , $userID)
	{
			$this->db->where(array("userID" => $userID));
			$this->db->set($data);
            $this->db->update('users');
			 return true;
	}

	function get_membership_type($membership_id)
	{
		$this->db->select('*');
		$where = array (
						'membershipID' => $membership_id,
						'membershipDeleted' => '0'
					);
		$this->db->where($where);
		$query = $this->db->get('membership');
		 $skillarryhold = '';
		 // $skillarryhold = '';
		 if($query->num_rows() > 0){
			 $row = $query->result_array();
			  return $row[0]['membershipExpiredDate'];

		 }else
		 {
			return 'no records';
		 }
	}
	function get_categories()
	{
		$this->db->select('*');
		$where = array (
						'categoryDeleted' => '0'
					);
		$this->db->where($where);
		$query = $this->db->get('category');
		 $skillarryhold = '';
		 // $skillarryhold = '';
		 if($query->num_rows() > 0){
			// print_r($query->result());
			 // return $query->result_array();
			 	$rows     = $query->result();
												foreach ($rows as $row)
												{
													$skillarryhold .= '<option value="'.$row->categoryID.'">'.$row->categoryName.'</option>';
												}
												 // print_r($skillarryhold);
												return $skillarryhold;
		 }else
		 {
			return 'no records';
		 }
	}

function forgotPassword($email)
{
	$this->db->select('*');
	$where = array (
				'userEmail' => $email,
				'userDeleted' => '0'
	);
	$this->db->where($where);
	$query = $this->db->get('users');
		 
    return $query->result_array();
         
}

public function add_profile_image($attach_files, $userID)
{

        if ($attach_files) {
            $this->db->where(array("userID" => $userID));
            $this->db->set('userProfileImage', $attach_files);
            $this->db->update('users');
        }

        //return  $insert_id;
}


	/*
	*Fetch info of the profile
	* aud update function for the profile of the all types of the user
	*code by wajahat
	*05-09-2016
	*/
	function get_profile_info($id){

		$this->db->select('*');
		$this->db->join('users_meta', 'users.userID = users_meta.userID','LEFt');
		$where = array (
						'users.userID' => $id,
					);
		$this->db->where($where);

		$query = $this->db->get('users');
		$row = $query->result_array();

		return $row;
	}

	function update_profile_info($id, $UpdateData){

		$where = array (
						'userID' => $id
					);
          $update = $this->db->update('users' , $UpdateData , $where);
		  if($update)
			{
				// return $pass;
				return $update;
			}
	}


	function change_status($status_name,$status_value,$user_id)
	{
		if($status_name == 'user_status') {
							$this->db->where(array("userID" => $user_id));
									$this->db->set('userStatus',$status_value);
				}
            $query = $this->db->update('users');
			 return $query;
	}

	function change_user_role($user_id)
	{
		$where = array(
			"userID" => $user_id
		);
		$this->db->where($where);
		$this->db->set('userType', "student");
		$query  = $this->db->update('users');
		return $query;
	}

	function add_signup($data){
	// Inserting in Table(users) of Database(college)
	$this->db->select('userCompanyName , userEmail ');
		$where = array (
						'userEmail' => $data['userEmail'],
						'userName'  => $data['userName']
					);
		$this->db->where($where);
		$query = $this->db->get('users');

				 if($query->num_rows() > 0) {
					 				  return false;
				 } else {
							$result = $this->db->insert('users', $data);
								if($result)
								{
									$last_user_id = $this->db->insert_id();
									return $last_user_id;

								}
						}
	}
	function add_instructor($data){
	
		$this->db->select('userCompanyName , userEmail ');
		$where = 'users.userEmail = "'.$data['userEmail'].'" AND users.userDeleted = 0';
		$this->db->where($where);
		$query = $this->db->get('users');

				if($query->num_rows() > 0) 
				{
					return false;
				}else 
				{
					$result = $this->db->insert('users', $data);
					
						$last_user_id = $this->db->insert_id();
						return $last_user_id;
				
				}
	}

	function add_student($data){
		
		$this->db->select('userCompanyName , userEmail ');

		$where = 'users.userEmail = "'.$data['userEmail'].'" AND users.userDeleted = 0';
		
		$this->db->where($where);
		$query = $this->db->get('users');
		if($query->num_rows() > 0) 
		{
			return false;
		}else 
		{
			$result = $this->db->insert('users', $data);
			if($result)
			{
				$last_user_id = $this->db->insert_id();
				return $last_user_id;

			}
		}
	}
	function account_activate($result){

	$this->db->select('hash , userEmail,userName ');
		$where = array (
						'userID' => $result,
					);
		$this->db->where($where);
		$query = $this->db->get('users');
		$row = $query->result_array();

		return $row;
	}

	function verify_acc($user_email,$verification_code){

	$this->db->select('*');
		$where = array (
						'userEmail' => $user_email,
						'hash' => $verification_code,
					);
		$this->db->where($where);
		$query = $this->db->get('users');
		$row = $query->result_array();

		if($query->num_rows() > 0)
		{
			$where = array (
				'userEmail' => $user_email,
				'hash' => $verification_code,
			);
			$this->db->where($where);
			$result = $this->db->set('verified', '1');
			$query = $this->db->update('users');
			if($query){
				return true;
			}else{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	function auto_login($user_email){

		$this->db->select('*');
		$where = array (
						'userEmail' => $user_email,
					);
		$this->db->where($where);
		$query = $this->db->get('users');
		$row = $query->result_array();
		return $row;

	}

	function get_course_price($course_id){

		$this->db->select('price');
		$where = array (
						'coursesID' => $course_id,
					);
		$this->db->where($where);
		$query = $this->db->get('courses');
		$row = $query->result_array();
		return $row;
	}

	function get_tabs($id)
	{

		$this->db->select('users_key.keyName,users_key.keyType,users_key.keyTab,users_key.keyID');
		$this->db->from('users_key');
		$this->db->group_by("users_key.keyTab");
		$this->db->order_by("users_key.keyName", "ASC");
		$query = $this->db->get();
		$row = $query->result_array();

		return $row;
	}

	function tabs_content($id)
	{

		$this->db->select('*');
		$this->db->from('users_key');
		$this->db->order_by("users_key.keyName", "ASC");
		$query = $this->db->get();
		$row = $query->result_array();

		return $row;
	}
	function tab_contents($id)
	{
		
		$this->db->select('users.userName,users_meta.userMetaKey,users_key.keyID,users_key.keyName,users_meta.userMetaValue');
		$this->db->join('users_meta', 'users_meta.userID = users.userID','Inner');
		$this->db->join('users_key', 'users_key.keyID = users_meta.userMetaKey','Inner');
		$where = array(
						"users_meta.userID" => $id,
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by("users_key.keyName", "ASC");
		$query = $this->db->get();
		$row = $query->result_array();

		return $row;
	}

	function types($id)
	{

		$this->db->select('users_meta.userMetaKey,users_meta.userMetaValue,users_meta.tabs,users_meta.type');
		$this->db->join('users_meta', 'users.userID = users_meta.userID','LEFt');
		$where = array (
						'users.userID' => $id,
					);
		$this->db->where($where);
		$this->db->group_by("type");
		$query = $this->db->get('users');
		$row = $query->result_array();

		return $row;
	}

	function update_meta_info($data){

	// Inserting in Table(users) of Database(college)

	$this->db->select('*');
		$where = array (
						'userID' => $data['userID'],
						'userMetaKey' => $data['userMetaKey'],
					);
		$this->db->where($where);
		$query = $this->db->get('users_meta');

				 if($query->num_rows() > 0) {
						$where2 = array (
							'userID' => $data['userID'],
							'userMetaKey' => $data['userMetaKey'],
						);
						$update = $this->db->update('users_meta' , $data , $where2);
						return $update;
				  }
				  else {
							$result = $this->db->insert('users_meta', $data);
								if($result)
								{
									$last_user_id = $this->db->insert_id();
									return $last_user_id;

								}
						}
	}
    
    function helpOff($user_id)
    {
            $this->db->where(array("userID" => $user_id));
			$this->db->set('help','1');
            $query = $this->db->update('users');
            return $query;

    }
	
	function add_asha($data)
	{
		$this->db->select('*');
		$where = array(
			"userID" => $data['userID']
		);
		$this->db->where($where);
		$this->db->from('asha_fields');
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$this->db->where($where);
			$this->db->update('asha_fields', $data, $where);
			return true;
		}else
		{
			$this->db->insert('asha_fields', $data);
			return true;
		}
	}
	
	function get_asha($user_id)
	{
		$this->db->select('*');
		$where = array(
			"userID" => $user_id
		);
		$this->db->where($where);
		$this->db->from('asha_fields');
		
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function update_username($data)
	{
		$where = array(
			"users.userID" => $data['userID']
		);
		$this->db->where($where);
		$this->db->set('userName',$data['uri']);
        $query = $this->db->update('users');
	}
	
	function check_availability($user_name)
	{
		$this->db->select("users.userName");
		$where = array(
			"users.userName" => $user_name
		);
		$this->db->where($where);
		$this->db->from('users');
		
		$query = $this->db->get();
		
		return  $query->result_array();
	}

	function zoom_array($data)
	{
		$this->db->select('*');
		$where = array(
			"userID" => $data['userID'],
			"courseID" => $data['courseID'],
			"lessonID" => $data['lessonID'],
			"id" => $data['id']
		);
		$this->db->where($where);
		$this->db->from('zoom_credentials');

		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return false;
		}else{
			$this->db->insert('zoom_credentials', $data);
		}
	}

	function check_user_has_courses($user_id)
	{
		$this->db->select('*');
		$where = array(
			"userID" => $user_id,
			"authorDeleted" => 0
		);
		$this->db->where($where);
		$this->db->from('co_author');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	//This Function use for the report of the user who fill their ASHA Information.
	//When we send them email for the Last ASHA Reporting
	function track_users($user_email, $verification_code, $data)
	{
		$this->db->select("*");
		$where = array (
			"userEmail" => $user_email,
			"verificationCode" => $verification_code,
		);
		$this->db->where($where);
		$this->db->from('track_email_users');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			$this->db->where($where);
			$this->db->update('track_email_users', $data);
		}else
		{
			$this->db->insert('track_email_users', $data);
		}

	}
}

