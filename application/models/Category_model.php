<?php

class Category_model extends CI_Model{
	function __construct() 
	{
	parent::__construct();
	}	
	function add_category($data)
	{
		
		$query = $this->db->insert('category', $data);
		if($query)
		{
			$last_user_id = $this->db->insert_id();
			return $last_user_id;
		}
	}
	function all_categories()
	{

		$this->db->select('categoryName,categoryID');
					$where = array (
						'categoryDeleted' => '0',
					);
		$this->db->where($where);
		$query = $this->db->get('category');
	
			 $row = $query->result_array();
			  
			return $row;
	}
	function get_categories($params = array())
	{
		$this->db->select('*');
					$where = array (
						'categoryDeleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('category');
		// $this->db->from('users');
		$this->db->order_by('categoryID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();
		// print_r($query->num_rows());
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function delete_category($category_id)
	{
		$this->db->select('categoryDeleted'); 
		$where = array (
						'categoryID' => $category_id
					);
		$this->db->where($where);
		$query = $this->db->get('category');
	
		 if($query->num_rows() > 0){
			 $row = $query->result_array();
			  if($row[0]['categoryDeleted'] == 0) {
				  $category_deleted = 1;
			  }
			  else {
				  $category_deleted = 0;
			  }
			$this->db->where(array("categoryID" => $category_id));
			$this->db->set('categoryDeleted',$category_deleted);
            $update = $this->db->update('category');
			return $update;
		 }
	}
	function update_category($data , $category_id) {
		
	$where = array (
						'categoryID' => $category_id,
					);
		$this->db->where($where);
		$result = $this->db->update('category', $data); 
		if($result) {
			
		return $result;
		} else {
			
		return false;
		}
	}
	function get_category_by_id($category_id)
	{
		$this->db->select('*');
		$where = array (
						'categoryID' => $category_id,
					);
		$this->db->where($where);
		$this->db->from('category');
		$this->db->order_by('categoryID','desc');

			$this->db->limit(1);
		

				$query = $this->db->get();
				if(($query->num_rows() > 0)){
						 $row = $query->result_array();   
		
				return ($row);
				} else {
					return false;
				}
	}
	
	function getCategories()
	{
		$this->db->select('*');
			$where = array (
				'parentID' => null,
				'categoryDeleted'=>'0'
			);
		$this->db->where($where);
		$this->db->order_by('categoryName','ASC');
		$query=$this->db->get('category');
        if ($query->num_rows() > 0)
        {
            $result =$query->result_array();
			
            for($i=0;$i<count($result);$i++)
            {
                $query1=$this->db->query("select * from category where parentID='".$result[$i]['categoryID']."' AND categoryDeleted = 0 ");
                if($query1->num_rows() > 0)
                {
                $result[$i]['sub']=$query1->result_array();
				
                }
                else
                {
                // $result[$i]['sub']=array();
                }
            }
            return $result;
        }
        else
        {
            // return $query->result_array();
        }

	}

	function get_categories_by_id($id)
	{
		$this->db->select('*');
			$where = array (
				'categoryID' => $id,
				'categoryDeleted'=>'0'
			);
		$this->db->where($where);
		$query=$this->db->get('category');
		return $query->result_array();

	}
    
    function selectedCategories($course_id)
    {
        $this->db->select('categoryID');
			$where = array (
				'courseID' => $course_id
			);
        $this->db->where($where);
		$query=$this->db->get('course_category');
        return $query->result_array();
    }

    function update_category_slug($data)
	{
		$where = array(
			"category.categoryID" => $data['categoryID']
		);
		$this->db->where($where);
		$this->db->set('slug',$data['uri']);
        $query = $this->db->update('category');
        if($query)
        {
        	return $query;
        }
	}
}