<?php

class Dashboard_model extends CI_Model {
		public function __construct() {
		parent::__construct();
		$this->load->library('encrypt');

	}
	
	public function get_orders($params = array())
	{
		$this->db->select('*');
		$this->db->join('orders', 'orders.companyID = users.userID','right');
		$this->db->from('users');
		$this->db->order_by('userID','desc');
		$this->db->limit('5');
		$query = $this->db->get();
		
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function get_all_employee($company_id,$params = array())
	{

		$this->db->select('*');
		$where = "userType='employee' AND companyID='".$company_id."' AND users.userDeleted = 0";
		$this->db->where($where);
		$this->db->from('users');

		$query = $this->db->get();
		
		if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit'], $params['start']);
        } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit']);
        }
		// return $company_id;
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	
	function total_educator_students($role)
	{
		$query = $this->db->query("SELECT *
                    FROM `users` where
                    userDeleted = 0 AND userType='".$role."' 
                    ORDER BY users.firstName");
		return $query->num_rows();
	}

	function course_listing()
	{
		$this->db->select('courses.coursesID,courses.displayName,slugs.uri');
        $this->db->join('slugs', 'courses.coursesID = slugs.id','inner');
        $where = "courses.coursesStatus = 'publish'";
		$this->db->where($where);
        $this->db->from('courses');
		$this->db->order_by('courses.coursesID DESC');
		$this->db->limit(5);
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;

	}

	function get_all_student($instructor_id,$params = array())
	{

		$this->db->select('courses.displayName,slugs.uri,users.userName');
		$this->db->join('employee_courses', 'users.userID = employee_courses.employeeID','inner');
        $this->db->join('courses', 'employee_courses.coursesID = courses.coursesID','inner');
        $this->db->join('slugs', 'courses.coursesID = slugs.id','inner');
        $this->db->join('co_author', 'courses.coursesID = co_author.coursesID','inner');
		$where = "co_author.userID = '".$instructor_id."'";
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('courses.coursesID DESC, employee_courses.courseDate DESC');
		$this->db->limit(5);
		$query = $this->db->get();
		
		return ($query->num_rows() > 0)?$query->result_array():FALSE;

	}

	function get_all_student_csv($instructor_id)
	{
		return $this->db->query('SELECT
		`users`.`firstName` as "First Name",
		`users`.`lastName` as "Last Name",
		`users`.`userEmail` as "User Email",
		`courses`.`coursesID` as "Course ID",
		`courses`.`displayName` as "Course Name",
		`employee_courses`.`courseDate` as "Completion Date",
		`quizess_report`.`obtainedPercentage` as "Percentage Obtained",
		`quizess_report`.`modifiedDate` as "Quiz Completed"
		FROM
		`co_author`
		LEFT JOIN `courses`
		ON `co_author`.`coursesID` = `courses`.`coursesID` 
		JOIN `employee_courses`
		ON `courses`.`coursesID` = `employee_courses`.`coursesID` 
		JOIN `users`
		ON `employee_courses`.`employeeID` = `users`.`userID` 
		LEFT JOIN `quizess_report`
		ON `employee_courses`.`employeeID` = `quizess_report`.`userId`
		AND `employee_courses`.`coursesID` = `quizess_report`.`courseId`
		WHERE
		`co_author`.`userID` = "'.$instructor_id.'" ORDER BY courses.coursesID DESC, employee_courses.courseDate DESC');
	}
    
    function get_instructor_courses($instructor_id , $params = array())
    {
        
        $this->db->select('*');
        $this->db->join('courses', 'co_author.coursesID = courses.coursesID','inner');
        $this->db->join('slugs', 'courses.coursesID = slugs.id','inner');
		$where = "co_author.userID = '".$instructor_id."'";
		$this->db->where($where);
		$this->db->from('co_author');

        if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit'], $params['start']);
        } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit']);
        }

        $query = $this->db->get();
        // print_r($query->result_array()); exit;
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }
    
    
	function get_employee_assign()
	{
		$this->db->select('courses.coursesName,courses.coursesID');
		$where = array (
						'coursesDeleted' => '0'
					);
		$this->db->where($where);
		$this->db->group_by('courses.coursesID');
		$this->db->from('courses');
		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function get_employee_assignned($user_id)
	{
		$this->db->select('users.userID,courses.coursesName,courses.coursesID,courses.passingCriteria,');
		$this->db->join('employee_courses', 'users.userID = employee_courses.employeeID','inner');
		$this->db->join('courses', 'employee_courses.coursesID = courses.coursesID','inner');
		$where = array (
						'users.userID'=> $user_id,
						'userDeleted' => '0'
					);
		$this->db->where($where);
		// $this->db->group_by('courses.coursesID');
		$this->db->from('users');
		$query = $this->db->get();

		return $query->num_rows();
	}
	function get_course_summary($user_id)
	{
		$this->db->select('users.userID, users.userName, courses.coursesName, courses.start_time, courses.liveCheck, courses.coursesID, courses.passingCriteria, courses.displayName, quizzes.quizName, quizzes.quizID, quizzes.totalScore, courses.categoryID, quizess_report.obtainedPercentage, quizess_report.quizId, category.categoryName, employee_courses.startCourse, employee_courses.courseDate, slugs.uri');
		$this->db->join('employee_courses', 'users.userID = employee_courses.employeeID','left');
		$this->db->join('courses', 'employee_courses.coursesID = courses.coursesID','left');
		$this->db->join('slugs', 'courses.coursesID = slugs.id','left');
		$this->db->join('quizess_report', 'employee_courses.coursesID = quizess_report.courseId AND employee_courses.employeeID = quizess_report.userId','left');
		$this->db->join('category', 'courses.categoryID = category.categoryID','left');
		$this->db->join('quizzes', 'quizess_report.quizId = quizzes.quizID','left');
		$where = array (
						'users.userID'=> $user_id,
						'userDeleted' => '0'
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('courses.displayName', 'ASC');
		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function get_fail($user_id)
	{
		$this->db->select('users.userID,users.userName,courses.coursesName,courses.coursesID,courses.passingCriteria,quizzes.quizName,,quizzes.quizID,quizzes.totalScore,courses.categoryID,quizess_report.obtainedPercentage,category.categoryName,employee_courses.startCourse');
		$this->db->join('employee_courses', 'users.userID = employee_courses.employeeID','left');
		$this->db->join('courses', 'employee_courses.coursesID = courses.coursesID','left');
		$this->db->join('quizess_report', 'employee_courses.coursesID = quizess_report.courseId AND employee_courses.employeeID = quizess_report.userId','left');
		$this->db->join('category', 'courses.categoryID = category.categoryID','left');
		$this->db->join('quizzes', 'quizess_report.quizId = quizzes.quizID','left');
		$where = ('quizess_report.obtainedPercentage < courses.passingCriteria AND users.userID = "'.$user_id.'"');
		$this->db->where($where);
		$this->db->from('users');
		$query = $this->db->get();

		return $query->num_rows();
	}

	function get_pass($user_id)
	{
		$query = $this->db->query('SELECT
			users.userID,users.userName,
			courses.coursesName,
			courses.coursesID,
			courses.passingCriteria,
			quizzes.quizName,
			quizzes.quizID,
			quizzes.totalScore,
			courses.categoryID,
			quizess_report.obtainedPercentage,
			category.categoryName,
			employee_courses.startCourse
			FROM
			`users`
			LEFT JOIN `employee_courses`
			ON `users`.`userID` = `employee_courses`.`employeeID`
			LEFT JOIN `courses`
			ON `employee_courses`.`coursesID` = `courses`.`coursesID` 
			LEFT JOIN `quizess_report`
			ON `employee_courses`.`coursesID` = `quizess_report`.`courseId` AND `employee_courses`.`employeeID` = `quizess_report`.`userId`
			LEFT JOIN `category`
			ON `courses`.`categoryID` = `category`.`categoryID`
			LEFT JOIN `quizzes`
			ON `quizess_report`.`quizId` = `quizzes`.`quizID`
			WHERE
			users.userID = "'.$user_id.'" AND quizess_report.obtainedPercentage >= courses.passingCriteria'
		);

		return $query->num_rows();
	}

	

	function get_name($user_name)
	{
		$this->db->select('users.userID,users.userName, users.fileName, users.firstName, users.lastName');
		$this->db->join('users_meta', 'users.userID = users_meta.userID','left');
		$where = array (
						'users.userName' => $user_name,
						'users.userDeleted' => '0'
					);
		$this->db->where($where);
		$this->db->from('users');
		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
}

