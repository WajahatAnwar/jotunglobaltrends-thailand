<?php

class Earning_model extends CI_Model {
		
	public function __construct() {
		parent::__construct();

	}
	public function month_wise_earning($cur_year)
	{
		if(!empty($cur_year)){
			$cur_year = $cur_year;
			
		} 
		else {
			$cur_year = 'YEAR(CURDATE())';
		}
		$query = $this->db->query("SELECT 

	    SUM(IF(MONTH = 1, numRecords, NULL)) AS 'January',
	    SUM(IF(MONTH = 2, numRecords, NULL)) AS 'Feburary',
	    SUM(IF(MONTH = 3, numRecords, NULL)) AS 'March',
	    SUM(IF(MONTH = 4, numRecords, NULL)) AS 'April',
	    SUM(IF(MONTH = 5, numRecords, NULL)) AS 'May',
	    SUM(IF(MONTH = 6, numRecords, NULL)) AS 'June',
	    SUM(IF(MONTH = 7, numRecords, NULL)) AS 'July',
	    SUM(IF(MONTH = 8, numRecords, NULL)) AS 'August',
	    SUM(IF(MONTH = 9, numRecords, NULL)) AS 'September',
	    SUM(IF(MONTH = 10, numRecords, NULL)) AS 'October',
	    SUM(IF(MONTH = 11, numRecords, NULL)) AS 'November',
	    SUM(IF(MONTH = 12, numRecords, NULL)) AS 'December',
	    SUM(ordersPrice) AS total
	    FROM (
	        SELECT  ordersPrice , SUM(ordersPrice) AS numRecords, MONTH(modifiedDate) AS MONTH, COUNT(*) AS numRecords2
	        FROM orders WHERE YEAR(modifiedDate) = ".$cur_year."
	        GROUP BY  MONTH
	    ) AS SubTable1 ");

		if($query->num_rows() > 0) {
			$row = $query->result_array();
		  return $row;
		}else 
		{
			return false;
		}
    }

	public function earning_report($cur_year)
	{
		if(!empty($cur_year)){
			$cur_year = $cur_year;
		} 
		else {
			$cur_year = 'YEAR(CURDATE())';
		}
		return $this->db->query("SELECT 

					SUM(IF(MONTH = 1, numRecords, NULL)) AS 'January',
					SUM(IF(MONTH = 2, numRecords, NULL)) AS 'Feburary',
					SUM(IF(MONTH = 3, numRecords, NULL)) AS 'March',
					SUM(IF(MONTH = 4, numRecords, NULL)) AS 'April',
					SUM(IF(MONTH = 5, numRecords, NULL)) AS 'May',
					SUM(IF(MONTH = 6, numRecords, NULL)) AS 'June',
					SUM(IF(MONTH = 7, numRecords, NULL)) AS 'July',
					SUM(IF(MONTH = 8, numRecords, NULL)) AS 'August',
					SUM(IF(MONTH = 9, numRecords, NULL)) AS 'September',
					SUM(IF(MONTH = 10, numRecords, NULL)) AS 'October',
					SUM(IF(MONTH = 11, numRecords, NULL)) AS 'November',
					SUM(IF(MONTH = 12, numRecords, NULL)) AS 'December',
					SUM(ordersPrice) AS total
					FROM (
						SELECT  ordersPrice , SUM(ordersPrice) AS numRecords, MONTH(modifiedDate) AS MONTH, COUNT(*) AS numRecords2
						FROM orders WHERE YEAR(modifiedDate) = ".$cur_year."
						GROUP BY  MONTH
					) AS SubTable1 ");
    }

    function get_courses_from_orders()
    {
    	$this->db->select('courseID, transactionID');
    	$this->db->from("orders");
    	$this->db->group_by("orders.courseID");
    	$query = $this->db->get();
    	return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }

    function cal_author_earning($course_id)
    {
    	$query = $this->db->query('
    		SELECT sum(`educatorEarning`.`earnings`) AS `earning`,
			sum(`educatorEarning`.`adminCommission`) AS `admin_commission`,
			sum(`educatorEarning`.`totalPrice`) AS `total_earning`,
			`courses`.`displayName`,
			`courses`.`coursesID`,
			`users`.`firstName`,
			`users`.lastName,
			`co_author`.`authorID`,
			`co_author`.`userID`,
			`courses`.`ceCheck`
			FROM
			`courses`
			JOIN `co_author`
			ON `courses`.`coursesID` = `co_author`.`coursesID` 
			JOIN `educatorEarning`
			ON `co_author`.`userID` = `educatorEarning`.`authorID`
			AND `co_author`.`coursesID` = `educatorEarning`.`courseID`
			JOIN `users`
			ON `educatorEarning`.`authorID` = `users`.`userID`
			WHERE `courses`.`coursesID` = '.$course_id.' AND `co_author`.`authorDeleted` = 0
			GROUP BY
			`co_author`.`userID`
			ORDER BY courses.coursesID
			');
    	return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }

    function insert_earning($data, $user_id, $course_id)
    {
    	$this->db->select("earning,coursesID");
    	$where = array (
    		"userID" => $user_id,
    		"coursesID" => $course_id
    	);
    	$this->db->where($where);
    	$this->db->from('payments_educators');
    	$query = $this->db->get();
    	if($query->num_rows() > 0)
    	{
    		$this->db->where($where);
    		$this->db->update("payments_educators", $data);
    	}else
    	{
    		$rows = $this->db->insert("payments_educators", $data);
        	return $rows;
    	}
    }

    public function payment_report()
    {
    	return $this->db->query('SELECT
		`users`.`firstName` as "First Name",
		`users`.`lastName` as "Last Name",
		`users`.`userEmail` as "User Email",
		`payments_educators`.`courseName` as "Course Name",
		`payments_educators`.`earning` as "Earned By the Educator",
		`payments_educators`.`adminCommission` as "Commission Paid to Admin",
		`payments_educators`.`totalPriceByCourse` as "Total of Courses Sell"
		FROM
		`payments_educators`
		JOIN `users`
		ON `payments_educators`.`userID` = `users`.`userID`');
    }
}

