<?php

class Comapny_model extends CI_Model {

	public function __construct() {
		parent::__construct();

	}



	function add_company($data , $user_id)
	{
		$this->db->select('userCompanyName , userEmail ');
		$where = array (
						'userEmail' => $data['userEmail'],
					);
		$this->db->where($where);
		$query = $this->db->get('users');

				 if($query->num_rows() > 0) {
					$row = $query->result_array();
				  return false;
				} else {

					$this->db->insert('users', $data);
					$last_user_id = $this->db->insert_id();

					return true;

				}

	}

	function get_compnay($params = array())
	{
		$this->db->select('*');
			$where = array (
						'userType' => 'company',
						'userDeleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('userID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function search_company($company_id , $params = array())
	{
		$this->db->select('*');
			$where = array (
						'userID' => $company_id,
						'userType' => 'company',
						'userDeleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('userID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}


	function get_compnay_by_id($company_id)
	{
		$this->db->select('*');
		$this->db->join('users_meta', 'users.userID = users_meta.userID','LEFt');
		$where = array (
						'users.userID' => $company_id,
					);
		$this->db->where($where);
		$this->db->from('users');
		// $this->db->order_by('users.userID','desc');

			// $this->db->limit(1);


				$query = $this->db->get();
				if(($query->num_rows() > 0)){
						 $rows = $query->result_array();
		 	$newdata = array(
				  'userID'  => $rows[0]['userID'],
				  'userEmail'   => $rows[0]['userEmail'],
				  'userCompanyName'   => $rows[0]['userCompanyName'],
				  'userMetaKey'   => $rows[0]['userMetaKey'],
				  'userMetaValue'   => $rows[0]['userMetaValue'],
				);

				return ($rows);
				} else {
					return false;
				}

		// return ($query->num_rows() > 0)?$query->result_array()$where = array (
						// 'userID' => $userID,
						// 'userEmail' => $userEmail,
						// 'userCompanyName' => $userCompanyName,
					// );:FALSE;
	}



	function get_membership_package()
	{
		$this->db->select('*');
		$where = array (
						'membership.deleted' => 0,
					);
		$this->db->where($where);
		$this->db->from('membership');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function delete_company($company_id)
	{
		$this->db->select('userDeleted');
		$where = array (
						'userID' => $company_id
					);
		$this->db->where($where);
		$query = $this->db->get('users');

		 if($query->num_rows() > 0){
			 $row = $query->result_array();
			  if($row[0]['userDeleted'] == 0) {
				  $user_deleted = 1;
			  }
			  else {
				  $user_deleted = 0;
			  }
			$this->db->where(array("userID" => $company_id));
			$this->db->set('userDeleted',$user_deleted);
            $this->db->update('users');

		 }
	}



	 public function record_count() {
        return $this->db->count_all("users");
    }

    public function fetch_countries($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("users");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


	function update_company($data , $company_id) {

	$where = array (
						'userID' => $company_id
					);
		$this->db->where($where);
		$result = $this->db->update('users', $data);
		
		$data2   = array(
                    'userID' => $company_id
            );
			$where2 = array (
				'userID' => $company_id
			);
		if($result) {

		return true;
		} else {

		return false;
		}
	}



	function funccc() {

	$where = array (
						'userID' => $company_id,
					);
		$this->db->where($where);
		$this->db->update('users', $data);

		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		return $report;
	}





	function delete_users($userID)
	{
		$this->db->select('userDeleted');
		$where = array (
						'userID' => $userID
					);
		$this->db->where($where);
		$query = $this->db->get('users');

		 if($query->num_rows() > 0){
			 $row = $query->result_array();
			  if($row[0]['userDeleted'] == 0) {
				  $user_deleted = 1;
			  }
			  else {
				  $user_deleted = 0;
			  }
			$this->db->where(array("userID" => $userID));
			$this->db->set('userDeleted',$user_deleted);
            $this->db->update('users');

		 }



	}
	function change_status($status_name,$status_value,$user_id)
	{
		if($status_name == 'payment') {
							$this->db->where(array("userID" => $user_id));
									$this->db->set('userPaymentStatus',$status_value);
				} else {
							$this->db->where(array("userID" => $user_id));
									$this->db->set('userStatus',$status_value);
				}

            $query = $this->db->update('users');
			 return $query;
	}

	function update_user($data , $userID)
	{
			$this->db->where(array("userID" => $userID));
			$this->db->set($data);
            $this->db->update('users');
			 return true;
	}



	    /**
    * get client users data from  the database,
    * store it in a new array and return it to the controller
    * @return array
    */
	function company_report()
	{


				return $query = $this->db->query("SELECT
										users.userName,
										users.userEmail,
										users.userPhoneNo
										 FROM `users`
										 WHERE  userDeleted = '0'
										");



	}

/**
    * get show membership package  data from  the database,
    * store it in a new array and return it to the controller
    * @return array
    */
	function get_company_phone($phone_no)
	{

			$clientID = $this->session->userdata('userID');
            $query = $this->db->query("SELECT *
                    FROM `users`
                    WHERE userName like '" .$phone_no. "%' AND userDeleted = 0 AND userType='company'
                    ORDER BY userID
                    LIMIT 0,10");
            $skillarryhold = '';
            if($query->num_rows() > 0){
                
			    $rows     = $query->result();
                foreach ($rows as $row)
                {
                    $cashbackComission = 0;
                    $charityComission = 0;

                    $skillarryhold .= ' <ul id="name-list">
                    <li class="phone_click" onClick="selectname('.$row->userID.');"><span class=""><i class="fa fa-phone" style="/*background: #e5e5e5;*/ padding: 12px 27px 12px 12px;margin-top: 0;" ></i></span><span class="">'.$row->userName.'</span></li></ul>';
                }
                return $skillarryhold;
                
            }
}



	public function search ($title){
        $this->db->select('userName');
        $this->db->like('userName', $title, 'both');
        return $this->db->get('users');
    }

	public function filter_by_date($starting_date,$ending_date,$payment_filter,$status_filter,$params = array())
	{
		$original_start_Date = $starting_date;
		$original_end_Date = $ending_date;
		$start_date = date("Y-m-d", strtotime($original_start_Date));
		$end_date = date("Y-m-d", strtotime($original_end_Date));

		$this->db->select('*');
		// $this->db->join('orders', 'orders.companyID = users.userID','right');
		if($payment_filter =="" && $status_filter =="" && $starting_date != 0){
			$condition = "users.userType = 'company' AND users.Date BETWEEN " . "'" . $start_date . "'" . " AND " . "'" . $end_date . "' AND users.userDeleted = '0' ";
		}else if($payment_filter =="" && $starting_date != 0){
			$condition = "users.userType = 'company' AND users.Date BETWEEN " . "'" . $start_date . "'" . " AND " . "'" . $end_date . "' AND users.userStatus ='".$status_filter."' AND users.userDeleted = '0' ";
		}else if($status_filter =="" && $starting_date != 0){
			$condition = "users.userType = 'company' AND users.Date BETWEEN " . "'" . $start_date . "'" . " AND " . "'" . $end_date . "' AND users.userPaymentStatus ='".$payment_filter."' AND users.userDeleted = '0'";
		}else if ($starting_date == 0 && $payment_filter ==""){
			$condition = "users.userType = 'company' AND users.userStatus ='".$status_filter."' AND users.userDeleted = '0' ";
		}else if ($starting_date == 0 && $status_filter ==""){
			$condition = "users.userType = 'company' AND users.userPaymentStatus ='".$payment_filter."' AND users.userDeleted = '0' ";
		}else if($starting_date == 0){
			$condition = "users.userType = 'company' AND users.userStatus ='".$status_filter."' AND users.userPaymentStatus ='".$payment_filter."' AND users.userDeleted = '0' ";
		}else{
			$condition = "users.userType = 'company' AND users.Date BETWEEN " . "'" . $start_date . "'" . " AND " . "'" . $end_date . "' AND users.userPaymentStatus ='".$payment_filter."' AND users.userStatus ='".$status_filter."' AND users.userDeleted = '0' ";
		}

		$this->db->where($condition);
		$this->db->from('users');
		$this->db->order_by('userID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}









}

