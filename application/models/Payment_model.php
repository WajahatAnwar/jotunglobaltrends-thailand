<?php

class Payment_model extends CI_Model {
		
	public function __construct() {
		parent::__construct();

	}
	
	
	


	public function add_paypal_payment($paypal_info)
	{
       $this->db->select('*');
		$where = array (
						'transactionID' => $paypal_info['transactionID'],
					);
		$this->db->where($where);
		$query = $this->db->get('orders');

		 if($query->num_rows() > 0) {
			// $row = $query->result_array();
		  return true;
		} else {

			$this->db->insert('orders', $paypal_info);
			$last_user_id = $this->db->insert_id();
			$this->db->where(array("userID" => $paypal_info['companyID']));
			$this->db->set('userPaymentStatus','0');
            $this->db->update('users');
			// return $last_user_id;
			return true;

		}
    }



	function get_compnay_by_id($company_id)
	{
		$this->db->select('*');
		// $this->db->join('users_meta', 'users.userID = users_meta.userID','LEFt');
		$where = array (
						'users.userID' => $company_id,
					);
		$this->db->where($where);
		$this->db->from('users');
		// $this->db->order_by('users.userID','desc');

			$this->db->limit(1);


				$query = $this->db->get();
				if(($query->num_rows() > 0)){
						 $rows = $query->result_array();

				return ($rows);
				} else {
					return false;
				}
	}


	function assign_course($assign_course)
	{
        $this->db->select('*');
		$where = array (
						'employeeID' => $assign_course['employeeID'],
						'coursesID' => $assign_course['coursesID']
					);
		$this->db->where($where);
		$query = $this->db->get('employee_courses');
        
		 if($query->num_rows() > 0) {
            $this->db->where($where);
            $query = $this->db->update('employee_courses', $assign_course);
			return $query;
         }else
         {
			$query = $this->db->insert('employee_courses', $assign_course);
			return $query;
             
         }
	}
}

