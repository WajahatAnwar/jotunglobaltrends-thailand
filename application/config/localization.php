<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['site_name'] = 'YappGuru University';
$config['site_description'] = '';
$config['site_author'] = '';
$config['keywords'] = '';

$config['logo'] = 'assets/yappguru_logo.png';
$config['help_video'] = 'https://www.youtube.com/embed/zGxwXiI79F0';


//Terms
$config['project_name'] = 'YappGuru University';

//Landing Page
$config['slider'] = 'yguni_slider.jpg';
$config['into_video'] = 'https://www.youtube.com/embed/zGxwXiI79F0';
