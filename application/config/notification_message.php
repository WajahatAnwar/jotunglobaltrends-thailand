<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['wishlist_link'] = "http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/wishlist";
$config['wishlist_saved'] = "http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/wishlist_saved";
$config['calm_link'] = "http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/calm";
$config['refined_link'] = "http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/refined";
$config['raw_link'] = "http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/raw";
$config['palette_link'] = "http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/palette";
$config['colour_link'] = "http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/colours";

