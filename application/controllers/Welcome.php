<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		// $this->load->view('landing' ,false);
		$data["content"] = "landing";
		$this->load->view('template/template2', $data, false);
	}

	public function colours()
	{
		$data["content"] = "colours";
		$this->load->view('template/template', $data, false);
	}

	public function frontPage()
	{
		$data["content"] = "frontPage";
		$this->load->view('template/template', $data, false);
	}

	public function palettes()
	{
		$data["content"] = "palettes";
		$this->load->view('template/template', $data, false);
	}

	public function calm()
	{
		$data["content"] = "t1-calm";
		$this->load->view('template/template', $data, false);
	}

	public function refined()
	{
		$data["content"] = "t2-refined";
		$this->load->view('template/template', $data, false);
	}

	public function raw()
	{
		$data["content"] = "t3-raw";
		$this->load->view('template/template', $data, false);
	}

	public function wishlist()
	{
		$data["content"] = "wishlist";
		$this->load->view('template/template', $data, false);
	}

	public function wishlist_saved()
	{
		$data["content"] = "wishlist-saved";
		$this->load->view('template/template', $data, false);
	}

	public function share($var1 = null , $var2 = null)
	{
		// print_r($_GET);
		// $data["content"] = "wishlist-saved";
		// $this->load->view('save_image', $data ,false);
		if(isset($_GET["comp_color_array"]))
		{
			$data["comp_color_array"] = $_GET["comp_color_array"];
			$data["comp_name_array"] = $_GET["comp_name_array"];
		}

		if(isset($_GET["acce_color_array"]))
		{
			$data["acce_color_array"] = $_GET["acce_color_array"];
			$data["acce_name_array"] = $_GET["acce_name_array"];
		}
		
		if(isset($_GET["whit_color_array"]))
		{
			$data["whit_color_array"] = $_GET["whit_color_array"];
			$data["whit_name_array"] = $_GET["whit_name_array"];
		}
		

		$data["original_name"] = $_GET["original_name"];
		$data["original_colour"] = $_GET["original_colour"];

		$this->load->view('save_image', $data ,false);



	}

	public function show()
	{
		$this->load->view('save_image' ,false);
	}

	public function email()
	{

		$this->load->view('email_template', false);
	}

	public function submit_email()
	{
		$color_code_array = $_POST["color_code_array"];
		$color_name_array = $_POST["color_name_array"];
		$email = $_POST["email"];

		$data["color_code_array"] = $color_code_array;
		$data["color_name_array"] = $color_name_array;
		$data["email"] = $email;
		
		$this->send_email($email, $color_code_array, $color_name_array);
	}

	 /**
    * Send Email Function
    * @return void
    */
	function send_email($email, $color_code_array, $color_name_array)
	{
											
					$this->load->library('email');
					
					$config['charset'] = 'iso-8859-1';
					$config['wordwrap'] = TRUE;   
					$config['mailtype'] = 'html';
					$this->email->initialize($config);
					$this->email->from('info@jotun.com', 'Your Favorites');
					$this->email->to($email);

					$this->email->subject('Email Test');

					$data["color_code_array"] = $color_code_array;
					$data["color_name_array"] = $color_name_array;
					$messages = $this->load->view('email_template', $data, true);
					
					$this->email->message($messages);	

					$this->email->send();
					
					// $config['protocol'] 		= 'smtp';
					// $config['smtp_host']        = 'ssl://smtp.mailgun.org';
					// $config['smtp_port']        = 465;
					// $config['smtp_user']        = 'postmaster@mail.yguni.com';
					// $config['smtp_pass']        = '75aa2fc22be147890b517841ac00e565';
					// $config['smtp_crypto']      = '';
					

					// $this->email->initialize($config);
					// $this->email->from('info@yguni.com', 'YappGuru University (yguni.com)');
 
					
					// $this->email->to($email);
					// $this->email->subject('Verification Email'); 
					
					// $data["color_code_array"] = $color_code_array;
					// $data["color_name_array"] = $color_name_array;
					
					// $messages = $this->load->view('email_template', $data, true);
					
					// $this->email->message($messages);				
					// return $this->email->send();	
	}
	
	public function shares($var = null)
	{
		
		print_r($_POST);
	}

	public function submission_email()
	{
		$i = 0;
		$custom_variable = file_get_contents("https://colours-api-stage.allegro.no/submission/1eaffae0-f97c-4922-9805-bdc08a209579");
		$data = json_decode($custom_variable);
		$colors_numbers =  $data->colors;
		print_r($colors_numbers);
		$dataJson = file_get_contents(base_url()."assets/data2.json");
		$dataJson2 = json_decode($dataJson, true);
		// print_r($jsonFavorites);
		// $dataJson2 = json_decode($dataJson, true);
		// print_r($dataJson2);
		foreach($dataJson2 as $colorNumber)
		{
			print_r($colorNumber);
		}
	}

	public function landing()
	{
		$this->load->view('landing' ,false);
	}

    public function hex2rgb($hex) 
    {
           $hex = str_replace("#", "", $hex);

           if(strlen($hex) == 3) {
              $r = hexdec(substr($hex,0,1).substr($hex,0,1));
              $g = hexdec(substr($hex,1,1).substr($hex,1,1));
              $b = hexdec(substr($hex,2,1).substr($hex,2,1));
           } else {
              $r = hexdec(substr($hex,0,2));
              $g = hexdec(substr($hex,2,2));
              $b = hexdec(substr($hex,4,2));
           }
           $rgb = array($r, $g, $b);
           //return implode(",", $rgb); // returns the rgb values separated by commas
           return $rgb; // returns an array with the rgb values
    }

}
