<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
    }
    //This Function loads the lading page which contain latest course listing and things also

	public function index()
	{
		$data["content"] = "colours";
		$this->load->view('template/template', $data, true);
	}
}